# C Minus (C-) Compiler : CIS 4650 Project #

## Resources ##

- [Implementation outline](http://moodle.socs.uoguelph.ca/mod/resource/view.php?id=5236)
- [Markdown Syntax](http://daringfireball.net/projects/markdown/syntax)
- [Doxygen Syntax](http://www.stack.nl/~dimitri/doxygen/manual/docblocks.html)
- [Latex Math Syntax](ftp://ftp.ams.org/pub/tex/doc/amsmath/short-math-guide.pdf)
- [Git Reference](http://gitref.org/)
- [flex](http://flex.sourceforge.net/manual/)
- [bison](http://www.gnu.org/software/bison/manual/bison.pdf)


### Language Specification ###

### Special Symbols ###

- `{ }` Braces
- `[ ]` Brackets
- `( )` Parentheses

- `/* */` Comments (Compound)
- `*` Asterisk (Multiplication, Part of Comment Tokens)
- `/` Slash (Division, Part of Comment Tokens)

- `,` Comma 
- `;` Semicolon
- `+` Positive / Addition
- `-` Negative / Subtraction

- `=` Assignment
- `<` Less than
- `>`  Greater than
- `==`  Equal to (Compound)
- `!=`  Not Equal to (Compound, no risk of ambiguity)
- `<=`  precedes (Compound)
- `>=`  succeeds (Compound)

#### Keywords ####

    if  else  while  int  void  return

### Tokens ###

    ID = letter+  
    NUM = digit+  
    letter = [a-zA-z]  
    digit = [0-9]  

### Lexical Notes ###

- white spaces are ignored, but separate ID's and Num's
- Comments may *NOT* be nested
- Comments can appear anywhere white space can.

### Compiler Command Line Options ###

- a : perform syntactic analysis and output an abstract syntax tree (.abs)
- s : perform type checking and output symbol tables (.sym)
- c : compile and output TM assembly language code (.asm)