/**
    @file scan.h
    @brief The interface for the scanner
*/

#ifndef _SCAN_H_
#define _SCAN_H_
      
#ifdef __cplusplus
extern "C" {
#endif

/**
 * MAX_TOKEN_LEN is the maximum size of a token allowed
 */
#define MAX_TOKEN_LEN 40

/**
 * token_string array stores the lexeme of each token
 */
extern char token_string[MAX_TOKEN_LEN + 1];

/**
 * Drives the scanning process to read the next token.
 * @return the next token from stdin
 */
extern int getToken( void );
 
#ifdef __cplusplus
}
#endif
#endif
