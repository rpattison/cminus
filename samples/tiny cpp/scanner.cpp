/****************************************************/
/* File: scanner.cpp                                */
/* scanner for the TINY language                    */
/* Fei Song                                         */
/****************************************************/

#include "globals.h"
#include "util.h"
#include "scan.h"

// allocate global variables
int rowno = 1;
extern FILE * yyin;
extern FILE * yyout;
extern TokenStruct yylval;

main( int argc, char * argv[] ) { 
  yyin = stdin;
  yyout = stdout;
  TokenType ttype;
  while( (ttype = getToken()) != ENDFILE )
    printToken( yyout, ttype, yylval );
  return 0;
}

