## README ##
#### Compilation ####
On the command line, navigate to `cminus` and type

    $ make 

To remove any residual files type and the generated executable, type:

    $ make clean


#### Running ####
To run the *cm* compiler on a source file, type:

    $ ./cm -[asc] <file>

#### Compiler Command Line Options ####
-**a** :   perform syntactic analysis and output an abstract syntax tree (.abs)  
-**s** :   perform type checking and output symbol tables (.sym)  
-**c** :   compile and output TM assembly language code (.asm)  

#### Testing ####

Test files are provided in the `test_code` and `error_code` folders. For example:

    $ ./cm -a test_code/fib.c

#### Generate Documentation ####
Note that you will need to have [*Doxygen*](http://www.stack.nl/~dimitri/doxygen/download.html) installed and on the `PATH` to compile the documentation. On the command line, navigate to `cminus` and type

    $ doxygen

 