title: CIS*4650 Compilers
author: Ryan Pattison and Abhishek Vanarase
Date: Feb 28th, 2013

	 	
# Checkpoint 1 # 
## What was Accomplished ##

Checkpoint one was the implementation of the lexical and syntactical analysis portion of the C-Minus language. This was done in the C programming language using the flex and yacc tools which are available on most unix machines. The initial goal of the project was to implement the compiler in ‘c plus plus’. However, due to the unfamiliarity of the project, we decided to implement C-Minus in C and build off the sample code that was provided by our instructor. This restructuring of the *tiny* code gave us a clear design and structure to work within,. In addition, the use of the *tiny* code allowed us to use  the course textbook and slides as a reference.

## What is next ##

For the next implementation of the project, we still intend to move to ‘c plus plus’ due the benefits of object oriented programming, particularly in the implementation of data structures (trees and hash maps) which are greatly improved by polymorphism. 

## Development Process ##
### Lexical Analysis ###
The design of the lexer was relatively straightforward, using tiny and the language specification on moodle there wasn’t much design to be done. The most difficult part was removing the c-style comments since these spanned multiple lines and have 2 tokens to denote the end which requires a double nested loop while multiple conditions. Further complications arose when testing and it was revealed that the `input` function from lex does **not** return the `EOF` character when it reaches the end of file, but instead returns ‘0’. This bug was also present in the `tiny` language lex file.

### Syntactic Analysis ###
The parser was where the most time and effort was allocated. The parser required the implementation of all the rules for assigning the tokens generated by the lexer. The rules allowed for each node in the tree structure to be defined with either statement, error, for declaration or expression. These nodes once assigned were printed in a tree structure using indentation.

### Error Handling ###
The error handling was done using yacc rules to capture errors at point for which the input can be synchronized to allow further processing of the source file. The errors were defined as expressions which not expressly stated as a rule. This allowed us to state the rules for accepted grammar and classify anything not matching these rules to be an error.

###Work Distribution ###
Unfortunately, a breakdown of roles and responsibilities in the group is not possible with the development style used, *scrum*. Since the work was done in the same room, at the same time, and work was distributed on a micro level, all we can say is that the work was evenly distributed. This work method has been very effective and maximizes our learning of the task of implementing every part of the compiler.


