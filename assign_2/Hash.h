/**
* @file Hash.h
* @brief This is the interface for the SymbolTable Hash Table.
*   To use this table you should define the TreeNode* in Hash_types.h
* @note This file is part of the C- compiler for the CIS-Compilers Course.
* @date 2013
* @authors Ryan Pattison, Abhishek Vanarase
* @example hash_test.c
*/

#ifndef __HASH_H__
#define __HASH_H__ 1

#ifdef __cplusplus
extern "C" {
#endif

#include "Hash_types.h"


/************************************************************
 # Hash Table
************************************************************/

/**
* @brief Inserts a new key-value pair into a table.
* @param T an initialized Table object.
* @param key a key to store for retrieving the value later.
* @param value the value of key in the current scope.
* @return non-zero for an error, 0 for success.
* @note different scopes may contain duplicate keys.
* @note No scope can contain duplicate keys.
*/
extern int tbl_insert(Table* T, const char* key, TreeNode* value);

/**
* @brief Updates a key-value pair in the table.
* @param T an initialized Table object.
* @param key a key referring to the object.
* @param value the value for key to set.
* @return non-zero for an error, 0 for success.
*/
extern int tbl_update(Table* T, const char* key, TreeNode* new_value);

/**
* @brief Retrieves a value for a key in the nearest scope.
* @param T an initialized Table object.
* @param key a key to store for retrieving the value later.
* @param value a place to store the value of key in the current scope. The
*    caller must have allocated memory for value.
* @return non-zero for an error, 0 for success.
*/
extern int tbl_lookup(const Table* T, const char* key,
                    TreeNode** const value);

/**
* @brief Returns the current scope level.
* @param T an initialized Table object.
* @return the scope level [0, ...] or -1 for error.
*/
extern int tbl_scope(const Table* T);

/**
* @brief begins a new scope to insert values at.
* @param T an initialized Table object.
* @return non-zero for an error, 0 for success.
*/
extern int tbl_beginScope(Table* T);

/**
* @brief Ends the current scope, deleting values defined in that scope.
* @param T an initialized Table object.
* @return non-zero for an error, 0 for success.
*/
extern int tbl_endScope(Table * T);

/**
* @brief Creates a new table.
* @param size the size of the table, larger values will avoid clashes
*           and prime numbers tend to do better.
* @return an initialized Table object or NULL on failure.
* @note this allocates memory and must be freed using tbl_delete.
*/
extern Table* tbl_new(int size);

/**
* @brief Frees the memory allocates to the Table.
* @param T an initialized Table object.
* @note it is harmless to call this with NULL.
*/
extern void tbl_delete(Table* T);


/************************************************************
 # Hash Table Iterator
************************************************************/

/**
* @brief Moves the iterator to the next pair in the sequence.
* @param iter an initialized TableIter object.
* @return 1 on success, and 0 for the end of the sequence.
*/
extern int itr_step(TableIter* iter);

/**
* @brief Inserts a new key-value pair into a table.
* @param T an initialized Table object.
* @param key a key to match for defining a sequence or NULL to ignore.
* @param value the value to match for defining a sequence or -1 to ignore.
* @return a TableIter object set at the start of the sequence
*           or NULL on failure
* @note The creation of an iterator stops the Table from being
*       modified, you must call @ref itr_delete to be able to modify
*       the table  again.
*/
extern TableIter* itr_new(Table* T, const char* key, int scope);

/**
* @brief Returns 0 if the sequence is not done, and non-zero when finished.
* @param iter an initialized TableIter object.
* @return 0 if the sequence is *not* done, non-zero otherwie.
*/
extern int itr_done(const TableIter* iter);

/**
* @brief Deletes an iterator and releases the Table to be modified.
* @param iter an initialized TableIter object.
*/
extern void itr_delete(TableIter* iter);

/**
* @brief Returns the current value in the sequence.
* @param iter an initialized TableIter object.
* @return the current value in the sequence or garbage if it is the
*          end of the sequence.
*/
extern TreeNode* itr_value(const TableIter* iter);

/**
* @brief Returns the scope of the current value in the sequence.
* @param iter an initialized TableIter object.
* @return the scope of the value in the sequence or -1 at end of the sequence.
*/
extern int itr_scope(const TableIter* iter);

/**
* @brief Returns the key for the current value in the sequence.
* @param T an initialized TableIter object.
* @return The key for the current value in the sequence or NULL if it is the
*           end of the sequence.
*/
extern const char* itr_key(const TableIter* iter);

#ifdef __cplusplus
}
#endif
#endif
