/**
    @file util.h
    @brief provides utilities for output and displaying trees
    provides constructors for tree nodes for the parser to use.
*/

#ifndef _UTIL_H_
#define _UTIL_H_

#include "globals.h"

/**
     Prints a token and its value to listing.
     @param token the `TokenType` returned from the scanner
     @param tokenString the string that matched `token`
*/
extern void printToken( int token, const char* tokenString );

/**
     newExpNode creates a new expression node for syntax tree construction
     @param kind the kind of expression node
     @return a new expression node 
*/
extern TreeNode* newExpNode( ExpKind kind );

/**
 creates a new Declaration node for syntax tree construction
 @param kind the kind of declaration
 @return a new declaration node 
*/
extern TreeNode* newDeclNode( DecKind kind );

/**
 creates a new error node for syntax tree construction
 @param kind the kind of the error node
 @return  a new error node 
 */
 extern TreeNode * newErrorNode( );

/**
 creates a new statement node for syntax tree construction
 @param kind the kind of statement node
 @return  a new statement node 
 */
extern TreeNode* newStmtNode( StmtKind kind );

/**
 prints a syntax tree to the listing file using indentation to indicate subtrees
 @param tree the root of a syntax tree to print.
 */
extern void printTree( TreeNode* tree );

/**
 prints a syntax tree to the listing file in colour
 @param tree the root of a syntax tree to print.
 */
extern void printTreeColour( TreeNode* tree );

/**
     Prints a token and its value to listing in colour.
     @param token the `TokenType` returned from the scanner
     @param tokenString the string that matched `token`
*/

extern void printTokenColour( int token, const char* token_string );
 
/**
 allocates and makes a new copy of an existing string
 @param s the string to be copied
 @return  a malloced string that is a duplicate of s
 @warn this value must be freed later using `free()`
*/
extern char* copyString( char* s );

#endif
