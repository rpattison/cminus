int x[10];

int isSorted( int a[], int len ) {
	int i;
	
	i = 1;
	while ( i < len ) {
		if ( a[i] < a[i - 1]) {
			return ( 0 );
		} else {
			i = i + 1;
		} 
	}
	
	return ( 1 );
}


void main ( void ) {
	int i;
	int len;
	
	len = input();
	i = 0;
	while ( i < len ) {
		x[i] = input();
		i = i + 1;
	} 
	
	output( isSorted( x, len ) );
}
