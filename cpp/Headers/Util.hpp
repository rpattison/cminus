#ifndef __UTIL_HPP__
#define __UTIL_HPP__ 1

#include <fstream>
#include <iostream>

#include "Absyn.hpp"
#include "Exp.hpp"
#include "Var.hpp"
#include "Dec.hpp"
#include "VarDec.hpp"
#include "Misc.hpp"
#include "Visitors.hpp"

using namespace std;

class TreePrint:public ASTVisitor {
  public:
    TreePrint(int indent = 0) {
	indent_ = indent;
    }

    ~TreePrint() {
    }

    void indent() {
        indent_++;
    }

    void unindent() {
        indent_--;
    }

    void visit(Absyn * node) {
	if (node == NULL) {
	    cout << "(null)" << endl;
	} else {
	    cout << "[" << node->getLine() << "] Absyn" << endl;
	}
    }

    void visit(Exp * node) {
	cout << "Exp" << endl;
    }

    void visit(Dec * node) {
	cout << "Dec" << endl;
    }

    void visit(Var * node) {
	cout << "Var" << endl;
    }

    void visit(VarDec * node) {
	cout << "VarDec" << endl;
    }

    void visit(SimpleVar * node) {
	cout << "SimpleVar" << endl;
    }

    void visit(IndexVar * node) {
	cout << "IndexVar" << endl;
    }

    void visit(NilExp * node) {
	cout << "NilExp" << endl;
    }

    void visit(VarExp * node) {
	cout << "VarExp" << endl;
    }

    void visit(IntExp * node) {
    if (node == NULL)
    {
        cout << "(null) IntExp";
    }
    else
    {
        cout << "[" << node->getLine() << "]";
        cout << "number(" << node->getValue() << ")";
    }

	cout << endl;
    }

    void visit(CallExp * node) {
	cout << "CallExp" << endl;
    }

    void visit(OpExp * node) {
	cout << "OpExp" << endl;
    }

    void visit(AssignExp * node) {
	cout << "AssignExp" << endl;
    }

    void visit(IfExp * node) {
	cout << "IfExp:" << endl;
	node->getCondition()->accept(this);
	node->getThenBranch()->accept(this);
	node->getElseBranch()->accept(this);
    }

    void visit(WhileExp * node) {
	cout << "WhileExp" << endl;
    }

    void visit(ReturnExp * node) {
	cout << "ReturnExp" << endl;
    }

    void visit(CompoundExp * node) {
	cout << "CompoundExp" << endl;
    }

    void visit(WriteExp * node) {
	cout << "WriteExp" << endl;
    }

    void visit(ReadExp * node) {
	cout << "ReadExp" << endl;
    }

    void visit(FunctionDec * node) {
	    if (node == NULL) {
	        cout << "(null)FunctionDec";
	    } else {
	        cout << "FunctionDec";
	        cout << "\"";
	        cout << *node->getFunctionName();
	        cout << "\"";
	        node->getReturnType()->accept(this);
	        VarDecList* params = node->getParamList();
	        if (params) {
	            params->accept(this);
	        }
	        CompoundExp* body = node->getStatements();
	        if (body) {
	            body->accept(this);
	        }
         }
        cout << endl;
    }

    void visit(SimpleDec * node) {
	cout << "SimpleDec" << endl;
    }

    /** [3] ArrayDec "myArray"<int>[size: ... ] */
    void visit(ArrayDec * node) {
	    if (node == NULL) {
	        cout << "(null) ArrayDec";
	    } else {
	        cout << "[" << node->getLine() << "] ArrayDec ";
	        cout << "\"" << *node->getName() << "\"";
	        cout << "<";
	        this->indent();
	        node->getType()->accept(this);
	        this->unindent();
	        cout << ">";
            cout << "[size:";
            if(node->getSizeExpression())
                node->getSizeExpression()->accept(this);
             else
                cout << "(empty)";
            cout << "]";
        }
        cout << endl;
    }

    void visit(NameTy * node) {
	    if (node == NULL) {
	        cout << "(null) NameTy";
	    } else {
	        cout << "[" << node->getLine() << "] NameTy";
            if (node->getType() == NameTy::kInt) {
                cout << "<int>";
            } else if (node->getType() == NameTy::kVoid) {
                cout << "<void>";
            }
        }
        cout << endl;
    }

    void visit(ExpList * node) {
	if (node == NULL) {
	    cout << "(null) ExpList" << endl;
	} else {
	    cout << " ExpList" << endl;
	    if (node->getHead())
	        node->getHead()->accept(this);
	    else
	        cout << "(empty)";
	    for (ExpList * L = node->getNext(); L; L = L->getNext()) {
		    if (L->getHead())
		        L->getHead()->accept(this);
		    else cout << "(empty)";
	    }
	}
	cout << endl;
    }

    void visit(DecList * node) {
	if (node == NULL) {
	    cout << "(null) DecList";
	} else {
	    cout << " DecList ";
	    if (node->getHead() == NULL) {
	        cerr << "Null Head in Declist" << endl;
	        return;
	    }
	    if (node->getHead())
	        node->getHead()->accept(this);
	    else
	        cout << "(empty)";
	    for (DecList * L = node->getNext(); L; L = L->getNext()) {
		    if (L->getHead())
		        L->getHead()->accept(this);
	        else
	            cerr << "Null head in list" << endl;
	    }
	}
	cout << endl;
    }

    void visit(VarDecList * node) {
	if (node == NULL) {
	    cout << "(null) VarDecList " << endl;
	} else {
	    cout << " VarDecList ";
	    if (node->getHead())
	        node->getHead()->accept(this);
	    else
	        cout << "(empty)";
	    for (VarDecList * L = node->getNext(); L; L = L->getNext()) {
		    if(L->getHead())
		        L->getHead()->accept(this);
	        else cout << "(empty)";
	    }
	}
	cout << endl;
    }

  private:
    int indent_;
};

#endif

