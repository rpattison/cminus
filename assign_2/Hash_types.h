#ifndef __HASH_TYPE_H__
#define __HASH_TYPE_H__
#ifdef __cplusplus
extern "C" {
#endif

#include "globals.h"

typedef struct Hash {
    struct Hash* next; //< next link in the same table cell.
    const char* key; //< string key value
    struct treeNode* value; //< user defined type
    int scope; //< scope it is defined at.
} Hash;


typedef struct Table {
    Hash** cell; //< list of table cells
    int size; //< size of the table ideally large and prime.
    int scope; //< current scope, items added will be of this scope [0 ...].
    int TableIters;
} Table;


typedef struct TableIter {
    Hash* current; //< current item matching the iter criteria
    int cell_index; //< the cell index current belongs in
    int scope; //< the scope criteria (or <0 for none)
    const char* key; //< the key criteria (or NULL for none)
    Table* T; //< the table being iterated over
} TableIter;

#ifdef __cplusplus
}
#endif
#endif /* __HASH_TYPE_H__ */



