%{
#define YYPARSER /* distinguishes Yacc output from other code files */
#define YYSTYPE TreeNode*

#include "Hash.h"
#include "util.h"
#include "scan.h"
#include "parse.h"
#include "globals.h"

#include <stdio.h>


extern Table* Sym;          ///< symbol table
extern int yychar;          ///< for printToken
static char error_buff[1024];///< string to hold error message
static ExpType savedType;   ///< saved expression type
int yyparse(void);          ///< defined externally used here
TreeNode* savedTree = NULL; ///< stores syntax tree for later return 

static int useMul = 1;

/**
 *   Function for bison
 *   Not used internally (yet)
 */
int yywrap() {
   return 1;
}



/**
 *   Print error message without current token
 *   (same as yyerror without the printToken)
 */
int syntaxError( char* message ) {
    int i;
    static char string[1024];
    int ctx = 1;
    
    fseek(sourceCode, 0, SEEK_SET);
    
    fprintf(stderr, "\033[1;34mSyntax Error\033[0m at line %d: %s\n",
        lineno, message);
    
    for (i = 0; i <= lineno + ctx; ++i) {
        if (fgets(string, 1024, sourceCode) == NULL) {
            break;
        } 
        
        if (i - lineno == 0) {
            fprintf(stderr, "\033[1;32m%d| %s\033[0m", i, string);
        } else if (abs(i - lineno) <= ctx)  {
            fprintf(stderr, "\033[1;32m%d| %s\033[0m", i, string);
        }
    }
    
    return (0);
}

/**
 *   Print error message and other
 *   relevant information to stderr
 */
int yyerror( char* message ) {
    int i;
    static char string[1024];
    int ctx = 1;
    
    fseek(sourceCode, 0, SEEK_SET);
    
    fprintf(stderr, "\033[1;34mSyntax Error\033[0m at line %d: %s\n",
          lineno, message);
    fprintf( stderr, "Current token: ");
    printToken( yychar, token_string );
    
    for (i = 0; i <= lineno + ctx; ++i) {
        if (fgets(string, 1024, sourceCode) == NULL) {
            break;
        } 
        if (i - lineno == 0) {
            fprintf(stderr, "\033[1;32m%d| %s\033[0m", i, string);
        } else if (abs(i - lineno) <= ctx)  {
            fprintf(stderr, "\033[1;32m%d| %s\033[0m", i, string);
        }
    }
    
    return (0);
}

/**
 *   yylex calls getToken to make Yacc/Bison output
 *   compatible with the CMinus scanner
 */
static int yylex(void) {
     return getToken();
}

/**
 *   End current scope and print symbols
 *   uses hash.c
 */
static void endScope() {
    TableIter* it = itr_new(Sym, NULL, tbl_scope(Sym));
    
    for (; !itr_done(it); itr_step(it)) {
        if (printSymbols) {
            TreeNode* val = itr_value(it);
            printf("\t%s<%s> +%d (use %d)\n",
                itr_key(it), typeName(val->type, val->subtype), val->offset, val->use_count);
        }
    }
    
    itr_delete(it);
    tbl_endScope(Sym);
}

/**
 *   calls yyparse to generate tree
 */
TreeNode* parse(void) {
    yyparse();
    return savedTree;
}

%}
%token ENDFILE
%token IF ELSE WHILE RETURN
%token VOID INT ID NUMBER
%token SEMICOLON COMMA
%token L_PAREN  R_PAREN L_BRACK  R_BRACK L_BRACE R_BRACE
%token ADD SUBTRACT ASSIGN DIVISION STAR PAREN
%token EQUAL NOT_EQUAL LESS_THAN GREATER_THAN LESS_THAN_EQ GREATER_THAN_EQ
%token ERROR

%nonassoc EQUAL NOT_EQUAL LESS_THAN GREATER_THAN LESS_THAN_EQ GREATER_THAN_EQ
%left ADD SUBTRACT
%left DIVISION STAR

%% /* Grammar for C Minus */

/* Begin Program and global scope */
program : {
    tbl_beginScope(Sym);
    if (printSymbols)
        printf("~~ Global Begin %d ~~\n", Sym->scope);
}
/* Global variables */
dec_list {
    savedTree = $2;
    if (printSymbols) 
        printf("~~ Global End %d ~~\n", Sym->scope);
    endScope();
};


dec_list :
/* Declaration List */
dec_list declar {
    YYSTYPE t = $1;
    if ( t != NULL ) {
        while ( t->sibling != NULL ) {
            t = t->sibling;
        }
        t->sibling = $2;
        $2->offset = t->offset + cmSize(t);
        $$ = $1;
    } else {
        $$ = $2;
    }
} 
| /* Single Declaration */
declar {
    $$ = $1;
    //$$->offset = cmSize($1);
};


declar :
/* Variable Declaration */
var_declar {
    $$ = $1;
} 
| /* Function Declaration */
func_declar {
    $$ = $1;
};

var_declar :
/* Variable declaration (int x;) */
type_spec id SEMICOLON {
    $$ = newDeclNode( VarK );
    $$->attr.name = (char*) $2;
    $$->type = savedType;
    $$->nestLevel = tbl_scope(Sym);

    TableIter* iter = itr_new(Sym, $$->attr.name, tbl_scope(Sym));
    if (!itr_done(iter)) {
        sprintf(error_buff, "%s already declared in current scope (%d)", $$->attr.name, tbl_scope(Sym));
        syntaxError(error_buff);
    }
    itr_delete(iter);
    tbl_insert(Sym, $$->attr.name, $$);
}
| /* Array declaration (int x[2];) */
type_spec id L_BRACK number R_BRACK SEMICOLON {
    $$ = newDeclNode( VarK );
    $$->attr.name = (char*)$2;
    $$->type = Array;
    $$->subtype = savedType;
    YYSTYPE t = newExpNode(ConstK);
    t->attr.val = atoi((char*)$4);
    t->type = Integer;
    $$->child[0] = t;

    TableIter* iter = itr_new(Sym, $$->attr.name, tbl_scope(Sym));
    if (!itr_done(iter)) {
        sprintf(error_buff, "%s already declared in current scope (%d)", $$->attr.name, tbl_scope(Sym));
        syntaxError(error_buff);
    }
    itr_delete(iter);
    $$->nestLevel = tbl_scope(Sym);
    tbl_insert(Sym, $$->attr.name, $$);
}
| /* Grab invalid identifier error */
type_spec lex_err SEMICOLON {
    $$ = $2;
}
| /* Grab invalid identifier error */
type_spec error SEMICOLON {
    $$ = newErrorNode();
};


lex_err :
/* Invalid Identifier (kasdjlajd;) */
ERROR {
    $$ = newErrorNode();
    yyerror("Not a valid identifier");
}
| /* Invalid Identifier (kasdjlajd;) */
lex_err ERROR {
    $$ = newErrorNode();
    yyerror("Not a valid identifier");
};


type_spec :
/* type specification Integer */
INT {
    savedType = Integer;
}
| /* type specification Void */
VOID {
    savedType = Void;
};


func_declar :
/* function declaration ( int main(void); ) */
type_spec id {
    $$ = newDeclNode( FunK );
    $$->attr.name = (char*)$2;
    if( tbl_insert(Sym, (char*)$2, $$) == -1 ) {
        sprintf(error_buff, "%s already declared in current scope (%d)",
            $$->attr.name, tbl_scope(Sym));
        syntaxError(error_buff);
    }

    tbl_beginScope(Sym);
    $$->type = savedType;
    $$->nestLevel = tbl_scope(Sym);
    if (printSymbols) 
        printf("~~ Function (%s) Begin %d ~~\n", (char*)$2, Sym->scope);
    tbl_insert(Sym, "return", $$); /* reference function return*/
} 
/* parameter list (int x, int j ...) */
L_PAREN params  {
    $3->child[0] = $5; // param
    $$ = $3;
} 
/* statements in function body (compound statements) */
R_PAREN compound_stmt {
    $6->child[1] = $8; // compound statement
    if (printSymbols) 
        printf("~~~ End Function (%s) %d ~~~\n", (char*)$2, Sym->scope);
    $$ = $6;
    endScope();
};


params :
/* parameter list used in function */
param_list {
    $$ = $1;
}
| /* unspecified parameters */
VOID {
    $$ = NULL;
};


param_list :
/* comma seperated parameter list */
param_list COMMA param {
    YYSTYPE t = $1;
    if ( t != NULL ) {
        while ( t->sibling != NULL ) {
            t = t->sibling;
        }
        t->sibling = $3;
        $3->offset = t->offset + cmSize(t);
        $$ = $1;
    } else {
        $$ = $3;
    }
}
| /* single parameter */
param {
    $$ = $1;
    //$$->offset = cmSize($$);
};


param :
/* Definition of single parameter (int x) */
type_spec id {
    $$ = newDeclNode( ParamK );
    $$->type = savedType;
    $$->attr.name = (char*) $2;

    TableIter* iter = itr_new(Sym, $$->attr.name, tbl_scope(Sym));
    if (!itr_done(iter)) {
        sprintf(error_buff, "%s already declared in current scope (%d)", $$->attr.name, tbl_scope(Sym));
        syntaxError(error_buff);
    }
    itr_delete(iter);

    tbl_insert(Sym, $$->attr.name, $$);
}
| /* Definition of single array parameter (int x[]) */
type_spec id L_BRACK R_BRACK {
    $$ = newDeclNode( ParamK );
    $$->type = Array;
    $$->subtype = savedType;
    $$->attr.name = (char*)$2;

    TableIter* iter = itr_new(Sym, $$->attr.name, tbl_scope(Sym));
    if (!itr_done(iter)) {
        sprintf(error_buff, "%s already declared in current scope (%d)", $$->attr.name, tbl_scope(Sym));
        syntaxError(error_buff);
    }
    itr_delete(iter);

    tbl_insert(Sym, $$->attr.name, $$);
};


compound_stmt :
/* Compuund statement */
L_BRACE local_declar stmt_list R_BRACE {
    $$ = newStmtNode( CmpdK );
    $$->child[0] = $2;
    $$->child[1] = $3;
};


local_declar :
/* local variable declaration */
/* empty */ {
    $$ = NULL;
}
| /* Local variable declaration */
local_declar var_declar {
    YYSTYPE t = $1;
    if (t != NULL) {
        while ( t->sibling != NULL ) {
            t = t->sibling;
        }
        t->sibling = $2;
        $2->offset = t->offset + cmSize(t);
        $$ = $1;
    } else {
        $$ = $2;
        //$$->offset = cmSize($$);
    }
};


stmt_list :
/* Empty statement list */
/* empty */ {
    $$ = NULL;
}
| /* Statement list */
stmt_list statement {
    YYSTYPE t = $1;
    if ( t != NULL ) {
        while ( t->sibling != NULL ) {
            t = t->sibling;
        }
        t->sibling = $2;
        $$ = $1;
    } else {
        $$ = $2;
    }
};


statement :
/* Expression statement */
expression_stmt { $$ = $1; } |
{
    tbl_beginScope(Sym);
    if (printSymbols) 
        printf("~~ Block Begin %d ~~\n", Sym->scope);
}
compound_stmt {
    $$ = $2;
    if (printSymbols) 
        printf("~~~ End Block %d ~~~\n", Sym->scope);
    endScope();
}
| /* If statement */
selection_stmt {
    $$ = $1;
}
| /* While statement */
iteration_stmt {
    $$ = $1;
}
| /* Return Statement*/
return_stmt {
    $$ = $1;
};


expression_stmt :
/* complete expression */
expression SEMICOLON {
    $$ = $1;
}
| /* Empty expression */
SEMICOLON {
    $$ = NULL;
};


selection_stmt :
/* If expression with else part */
ifstate | ifstate ELSE statement {
    $$ = $1;
    $$->child[2] = $3;
};


ifstate :
/* If expression without else */
IF  L_PAREN expression R_PAREN statement {
    $$ = newStmtNode( IfK );
    if ( $3->type != Integer ) { 
        syntaxError("If Expression Condition Must be of type int");
    }
    $$->child[0] = $3;
    $$->child[1] = $5;
};
 
 
iteration_stmt :
/* While statement */
WHILE {useMul *= 10;} L_PAREN expression R_PAREN statement { useMul /= 10; } {
    $$ = newStmtNode( WhileK );
    if ( $4->type != Integer ) { 
        syntaxError("While Expression Condition Must be of type int");
    }
    $$->child[0] = $4;
    $$->child[1] = $6;
};


return_stmt :
/* Return statement */
RETURN SEMICOLON {
    YYSTYPE t;
    $$ = newStmtNode( ReturnK );
    if (tbl_lookup(Sym, "return", &t) == 0) {
        if (t->type != Void) {
            sprintf(error_buff, "Function(%s) must return "
            "a %s type.", t->attr.name, typeName(t->type,
                t->subtype));
                syntaxError(error_buff);
        }
    }
}
| /* Return an expression */
RETURN expression SEMICOLON {
    YYSTYPE t;
    $$ = newStmtNode( ReturnK );
    $$->child[0] = $2;
    if (tbl_lookup(Sym, "return", &t) == 0) {
        if (t->type != $2->type) {
            sprintf(error_buff, "Function(%s) must return "
            "a %s type not a %s type", t->attr.name,
            typeName(t->type, t->subtype),
            typeName($2->type, $2->subtype));
                syntaxError(error_buff);
        }
    }
};


expression :
/* Assignment expression */
var ASSIGN expression {
    $$ = newExpNode( OpK );
    $$->child[0] = $1;
    $$->child[1] = $3;
    $$->attr.op = ASSIGN;
    if ($1->type != $3->type) {
        sprintf(error_buff, "Assignment type mismatch, %s %s",
                            typeName($1->type, $1->subtype),
                            typeName($3->type, $3->subtype));
        syntaxError(error_buff);
    }
    
}
| /* Simple expression */
simple_express {
    $$ = $1;
}
| /* Error in expression */
error {
    $$ = newErrorNode();
}
| /* Scanner error */
ERROR {
    $$ = newErrorNode();
};


simple_express :
/* Expression */
add_expr {
    $$ = $1;
    $$->type = $1->type;
    $$->subtype = $1->type;
}
| /* Comparison expression (a == b) */
add_expr EQUAL add_expr {
    $$ = newExpNode( OpK );
    $$->child[0] = $1;
    $$->child[1] = $3;
    $$->attr.op = EQUAL;
    $$->type = Integer;
    if ($1->type != Integer) {
        sprintf(error_buff, "Invalid Type %s for operand on"
                            " LHS of == expected Integer",
                            typeName($1->type, $1->subtype));
        syntaxError(error_buff);
    }
    if ($3->type != Integer) {
        sprintf(error_buff, "Invalid Type %s for operand on"
                            " RHS of == expected Integer",
                            typeName($3->type, $3->subtype));
        syntaxError(error_buff);
    }
}
| /* Comparison expression (a != b) */
add_expr NOT_EQUAL add_expr {
    $$ = newExpNode( OpK );
    $$->child[0] = $1;
    $$->child[1] = $3;
    $$->attr.op = NOT_EQUAL;
    $$->type = Integer;
    if ($1->type != Integer) {
        sprintf(error_buff, "Invalid Type %s for operand on"
                            " LHS of != expected Integer",
                            typeName($1->type, $1->subtype));
        syntaxError(error_buff);
    }
    if ($3->type != Integer) {
        sprintf(error_buff, "Invalid Type %s for operand on"
                            " RHS of != expected Integer",
                            typeName($3->type, $3->subtype));
        syntaxError(error_buff);
    }
}
| /* Comparison expression (a < b) */
add_expr LESS_THAN add_expr {
    $$ = newExpNode( OpK );
    $$->child[0] = $1;
    $$->child[1] = $3;
    $$->attr.op = LESS_THAN;
    $$->type = Integer;
    if ($1->type != Integer) {
        sprintf(error_buff, "Invalid Type %s for operand on"
                            " LHS of < expected Integer",
                            typeName($1->type, $1->subtype));
        syntaxError(error_buff);
    }
    if ($3->type != Integer) {
        sprintf(error_buff, "Invalid Type %s for operand on"
                " RHS of < expected Integer",
            typeName($3->type, $3->subtype));
        syntaxError(error_buff);
    }
}
| /* Comparison expression (a > b) */
add_expr GREATER_THAN add_expr {
    $$ = newExpNode( OpK );
    $$->child[0] = $1;
    $$->child[1] = $3;
    $$->attr.op = GREATER_THAN;
    $$->type = Integer;
    if ($1->type != Integer) {
        sprintf(error_buff, "Invalid Type %s for operand on"
                            " LHS of > expected Integer",
                            typeName($1->type, $1->subtype));
        syntaxError(error_buff);
    }
    if ($3->type != Integer) {
        sprintf(error_buff, "Invalid Type %s for operand on"
                            " RHS of > expected Integer",
                            typeName($3->type, $3->subtype));
        syntaxError(error_buff);
    }
}
| /* Comparison expression (a <= b) */
add_expr LESS_THAN_EQ add_expr {
    $$ = newExpNode( OpK );
    $$->child[0] = $1;
    $$->child[1] = $3;
    $$->attr.op = LESS_THAN_EQ;
    $$->type = Integer;
    if ($1->type != Integer) {
        sprintf(error_buff, "Invalid Type %s for operand on"
                            " LHS of <= expected Integer",
                            typeName($1->type, $1->subtype));
        syntaxError(error_buff);
    }
    if ($3->type != Integer) {
        sprintf(error_buff, "Invalid Type %s for operand on"
                            " RHS of <= expected Integer",
                            typeName($3->type, $3->subtype));
        syntaxError(error_buff);
    }
}
| /* Comparison expression (a >= b) */
add_expr GREATER_THAN_EQ add_expr {
    $$ = newExpNode( OpK );
    $$->child[0] = $1;
    $$->child[1] = $3;
    $$->attr.op = GREATER_THAN_EQ;
    $$->type = Integer;
    if ($1->type != Integer) {
        sprintf(error_buff, "Invalid Type %s for operand on"
                            " LHS of != . expected Integer",
                            typeName($1->type, $1->subtype));
        syntaxError(error_buff);
    }
    if ($3->type != Integer) {
        sprintf(error_buff, "Invalid Type %s for operand on"
                            " RHS of != . expected Integer",
                            typeName($3->type, $3->subtype));
        syntaxError(error_buff);
    }
};


var :
/* Variable */
id {
    $$ = newExpNode( IdK );
    $$->attr.name = (char*) $1;
    YYSTYPE t;
    if (tbl_lookup(Sym,(char*) $1, &t) == 0) {
        $$->type = t->type;
        $$->subtype = t->subtype;
        t->use_count += useMul;
        $$->ref = t;
    } else {
        sprintf(error_buff, "%s Not declared\n", (char*)$1);
        syntaxError(error_buff);
    }
}
| /* Array variable  subscripted */
id L_BRACK expression R_BRACK {
    $$ = newExpNode(SubsK);
    $$->child[0] = newExpNode(IdK);
    $$->child[0]->attr.name = (char*)$1;
    $$->child[1] = $3;

    YYSTYPE t;
    if (tbl_lookup(Sym,(char*)$1, &t) == 0) {
        if (t->type != Array) {
            sprintf(error_buff, "%s is not a subscriptable"
                                "\n", (char*)$1);
            syntaxError(error_buff);
            $$->type = Integer;
        } else {
            $$->child[0]->ref = t;
            $$->child[0]->type =  t->type;
            t->use_count += useMul;
            $$->type = t->subtype;
        }
    } else {
        sprintf(error_buff, "%s Not declared\n", (char*)$1);
        syntaxError(error_buff);
        $$->type = Integer;
    }
};


id :
/* Name of Variable */
ID {
    $$ = (TreeNode*)copyString( token_string );
};


number :
/* Constant integer */
NUMBER {
     $$ = (TreeNode*)copyString( token_string );
};


add_expr :
/* addition expression ( a + b ) */
add_expr ADD term {
    $$ = newExpNode( OpK );
    $$->child[0] = $1;
    $$->child[1] = $3;
    $$->attr.op = ADD;
    $$->type = Integer;
    if ($1->type != Integer) {
        sprintf(error_buff, "Invalid Type %s for operand on"
                            " LHS of + expected Integer",
                            typeName($1->type, $1->subtype));
        syntaxError(error_buff);
    }
    if ($3->type != Integer) {
        sprintf(error_buff, "Invalid Type %s for operand on"
                            " RHS of + expected Integer",
                            typeName($3->type, $3->subtype));
        syntaxError(error_buff);
    }
}
| /* subtraction expression (a - b) */
add_expr SUBTRACT term {
    $$ = newExpNode( OpK );
    $$->child[0] = $1;
    $$->child[1] = $3;
    $$->attr.op = SUBTRACT;
    
    $$->type = Integer;
    if ($1->type != Integer) {
        sprintf(error_buff, "Invalid Type %s for operand on"
                            " LHS of - expected Integer",
                            typeName($1->type, $1->subtype));
        syntaxError(error_buff);
    }
    if ($3->type != Integer) {
        sprintf(error_buff, "Invalid Type %s for operand on"
                            " RHS of - expected Integer",
                            typeName($3->type, $3->subtype));
        syntaxError(error_buff);
    }
}
| /* single term */
term {
    $$ = $1;
};


term :
/* multiplication expression (a * b) */
term STAR factor {
    $$ = newExpNode( OpK );
    $$->child[0] = $1;
    $$->child[1] = $3;
    $$->attr.op = STAR;
    
    $$->type = Integer;
    if ($1->type != Integer) {
        sprintf(error_buff, "Invalid Type %s for operand on"
                            " LHS of * expected Integer",
                            typeName($1->type, $1->subtype));
        syntaxError(error_buff);
    }
    if ($3->type != Integer) {
        sprintf(error_buff, "Invalid Type %s for operand on"
                            " RHS of * expected Integer",
                            typeName($3->type, $3->subtype));
        syntaxError(error_buff);
    }
}
| /* division expression (a / b) */
term DIVISION factor {
    $$ = newExpNode( OpK );
    $$->child[0] = $1;
    $$->child[1] = $3;
    $$->attr.op = DIVISION;
    
    $$->type = Integer;
    if ($1->type != Integer) {
        sprintf(error_buff, "Invalid Type %s for operand on"
                            " LHS of / expected Integer",
                            typeName($1->type, $1->subtype));
        syntaxError(error_buff);
    }
    if ($3->type != Integer) {
        sprintf(error_buff, "Invalid Type %s for operand on"
                            " RHS of /  expected Integer",
                            typeName($3->type, $3->subtype));
        syntaxError(error_buff);
    }
}
| /* single term */
factor {
    $$ = $1;
};


factor :
/* bracketed expression ( (a) ) */
L_PAREN expression R_PAREN {
    $$ = newExpNode( OpK );
    $$->child[0] = $2;
    $$->attr.op = PAREN;
    $$->type = $2->type;
    $$->subtype = $2->subtype;
}
| /* variable term */
var {
    $$ = $1;
}
| /* function call */
call {
    $$ = $1;
}
| /* constant number term */
NUMBER {
    $$ = newExpNode(ConstK);
    $$->attr.val = atoi( token_string );
    $$->type = Integer;
};


call :
/* function call */
id L_PAREN args R_PAREN {
    $$ = newExpNode( CallK );
    $$->attr.name = (char*) $1;
    $$->child[0] = $3;
    YYSTYPE t;
 
    if (tbl_lookup(Sym, (char*) $1, &t) == 0) {
        $$->type = t->type;
        $$->subtype = t->subtype;
        $$->ref = t; 
        int argc = 0;
        YYSTYPE param = t->child[0];
        YYSTYPE argv = $3;
        
        while (argv) {
            if (param == NULL) {
                syntaxError("Too many parameters");
                break;
            } else if (argv->type != param->type && argv->subtype != param->subtype) {
                    sprintf(error_buff, "parameter %d of %s"
                                        " does not match declaration", 
                                        argc,(char*)$1);
                    syntaxError(error_buff);
                }
           argc++;
           param = param->sibling;
           argv = argv->sibling;
         }
         if (param) {
                syntaxError("Too few parameters");
                break;
         }
        
    } else {
        $$->type = Integer;
        fprintf(stderr, "Function(%s) not declared.\n", (char*) $1);
    }
};


args :
/* empty argument list for function call */
/* empty */ {
    $$ = NULL;
}
| /* argument list for function call */
arg_list {
    $$ = $1;
};


arg_list :
/* comma seperated argument list for function call */
arg_list COMMA expression {
    YYSTYPE t = $1;
    if ( t != NULL ) {
        while ( t->sibling != NULL ) {
            t = t->sibling;
        }
        t->sibling = $3;
        $$ = $1;
        
    } else {
        $$ = $3;
    }
}
| /* individual expression in argument list for function call */
expression {
    $$ = $1;
}
