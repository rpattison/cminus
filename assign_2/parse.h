/**
@file parse.h
@brief The parser interface for the CMinus compiler
*/
  
#ifndef _PARSE_H_
#define _PARSE_H_

#ifdef __cplusplus
extern "C" {
#endif
  
/**
Returns the newly constructed syntax tree
*/
extern TreeNode* parse( void );

extern int yyparse( void );

#ifdef __cplusplus
}
#endif

#endif
