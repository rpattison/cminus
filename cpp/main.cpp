/**
 * @file main.c
 * @brief Executes the main fuction which opens the source file, parses it and displays the syntax tree.
 */


#include <stdio.h>

#include <getopt.h>
#include <iostream>
#include <fstream>

#include "Headers.hpp"
#include "parse.h"

using namespace std;

FILE *listing = stdout;
int lineno = 0;			///< the line number in the current document
FILE *source;			///< the file stream for the source file used in tokenizing
//fstream listing(cout); ///< the output stream for displaying the parse tree

TreePrint printer;

/**
    The main function which interacts with the command line and processes the user's request
*/
int
main (int argc, char **argv)
{

  int opt;

  if (argc != 3)
    {
      cerr << "usage: " << string (argv[0]) << " -[asc] <filename>" << endl;
      return 1;
    }


  char pgm[120];		/* source code file name */

  strcpy (pgm, argv[2]);

  if (strchr (pgm, '.') == NULL)
    {
      strcat (pgm, ".cm");
    }

  source = fopen (pgm, "r");

  if (source == NULL)
    {
      cerr << "File " << string (pgm) << " not found" << endl;
      return -1;
    }

  opt = getopt (argc, argv, "ascp");
  switch (opt)
    {
    case 'a':
      {
	cout << endl << "C Minus COMPILATION: " << string (pgm) << endl;
	/*
	   if ( 0 == 2 ) {
	   return 1;
	   } 
	 */
	//NilExp nil(0);
	//ExpList exps;
	//VarDecList vars;
	//CompoundExp cmp(0, &vars, &exps);
	//IfExp iff(0, &nil, &cmp, &nil);
	//TreePrint tp;
	//iff.accept(&tp);
	Absyn* syntaxTree = parse ();
	//Absyn* syntaxTree = parse();
	 //printTree( syntaxTree );
	 syntaxTree->accept(&printer);
      }
      break;
    case 's':
      cerr << endl << "This feature is under construction" << endl;
      break;
    case 'c':
      cerr << endl << "This feature is under construction" << endl;
      break;
    case 'p':
      //syntaxTree = parse();
      //printTreeColour( syntaxTree );
      break;
    default:
      cerr << "usage:" << string (argv[0]) << "-[asc] <filename>" << endl;
      break;
    }


  fclose (source);

  return (0);
}

