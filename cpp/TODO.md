
## C++ implementation

So far, the C++ implementation will compile and execute.
However, it will correctly print (without segfaulting) in very few cases.
The cause of these segfaults is due to the rules for the *list* types
(VarDecList, ExpList, etc.). In the C implementation, list items and lists
are the same type and the current rules work. In C++ however, an Expression
and an ExpList are two different types and so a rule like

    param_list : param_list COMMA param | param 

causes problems since it will return a param object **or** a param_list
depending on the length (A length 1 list will return a param).
The fix would be to replace these with lists and instead of appending a param
to a list in the `param_list COMMA param` code we should *extend* param_list
to add the *list* param and make `param` return a single item list. This issue
must have been dealt with in the java implementation sample code so we should
follow its solution.

Still missing error nodes.

## C implementation

So far, the C code is at checkpoint 1, with a few improvements that need to be done. Error handling for other cases needs to be added---some discussion is on the bitbucket page. The most troubling is the need for an array type which is separate from the integer type. Currently we use the prescence of a child node storing the size to determine that it is an array data type. However, the size of the array is not given in function parameters and so we have no way of distinguishing between the two. Solutions are to create a new array type or add a NilExpression to be the size in the function parameter case. Only one of these is necessary but both is the cleanest implmentation.


