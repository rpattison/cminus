#ifndef HEADERS_H_
#define HEADERS_H_

#include <string.h>

using namespace std;

// Abstract syntax
#include "Headers/Absyn.hpp"

// Expressions
#include "Headers/Exp.hpp"

// Variables
#include "Headers/Var.hpp"

// Declarations
#include "Headers/Dec.hpp"

// Variables declarations
#include "Headers/VarDec.hpp"

// Miscellaneous
#include "Headers/Misc.hpp"

// Visitor
#include "Headers/Visitors.hpp"

// Util
#include "Headers/Util.hpp"

#endif
