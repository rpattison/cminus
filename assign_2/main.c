/**
 * @file main.c
 * @brief Executes the main fuction which opens the source file, parses it and displays the syntax tree.
 */
#include <getopt.h>

#include "globals.h"
#include "util.h"
#include "parse.h"
#include "generator.h"
#include "Hash_types.h"
#include "Hash.h"


#define TABLE_SIZE 101

int lineno; ///< the line number in the current document
FILE* source; ///< the file stream for the source file used in tokenizing
FILE* listing; ///< the output stream for displaying the parse tree
FILE* sourceCode; ///< input stream from command line argument
FILE* code;
Table* Sym; ///< symbol table
int printSymbols; ///< print symbols?
int TraceCode = 1;
/*
    The main function which interacts with the 
    command line and processes the user's request
*/
int main (int argc, char * argv[]) {
    TreeNode* syntaxTree;
    int opt;
    
    code = stdout;
    printSymbols = 0;
    lineno = 0;
    
    Sym = tbl_new(TABLE_SIZE);
    
    
    TreeNode* input = newDeclNode(FunK);
    input->attr.name = "input";
    input->type = Integer;
    input->address = 4;
    
    TreeNode* output = newDeclNode(FunK);
    output->attr.name = "output";
    output->type = Void;
    output->child[0] = newDeclNode(ParamK);
    output->child[0]->type = Integer;
    output->child[0]->offset = 1;
    output->attr.name = "value";
    output->address = 7;
    
    tbl_insert(Sym, "input", input);
    tbl_insert(Sym, "output", output);
    
    if ( argc != 3 ) {
        fprintf( stderr, "usage: %s -[asc] <filename>\n", argv[0] );
        exit( 1 );
    }
    
    
    char pgm[120]; /* source code file name */
    
    strcpy( pgm, argv[2] );
    
    if ( strchr( pgm, '.' ) == NULL ) {
        strcat( pgm, ".cm" );
    }
    
    source = fopen( pgm, "r" );
    sourceCode = fopen(pgm, "r");
    
    if ( source == NULL ) {
        fprintf( stderr, "File %s not found\n", pgm );
        exit( 1 );
    }
    
    listing = stdout; /* send listing to screen */
    
    
    opt = getopt(argc, argv, "ascp");
    switch(opt) {
        case 'a':
            fprintf( listing, "\nC Minus COMPILATION: %s\n", pgm);
            syntaxTree = parse();
            printTree( syntaxTree );
            break;
        case 's':
            printSymbols = 1;
            syntaxTree = parse();
            break;
        case 'c':
            fprintf( listing, "\n\tGenerating assembly language code\n");
            syntaxTree = parse();
            generateASM(syntaxTree);
            break;
        case 'p':
            syntaxTree = parse();
            //printTreeColour( syntaxTree );
            break;
        default:
            fprintf( stderr, "usage: %s -[asc] <filename>\n", argv[0] );
            break;
    }
    
    
    fclose( source );
    
    return ( 0 );
}
