
class ASTVisitor {
public:
  ASTVisitor() {
    nested_ = 0
  }
  
  virtual void visit(  Exp        &node) =0 ;
  virtual void visit(  Dec        &node) = 0;
  virtual void visit(  Var        &node) = 0;
  virtual void visit(  VarDec     &node) = 0;
  virtual void visit(  SimpleVar  &node) = 0;
  virtual void visit(  IndexVar   &node) = 0;
  virtual void visit(  NilExp     &node) = 0;
  virtual void visit(  VarExp     &node) = 0;
  virtual void visit(  IntExp     &node) = 0;
  virtual void visit(  CallExp    &node) = 0;
  virtual void visit(  OpExp      &node) = 0;
  virtual void visit(  AssignExp  &node) = 0;
  virtual void visit(  IfExp      &node) = 0;
  virtual void visit(  WhileExp   &node) = 0;
  virtual void visit(  ReturnExp  &node) = 0;
  virtual void visit(  CompoundExp &node) = 0;
  virtual void visit(  WriteExp   &node) = 0;
  virtual void visit(  ReadExp    &node) = 0;
  virtual void visit(  FunctionDec  &node) = 0;
  virtual void visit(  SimpleDec  &node) = 0;
  virtual void visit(  ArrayDec   &node) = 0;
  virtual void visit(  NameTy     &node) = 0;
  virtual void visit(  ExpList    &node) = 0;
  virtual void visit(  DecList    &node) = 0;
  virtual void visit(  VarDecLonst &node) = 0;
  private:
  int nested_;
}