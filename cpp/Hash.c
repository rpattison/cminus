#include <stdlib.h>
#include <string.h>

#include "Hash.h"
#include "Hash_types.h"

/**< shift value for hash **/
#define SHIFT 4


/** From professor's notes and slides. **/
static int hash(const Table* T, const char* key) {
    int temp = 0;
    int i = 0;

    while (key[i] != '\0') {
        temp = ((temp << SHIFT) + key[i]) % T->size;
        i++;
    }

    return temp;
}


static Hash* newCell(const char* key, table_value value, int scope) {
    Hash* H = (Hash*)malloc(sizeof(Hash));
    if (H == NULL) return NULL;
    H->key = strdup(key);
    if (H->key == NULL) {
        free(H);
        return NULL;
    }
    H->value = value;
    H->scope = scope;
    H->next = NULL;
    return H;
}


static void deleteCell(Hash* cell) {
    free((void*)cell->key);
    free(cell);
}


int itr_step(TableIter* iter) {
    int new_cell = 0;

    if (iter == NULL || (iter->current == NULL &&
            iter->cell_index >= iter->T->size)) {
        return 0;
    }

    do {
        new_cell = 0;
        while (iter->current == NULL) {
            iter->cell_index++;
            /* if new cell, and searching key, key DNE anywhere */
            if (iter->key) { /* optimize for key search */
                iter->current = NULL;
                iter->cell_index = iter->T->size;
                return 0;
            }
            if (iter->cell_index >= iter->T->size) {
                return 0;
            }
            iter->current = iter->T->cell[iter->cell_index];
            new_cell = 1;
        }

        if (new_cell == 0)
            iter->current = iter->current->next;
    }  while (iter->current == NULL || /* not end of cell list */
                /*not a scope we're looking for '*/
              (iter->scope != iter->current->scope && iter->scope >= 0) ||
              /* not a key we're looking for'*/
              (iter->key && strcmp(iter->key, iter->current->key) != 0));

    return 1;
}


TableIter* itr_new(Table* T, const char* key, const int scope) {
    TableIter* iter;
    if (T == NULL) return NULL;
    iter = (TableIter*)malloc(sizeof(TableIter));
    if (iter == NULL) return NULL;
    iter->cell_index = key ? hash(T, key) : 0;
    iter->current = T->cell[iter->cell_index];
    iter->scope = scope;
    iter->key = key ? strdup(key) : NULL;
    iter->T = T;
    T->TableIters++;

    if (iter->current == NULL)  {
        itr_step(iter);
        return iter;
    }

    if (key && strcmp(key, iter->current->key) != 0) {
        itr_step(iter);
        return iter;
    }

    if (scope >= 0 && iter->current->scope != scope ) {
        itr_step(iter);
        return iter;
    }

    return iter;
}


int itr_done(const TableIter* iter) {
    return iter == NULL || iter->current == NULL;
}


void itr_delete(TableIter* iter) {
    if (iter == NULL)
        return;
    if (iter->T)
        iter->T->TableIters--;
    free((void *)iter->key);
    free(iter);
}


table_value itr_value(const TableIter* iter) {
    table_value fake;
    if (iter == NULL || iter->current == NULL)
        return fake;
    return iter->current->value;
}


int itr_scope(const TableIter* iter) {
    if (iter == NULL || iter->current == NULL)
        return -1;
    return iter->current->scope;
}


const char* itr_key(const TableIter* iter) {
    if (iter == NULL || iter->current == NULL)
        return NULL;
    return iter->current->key;
}


int tbl_insert(Table* T, const char* key, table_value value) {
    int cell = -1;
    Hash* H = NULL;
    Hash* new_cell = NULL;

    if (T == NULL || key == NULL || T->TableIters > 0) return -2;

    cell = hash(T, key);
    H = T->cell[cell];
    while (H != NULL) {
        if (strcmp(key, H->key) == 0 && H->scope == T->scope) {
            return -1;
        }
        H = H->next;
    }

    new_cell = newCell(key, value, T->scope);
    if (new_cell == NULL) return -3;
    new_cell->next = T->cell[cell];
    T->cell[cell] = new_cell;
    return T->scope;
}


int tbl_lookup(const Table* T, const char* key, table_value* value) {
    int cell = -1;
    Hash* H = NULL;
    Hash* found = NULL;

    if (T == NULL || key == NULL || value == NULL) return -2;

    cell = hash(T, key);
    H = T->cell[cell];
    while (H != NULL) {
        if (strcmp(key, H->key) == 0) {
            if (found) {
                if (found->scope < H->scope) {
                    found = H;
                }
            } else {
                found = H;
            }
        }
        H = H->next;
    }

    if (found == NULL) {
        return 0;
    } else {
        *value = found->value;
        return found->scope;
    }
}


int tbl_scope(const Table* T) {
    if (T == NULL) return -1;
    return T->scope;
}


int tbl_beginScope(Table* T) {
    if (T == NULL || T->TableIters > 0) return -1;
    return 0;
}


int tbl_endScope(Table * T){
    Hash* cell = NULL;
    Hash* prev = NULL;
    int i = 0;

    if (T == NULL || T->scope <= 0 || T->TableIters > 0)
        return -2;

    T->scope--;

    for (i = 0; i < T->size; ++i) {
        cell = T->cell[i];
        while (cell && cell->scope > T->scope) {
            T->cell[i] = cell->next;
            deleteCell(cell);
            cell = T->cell[i];
        }

        if (cell == NULL)
            continue;
        prev = cell;
        cell = cell->next;
        while (cell) {
            /* re arrange pointers here TODO*/
            if (cell->scope > T->scope) {
                prev->next = cell->next;
                deleteCell(cell);
                cell = prev->next;
            } else {
                prev = cell;
                cell = cell->next;
            }
        }
    }
    return 0;
}


Table* tbl_new(int size) {
    Table* T = NULL;
    if (size <= 0) return NULL;
    T = (Table*)malloc(sizeof(Table));
    if (T == NULL) return NULL;
    T->cell = (Hash**)calloc(size, sizeof(Hash*));
    T->TableIters = 0;
    if (T->cell == NULL)  {
        free(T);
        return NULL;
    }

    T->size = size;
    T->scope = 0;
    return T;
}


void tbl_delete(Table* T) {
    Hash* cell = NULL;
    Hash* prev = NULL;
    int i = 0;

    if (T == NULL) return;
    for (i = 0; i < T->size; ++i) {
        cell = T->cell[i];
        prev = cell;
        while (cell) {
            prev = cell;
            cell = cell->next;
            deleteCell(prev);
        }
    }

    free(T->cell);
    free(T);
}
