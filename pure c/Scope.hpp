#ifndef __Scope_HPP__
#define __Scope_HPP__ 1

#include <list>
#include <map>

using namespace std;

/**
* Scope
* @todo, currently there is no way to find what scope level
* a variable is defined at.
* @todo provide some way of printing the current scope variables (for compiler assignment) (iterators for different levels).
* @todo make the exceptions store useful information, previous value new value and the key. (safely preferably)
* @todo cleanup the type names with typedefs
*/
template<class K, class V> 
class Scope {
public:
  //< Exception for looking up a key that is not defined in ay scope.
  class UnknownKey {
    public:
    UnknownKey(K key) {
        key_ = key;
    }

    K getKey() const {
        return key_;
    }
    
    private:
      K key_;
  }; 

  //< Exception for declaring 2 things with same name in same scope.
  class Redeclaration {
    Redeclaration(K key, V newVal, V oldVal ) {
        key_ = key;
        oldVal_ = oldVal;
        newVal_ = newVal;
    }

    K getKey() const {
        return key_;
    }
    
    V getOldVal() const {
        return oldVal_;
    }
    
    V getNewVal() const {
        return newVal_;
    }

    private:
        K key_;
        V oldVal_;
        V newVal_;
  }; 
  class NoScope { }; //< Exception for operating on 'No scope'
  
  Scope<K, V>() { }
  
  ~Scope<K, V>() {
    while (getScopeLevel() > 0) endScope(); /* delete hash maps memory */
  }
  
  /**
  * Begins a new scope.
  * Must be called after initialization 
  * and before attempting to insert.
  **/
  void beginScope() {
    tables_.push_front(new map<K,V>());
  }
  
  /**
  * Ends the current scope, discarding 
  * the definitions defined in it.
  * @throw NoScope - if asks to end a scope at level 0.
  */
  void endScope() {
    if (tables_.empty())
      throw NoScope();
    delete tables_.front();
    tables_.pop_front();
  }
  
  /**
  * Returns the current scope level.
  * @note 0 is *not* a scope level,
  * it denote no scope.
  */
  int getScopeLevel() const {
    return tables_.size();
  } 
  
  /**
  * Looks up the definition for Key
  * by moving up the chain of scopes
  * to get the nearest definition.
  * @param key the name given to a definiton in @ref insert
  * @return the defintion assocoiated with Key
  * @throw UnknownKey if there is no definition for Key in any scope.
  */
  V lookup(K key) const {
    if (getScopeLevel() < 1) throw NoScope();
    typename list<map<K,V>* >::const_iterator t;
    for (t = tables_.begin(); t != tables_.end(); ++t ) {
      typename map<K,V>::const_iterator find;
      find = (*t)->find(key);
      if (find != (*t)->end())
        return find->second;
    }
    throw UnknownKey(key);
  }
  
  /**
  * Associates, or defines, a key to a definition in the current scope.
  * @param key the Key, or name, to give the definition
  * @param value the value or definition for the key
  * @throw Redeclaration if the key is already defined in the current scope.
  * @throw NoScope if there is no current scope.
  */
  void insert(K key, V value) {
    if (tables_.empty())
      throw NoScope();
    map<K,V>* table = tables_.front();
    typename map<K,V>::const_iterator pair = table->find(key);
    if (pair != table->end())
      throw Redeclaration(key, value, pair->second);
    else
     table->insert(make_pair(key, value));
  }
  
private:
  list<map<K,V>* > tables_;
};

#endif
