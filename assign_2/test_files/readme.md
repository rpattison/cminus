# Sample C Minus Files #  

Note that none of the following programs are well written,
they are almost all heavily reliant on recusion and being given only 
valid input. However, for our purposes, they are written in C- and are relatively small enough to use as input and verifying syntax and token scanning.

Note: to compile any of these as C executables, you will need to add `#include "io.h"` to the top of the `.cm` file and link it with the `io` module.

TODO: write a makefile or python script that will automate the building of C executables and possibly verify them against the compiler.

TODO: verify the token output from scanning the `.cm` files and save the output
for regression testing.


# Supporting Files #

io.*
:	used for compiling cminus programs as c executables for testing.

# Test Programs #

fib
:	outputs the first n fobonacci numbers.

combi
:	calculates some combinatorial functions, permutation combinations, with and without replacement.

sort_test
:	input a list of numbers (max length 10), outputs 1 if the list is sorted, 0 otherwise.

stack
:	an interactive stack program, PUSH, POP, query for the size of a stack of integers.

det
:	Calculates the determinant of a 3 x 3 matrix.

# Files from the textbook #

EuclidGCD
:	outputs the greatest common divisor of two integers.

sort
:	performs a selection sort of a list of size 10.
