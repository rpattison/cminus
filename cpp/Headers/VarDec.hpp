#ifndef __VAR_DEC_H__
#define __VAR_DEC_H__

#include "Absyn.hpp"
#include <string>

class VarDec : public Absyn {
public:
    VarDec(int pos,   NameTy* type, std::string* name) : Absyn(pos),
        typ_(type) {
      name_ = name;
    }

     
    void accept(ASTVisitor* visitor);

    std::string* getName() {
        return name_;
    }

    NameTy* getType() {
        return typ_;
    }
private:
    std::string* name_;
    NameTy* typ_;
};


class SimpleDec : public VarDec {
public:
    SimpleDec(int pos,   NameTy* typ, std::string* name) :
        VarDec(pos, typ, name) {}
     
    void accept(ASTVisitor* visitor);
};


class ArrayDec  : public VarDec {
public:
    ArrayDec(int pos,   NameTy* typ, std::string* name, IntExp* size=NULL)
        : VarDec(pos, typ, name), size_(size) {
    }

     
    void accept(ASTVisitor* visitor);

    IntExp* getSizeExpression() {
        return size_;
    }
private:
    IntExp* size_;
};

#endif /* __VAR_DEC_H__ */

