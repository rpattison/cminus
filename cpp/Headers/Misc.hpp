#ifndef MISC_HPP
#define MISC_HPP 1

class Dec;
class Exp;
class ASTVisitor;

#include <iostream>

class ExpList {
public:
    ExpList(Exp* head, ExpList* tail = NULL) : head_(head) {
        tail_ = tail;
    }

    void accept(ASTVisitor* visitor);

    Exp* getHead() {
        return head_;
    }

    ExpList* getNext() {
      return tail_;
    }


    void append(Exp* new_tail) {
        ExpList* list = this;
        while (list->getNext())
            list = list->getNext();
        list->tail_ = new ExpList(new_tail);
    }

    ExpList* getTail() {
        ExpList* tail = this;
        while (tail->getNext())
            tail = tail->getNext();
        return tail;
    }
private:
    Exp* head_;
    ExpList* tail_;
};


class DecList {
public:
    DecList(Dec* head, DecList* tail = NULL) : head_(head) {
        if (head == NULL) {
            std::cerr << "NULL HEAD DECLIST" << std::endl;
        }
        tail_ = tail;
    }

    void accept(ASTVisitor* visitor);

    Dec* getHead() {
        return head_;
    }

    DecList* getNext() {
        return tail_;
    }

    void append(Dec* new_tail) {
        DecList* list = this;
        while (list->getNext())
            list = list->getNext();
        list->tail_ = new DecList(new_tail);
    }

     DecList* getTail() {
         DecList* tail = this;
        while (tail->getNext())
            tail = tail->getNext();
        return tail;
    }
private:
    Dec* head_;
    DecList* tail_;
};


class VarDecList {
public:
    VarDecList(VarDec* head, VarDecList* tail = NULL) : head_(head) {
        tail_ = tail;
    }

    void accept(ASTVisitor* visitor);

    VarDec* getHead() {
        return head_;
    }

    VarDecList* getNext() {
        return tail_;
    }

    void append(VarDec* new_tail) {
        VarDecList* list = this;
        while (list->getNext())
            list = list->getNext();
        list->tail_ = new VarDecList(new_tail);
    }
private:
    VarDec* head_;
    VarDecList* tail_;
};

#endif




