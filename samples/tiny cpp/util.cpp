/****************************************************/
/* File: util.cpp                                   */
/* Utility functions for the TINY scanner           */
/* Fei Song                                         */
/****************************************************/

#include "util.h"

// prints a token and its value to the output file
void printToken( FILE * listing, TokenType token, TokenStruct & tokenStruct ) { 
  switch (token) { 
    case IF: fprintf( listing, "IF\n" ); break;
    case THEN: fprintf( listing, "THEN\n" ); break;
    case ELSE: fprintf( listing, "ELSE\n" ); break;
    case END: fprintf( listing, "END\n" ); break;
    case REPEAT: fprintf( listing, "REPEAT\n" ); break;
    case UNTIL: fprintf( listing, "UNTIL\n" ); break;
    case READ: fprintf( listing, "READ\n" ); break;
    case WRITE: fprintf( listing, "WRITE\n" ); break;
    case ASSIGN: fprintf( listing, ":=\n" ); break;
    case LT: fprintf( listing, "<\n" ); break;
    case GT: fprintf( listing, ">\n" ); break;
    case EQ: fprintf( listing, "=\n" ); break;
    case LPAREN: fprintf( listing, "(\n" ); break;
    case RPAREN: fprintf( listing, ")\n" ); break;
    case SEMI: fprintf( listing, ";\n" ); break;
    case PLUS: fprintf( listing, "+\n" ); break;
    case MINUS: fprintf( listing, "-\n" ); break;
    case TIMES: fprintf( listing, "*\n" ); break;
    case OVER: fprintf( listing, "/\n" ); break;
    case ENDFILE: fprintf( listing, "EOF\n" ); break;
    case NUM:
      fprintf( listing, "NUM, val = %s\n", tokenStruct.value.c_str() );
      break;
    case ID:
      fprintf( listing, "ID, name = %s\n", tokenStruct.value.c_str() );
      break;
    case ERROR:
      fprintf( listing, "ERROR: %s\n", tokenStruct.value.c_str() );
      break;
    default: // should never happen 
      fprintf( listing, "Unknown token: %d\n", token );
  }
}
