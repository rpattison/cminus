/** determinant of a 3 by 3 matrix **/

int ORDER;
int grid[9];
ORDER = 3;

int det( ) {
	int x; int y; int z; int u; int v; int w;
	u = a[0][0] * a[1][1] * a[2][2];
	v = a[0][1] * a[1][2] * a[2][0];
	w = a[0][2] * a[1][0] * a[2][1];
	x = a[0][2] * a[1][1] * a[2][0];
	y = a[0][1] * a[1][0] * a[2][2];
	z = a[0][0] * a[1][2] * a[2][1];
	return ( u + v + w - x - y - z );
}

void main( void ) {
	int x;
	int y;
	
	x = 0;
	while ( x < ORDER ) {
		y = 0;
		while ( y < ORDER ) {
			grid[x][y] = input();
			y = y + 1;
		}
		x = x + 1;
	}
	
	output( det() );
}
