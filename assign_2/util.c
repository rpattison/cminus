/**
 * Utility file.
 */

#include "globals.h"
#include "util.h"
#include <ctype.h>
#include "y.tab.h"
#include "Hash.h"

const char * ifnn(const char *str) {
return (str) ? (str) : "NULL";
}

int cmSize(TreeNode* t) {
    if (t->kind.dec == FunK) {
        return 0;
        fprintf(stderr, "Unknown size for type function\n");
    }
    if (t->type == Integer || t->type == Void) {
        return 1;
    } else if (t->type == Array) {
        if (t->child[0] == NULL)
            return 1; /* just a reference */
        else
            return t->child[0]->attr.val * 1;
    } else {
        fprintf(stderr, "Unknown size for type\n");
    }
    return 0;
}

/**
 prints a token and its lexeme to the listing file
 */
void printToken( int token, const char* token_string ){
    switch ( token ) {
        case IF:
            fprintf(listing, "IF\n");
            break;
        case ELSE:
            fprintf(listing, "ELSE\n");
            break;
        case WHILE:
            fprintf(listing, "WHILE\n");
            break;
        case RETURN:
            fprintf(listing, "RETURN\n");
            break;
        case VOID:
            fprintf(listing, "VOID\n");
            break;
        case INT:
            fprintf(listing, "INT\n");
            break;
        case PAREN:
            fprintf(listing, "PAREN\n");
            break;
        case L_PAREN:
            fprintf(listing, "L_PAREN\n");
            break;
        case R_PAREN:
            fprintf(listing, "R_PAREN\n");
            break;
        case L_BRACK:
            fprintf(listing, "L_BRACK\n");
            break;
        case R_BRACK:
            fprintf(listing, "R_BRACK\n");
            break;
        case L_BRACE:
            fprintf(listing, "L_BRACE\n");
            break;
        case R_BRACE:
            fprintf(listing, "R_BRACE\n");
            break;
        case ADD:
            fprintf(listing, "ADD\n");
            break;
        case SUBTRACT:
            fprintf(listing, "SUBTRACT\n");
            break;
        case ASSIGN:
            fprintf(listing, "ASSIGN\n");
            break;
        case EQUAL:
            fprintf(listing, "EQUAL\n");
            break;
        case NOT_EQUAL:
            fprintf(listing, "NOT_EQUAL\n");
            break;
        case DIVISION:
            fprintf(listing, "DIVISION\n");
            break;
        case STAR:
            fprintf( listing, "STAR\n" );
            break;
        case LESS_THAN:
            fprintf(listing, "LESS_THAN\n");
            break;
        case GREATER_THAN:
            fprintf(listing, "GREATER_THAN\n");
            break;
        case SEMICOLON:
            fprintf(listing, "SEMICOLON\n");
            break;
        case COMMA:
            fprintf(listing, "COMMA\n");
            break;
        case GREATER_THAN_EQ:
            fprintf(listing, "GREATER_THAN_EQ\n");
            break;
        case LESS_THAN_EQ:
            fprintf( listing, "GREATER_THAN_EQ\n" );
            break;
        case ID:
            fprintf( listing, "ID(%s)\n", ifnn(token_string));
            break;
        case NUMBER:
            fprintf( listing, "NUMBER(%s)\n", ifnn(token_string));
            break;
        case ENDFILE:
            break;
        case ERROR:
            fprintf( listing, "ERROR(%s)\n", ifnn(token_string));
            break;
        default: /* should never happen */
            fprintf( listing, "Unknown(%s)\n", ifnn(token_string));
    }
}

/**
 creates a new statement node for syntax tree construction
 */
TreeNode* newStmtNode(StmtKind kind) {
    TreeNode * t = (TreeNode *) malloc(sizeof(TreeNode));
    int i;
    if (t == NULL)
        fprintf(listing,"Out of memory error at line %d\n",lineno);
    else {
        for (i = 0; i < MAXCHILDREN; i++) {
            t->child[i] = NULL;
        }
        t->nestLevel = 0;
        t->use_count = 0;
        t->offset = 0;
        t->ref = NULL;
        t->sibling = NULL;
        t->nodekind = StmtK;
        t->kind.stmt = kind;
        t->lineno = lineno;
        t->subtype = Integer;
    }
    return t;
}

/**
 creates a new Declaration node for syntax tree construction
 */
TreeNode* newDeclNode( DecKind kind ) {
    TreeNode* t = (TreeNode *) malloc( sizeof( TreeNode ) );
    int i;
    if (t == NULL)
        fprintf( listing, "Out of memory error at line %d\n", lineno );
    else {
        for ( i = 0; i < MAXCHILDREN; i++ ) {
            t->child[i] = NULL;
        }
        t->nestLevel = 0;
        t->use_count = 0;
        t->offset = 0;
        t->ref = NULL;
        t->sibling = NULL;
        t->nodekind = DecK;
        t->kind.dec = kind;
        t->lineno = lineno;
        t->subtype = Integer;
    }
    return t;
}


/**
 newExpNode creates a new expression node for syntax tree construction
 */
TreeNode * newErrorNode(  ) {
    return newExpNode( ErrK );
}

/**
 newExpNode creates a new expression node for syntax tree construction
 */
TreeNode * newExpNode(ExpKind kind) {
    TreeNode * t = (TreeNode *) malloc(sizeof(TreeNode));
    int i;
    if (t == NULL)
        fprintf(listing,"Out of memory error at line %d\n",lineno);
    else {
        for (i = 0; i < MAXCHILDREN; i++) {
            t->child[i] = NULL;
        }
        t->nestLevel = 0;
        t->use_count = 0;
        t->offset = 0;
        t->ref = NULL;
        t->sibling = NULL;
        t->nodekind = ExpK;
        t->kind.exp = kind;
        t->lineno = lineno;
        t->type = Void;
        t->subtype = Integer;
    }
    return t;
}

/**
 allocates and makes a new copy of an existing string
 a duplicate of *strdup* ?
 */
char* copyString(const char* s) {
    int n;
    char* t;

    if ( s == NULL ) {
        return NULL;
    }

    n = strlen( s ) + 1;
    t = (char *)malloc( n * sizeof( char ) );

    if ( t == NULL ) {
        fprintf( listing, "Out of memory error at line %d\n", lineno);
        return NULL;
    } else {
        strcpy( t, s );
        return t;
    }
}

/*
 used by printTree to store current number of spaces to indent
 */
static int indentno = 0;

/* macros to increase/decrease indentation */
#define INDENT indentno += 2
#define UNINDENT indentno -= 2

/*
 printSpaces indents by printing spaces
 */
static void printSpaces(void) {
    int i;
    for (i = 0; i < indentno; i++)
        fprintf(listing , " ");
}

const char* typeName(ExpType type, ExpType subtype) {
    switch (type) {
        case Integer:
            return "Integer";
        case Void:
            return "Void";
        case Array:
            if (subtype == Integer) {
                return "Array of Integers";
            } else if (subtype == Void) {
                return "Array of Voids";
            }
    }
    return "Unknown Type";
}

/*
 prints a syntax tree to the listing file using indentation to indicate subtrees
 */
void printTree( TreeNode * tree ) {
    int i;
    INDENT;

    while ( tree != NULL ) {
        printSpaces();
        if ( tree->nodekind == StmtK ) {
            switch ( tree->kind.stmt ) {
                case IfK:
                    fprintf(listing, "If\n");
                    break;
                case WhileK:
                    fprintf(listing,"While\n");
                    break;
                case ReturnK:
                    fprintf(listing, "Return\n");
                    break;
                case CmpdK:
                    fprintf(listing, "Compound Stmt\n" );
                    break;
                default:
                    fprintf(listing,"Unknown ExpNode kind\n");
                    break;
            }
        }
        else if ( tree->nodekind == ExpK ) {
            switch ( tree->kind.exp ) {
                case ErrK:
                    fprintf( listing, "Err: ERROR\n" );
                    break;
                case OpK:
                    fprintf( listing, "Op: " );
                    printToken( tree->attr.op, "\0" );
                    break;
                case ConstK:
                    fprintf( listing, "Const: %d<%s>\n", tree->attr.val,
                            typeName(tree->type, tree->subtype));
                    break;
                case IdK:
                    fprintf(listing,"Id: %s", ifnn(tree->attr.name));
                    fprintf(listing,"<%s>\n", typeName(tree->type, tree->subtype));
                    break;
                case CallK:
                    fprintf( listing, "Call: %s", ifnn(tree->attr.name));
                    fprintf(listing,"<%s>\n", typeName(tree->type, tree->subtype));
                    break;
                case SubsK:
                    fprintf( listing, "Subscript:\n" );
                    break;
                default:
                    fprintf( listing, "Unknown ExpNode kind\n" );
                    break;
            }
        } else if ( tree->nodekind == DecK ) {
            switch ( tree->kind.exp ) {
                case VarK:
                    fprintf(listing, "NewVar: %s<%s>\n", ifnn(tree->attr.name),
                        typeName(tree->type, tree->subtype));
                    break;
                case FunK:
                    fprintf(listing, "NewFunc:%s<%s>\n", ifnn(tree->attr.name),
                        typeName(tree->type, tree->subtype));
                    break;
                case ParamK:
                    fprintf(listing, "NewParam:%s<%s>\n", ifnn(tree->attr.name),
                        typeName(tree->type, tree->subtype));
                    break;
                default:
                    fprintf( listing, "Unknown DecNode kind\n" );
                    break;
            }
        }  else {
            fprintf( listing, "Unknown node kind\n" );
        }

        for ( i = 0; i < MAXCHILDREN; ++i ) {
            printTree( tree->child[i] );
        }

        tree = tree->sibling;
    }
    UNINDENT;
}


#define BLUE  '1'
#define RED '4'
#define CYAN '3'
#define BLACK '0'
#define GREEN '2'
#define MAG '5'
#define BROWN '6'
#define LGRAY '7'
#define DGRAY '8'
#define LBLUE '9'

#define LGREEN '10'
#define LCYAN '11'
#define LRED '12'
#define LMAG '13'
#define YELLOW '14'
#define WHITE '15'

static const char KEY = GREEN;
static const char PUNC = BLUE;
static const char OP = MAG;
static const char COMP = RED;
static const char TYPE = LGRAY;
static const char CONST = BROWN;
static const char NAME = LGRAY;
static const char CALL = CYAN;
static const char VAR = LBLUE;

static void printc( const char* text, const char colour ) {
    printf( "\033[1;3%cm%s\033[0m\n", colour, text ? text : "NULL" );
}

static void printcd( const int num, const char colour ) {
    printf( "\033[1;3%cm%d\033[0m\n", colour, num );
}

static void printc2( const char* text, const char* next, const char colour ) {
    printf( "\033[1;3%cm%s%s\033[0m\n",
        colour, text ? text : "NULL", next ? next : "NULL" );
}

static void printct( const char* text, int t, const char colour ) {
    if (t == Integer ) {
        printf( "\033[1;3%cmint %s\033[0m\n", colour, text ? text : "NULL" );
    } else {
        printf( "\033[1;3%cmvoid %s\033[0m\n", colour, text ? text : "NULL" );
    }
}

static void printct2(const char* text, const char* next, int t,
        const char col) {
    if (t == Integer ) {
        printf( "\033[1;3%cmint \033[0m\033[1;3%cm%s%s\033[0m\n",
            TYPE, col, text ? text : "NULL", next ? next : "NULL"  );
    } else {
        printf( "\033[1;3%cmint \033[0m\033[1;3%cm%s%s\033[0m\n",
            TYPE, col, text ? text : "NULL", next ? next : "NULL"  );
    }
}

/*
 prints a syntax tree to the listing file using indentation to indicate subtrees
 */
void printTreeColour( TreeNode* tree ) {
    int i;
    INDENT;

    while ( tree != NULL ) {
        printSpaces();
        if ( tree->nodekind == StmtK ) {
            switch ( tree->kind.stmt ) {
                case IfK:
                    printc( "if", KEY );
                    break;
                case WhileK:
                    printc( "while", KEY );
                    break;
                case ReturnK:
                    printc( "return", KEY );
                    break;
                case CmpdK:
                    printc( "{", PUNC );
                    break;
                default:
                    fprintf(listing,"Unknown ExpNode kind\n");
                    break;
            }
        }
        else if ( tree->nodekind == ExpK ) {
            switch ( tree->kind.exp ) {
                case ErrK:
                    printc( "ERROR", RED );
                    break;
                case OpK:
                    printTokenColour( tree->attr.op, "\0" );
                    break;
                case ConstK:
                    printcd( tree->attr.val , CONST );
                    break;
                case IdK:
                    printc( tree->attr.name, NAME );
                    break;
                case CallK:
                    printc2( tree->attr.name, "(", CALL) ;
                    break;
                case SubsK:
                    printc( "[]", CALL );
                    break;
                default:
                    fprintf( listing, "Unknown ExpNode kind\n" );
                    break;
            }
        } else if ( tree->nodekind == DecK ) {
            switch ( tree->kind.exp ) {
                case VarK:
                    printct( tree->attr.name, tree->type, VAR );
                    break;
                case FunK:
                     printct2( tree->attr.name, "(", tree->type, VAR );
                    break;
                case ParamK:
                     printct( tree->attr.name, tree->type, VAR );
                    break;
                default:
                    fprintf( listing, "Unknown DecNode kind\n" );
                    break;
            }
        } else {
            fprintf( listing, "Unknown node kind\n" );
        }

        for ( i = 0; i < MAXCHILDREN; ++i ) {
            printTreeColour( tree->child[i] );
        }
        tree = tree->sibling;
    }
    UNINDENT;
}

/*
 prints a token and its lexeme to the listing file
 */
void printTokenColour( int token, const char* token_string ) {

    switch ( token ) {
        case IF:
            printc("if ", KEY );
            break;
        case ELSE:
            fprintf(listing, "ELSE\n");
            break;
        case WHILE:
            fprintf(listing, "WHILE\n");
            break;
        case RETURN:
            fprintf(listing, "RETURN\n");
            break;
        case VOID:
            printc("void", TYPE);
            break;
        case INT:
            printc("int", TYPE);
            break;
        case PAREN:
            printc("(", PUNC);
            break;
        case L_PAREN:
            fprintf(listing, "L_PAREN\n");
            break;
        case R_PAREN:
            fprintf(listing, "R_PAREN\n");
            break;
        case L_BRACK:
            fprintf(listing, "L_BRACK\n");
            break;
        case R_BRACK:
            fprintf(listing, "R_BRACK\n");
            break;
        case L_BRACE:
            fprintf(listing, "L_BRACE\n");
            break;
        case R_BRACE:
            fprintf(listing, "R_BRACE\n");
            break;
        case ADD:
            printc("+", OP);
            break;
        case SUBTRACT:
             printc("-", OP);
            break;
        case ASSIGN:
             printc("=", OP);
            break;
        case EQUAL:
            printc("==", COMP);
            break;
        case NOT_EQUAL:
            printc("!=", COMP);
            break;
        case DIVISION:
             printc("/", OP);
            break;
        case STAR:
             printc("*", OP);
            break;
        case LESS_THAN:
            printc("<", COMP);
            break;
        case GREATER_THAN:
            printc(">", COMP);
            break;
        case SEMICOLON:
             printc(";", PUNC);
            break;
        case COMMA:
             printc(",", PUNC);
            break;
        case GREATER_THAN_EQ:
            printc(">=", COMP);
            break;
        case LESS_THAN_EQ:
           printc("<=", COMP);
            break;
        case ID:
            fprintf( listing, "ID(%s)\n", ifnn(token_string));
            break;
        case NUMBER:
            fprintf( listing, "NUMBER(%s)\n", ifnn(token_string));
            break;
        case ENDFILE:
            break;
        case ERROR:
            fprintf( listing, "ERROR(%s)\n", ifnn(token_string));
            break;
        default: /* should never happen */
            fprintf( listing, "Unknown(%s)\n", ifnn(token_string));
    }
}

