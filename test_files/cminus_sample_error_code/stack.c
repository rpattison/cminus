/** 
	* Simple Stack Program  * 
	Commands
	: 1 = Push, 2 = Pop, 3 = Size, 0 = Quit
	
	Note: The size of the stack is limited to 20.
	TODO: The size limit could be removed if recursion
	is used instead of an array by making use of the
	stack frame.
	
	```
	if ( command == PUSH ) {
		val = input();
		self( input );
	} else if ( command == POP ) {
		return; 
	}
	```

	Example Use:
		>>	1	Push 1 
		>>	1	
		>> 	2	Pop 
		   	1
		>> 	1	Push 3
		>> 	3
		>>	3	Size of stack ? 
			1
		>>	2	Pop 
			3
		>>	0	Quit
**/

int stack[20];
int index;
int PUSH;
int POP;
int SIZE;
int QUIT;

index = 0;
PUSH = 1;
POP = 2;
SIZE = 3;
QUIT = 0;

void push( int v ) {
	stack[index] = v;
	index = index + 1;
}

int pop( void ) {
	int v;
	v = stack[index - 1];
	index = index - 1;
	return ( v );
}

void main( void ) {
	int command = SIZE;
	int val;
	
	while ( command != QUIT ) {
		command = input();
		
		if ( command == PUSH ) {
			/** PUSH **/
			val = input();
			push( val );
		} else if ( command == POP ) {
			/** POP **/
			val = pop();
			output( val );
		} else if ( command == SIZE ) {
			/** LENGTH **/
			output( index );
		} else if ( command == QUIT )  {
			/** Quit **/
			return;
		} else {
			/** Errort **/
			output( -1 );
			command = QUIT;
		}
	}
}


