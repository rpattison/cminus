/****************************************************/
/* File: util.h                                     */
/* Utility functions for the TINY scanner           */
/* Fei Song                                         */
/****************************************************/

#ifndef _UTIL_H_
#define _UTIL_H_

#include "globals.h"

// prints a token and its value to the output file 
void printToken( FILE *, TokenType, TokenStruct & );

#endif
