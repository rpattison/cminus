/**
	A Combinatorial Counting Program:
	Usage MODE, N, R
	example for CHOOSE ( 4 , 3 ):
		> 	1 
		> 	4 
		> 	3
		4
**/


int CHOOSE;
int PERMUTATION;
int PERMREPLACE;
int CHOOSEREPLACE;

void fake( void ) {
    return 0 >= 1;
}

int fac( int v ) {
	if ( v <= 1 ) { /* forget -ve values */
		return 1;
	} else {
		return ( v * fac( v -1 ) );
	}  
}

int exp( int b, int r ) {
	if ( r == 0 ) { 
		return 1;/* Not always true. 0 ^ 0 */
	} else return exp( b, r - 1) * b;
}

int choose( int n, int r ) {
	return fac( n ) /  ( fac ( r ) * fac ( n - r ) );
}

int permu( int n, int r ) {
	return fac( n ) / fac( n - r );
}

int chooseR( int n, int r ) {
	return choose( n + r - 1, r );
}

int premuR( int n, int r ) {
	return exp( n, r );
}

void main( void ) {
	int command;

    CHOOSE = 1;
    PERMUTATION = 2;
    PERMREPLACE = 3;
    CHOOSEREPLACE = 4;
	
    CHOOSE >= 0;
    
	command = input();
	if ( command == CHOOSE ) {
		output( choose( input(), input() ) );
	} else if ( command == PERMUTATION ) {
		output( permu( input(), input() ) );
	} else if ( command == PREMREPLACE ) {
		output( premuR( input(), input() ) );
	} else if ( command == CHOOSEREPLACE ) {
		output( chooseR( input(), input() ) );
	}
}