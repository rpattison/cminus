#ifndef __DEC_HPP__
#define __DEC_HPP__ 1

#include "Absyn.hpp"
#include <string>


class Dec : public Absyn {
public:
    Dec(int pos) : Absyn(pos) {};

    void accept(ASTVisitor* visitor);
};


class NameTy : public Absyn {
public:
    enum Type {
        kInt=0,
        kVoid=1
    };

    NameTy(int pos, Type typ) : Absyn(pos), typ_(typ) {}


    void accept(ASTVisitor* visitor);
    Type getType() {
        return typ_;
    }
private:
    Type typ_;
};


class FunctionDec : public Dec {
public:
    FunctionDec(int pos, NameTy* result,
                std::string* func, VarDecList* params,
                CompoundExp* body)
                : Dec(pos), result_(result), params_(params), body_(body) {
        func_ = func;
    }

    void accept(ASTVisitor* visitor);

    std::string* getFunctionName() {
        return func_;
    }

    NameTy* getReturnType() {
        return result_;
    }

    VarDecList* getParamList() {
        return params_;
    }

    CompoundExp* getStatements() {
        return body_;
    }

private:
    std::string* func_;
    NameTy* result_;
    VarDecList* params_;
    CompoundExp* body_;
};
#endif

