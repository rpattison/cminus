#! /usr/bin/env perl

##
# This script makes the TM Assembly code (Compiled from CM) more readable
# Mostly, it replaces the register numbers with their symbolic names
# and replaces any meaningless values with '#'
# pipe the file into STDIN and the readable version prints to STDOUT

my $NO_SUB;
sub regname { # interprets the context and gives the register name if applicable.
    my ($regid, $op, $num) = @_;
    #$regid = shift @_;
    #$op = shift @_;
    #$num = shift @_;
    if ($NO_SUB) {
        return $regid;
    }
    if ($op =~ /HALT/) {
        return "#";
    }

    if ($op =~ /LDC/) {
        if ($num == 3) {
            return "#";
        } elsif ($num == 2) {
            return $regid;   
        }
    }
    
    if (($op =~ /IN/ or $op =~ /OUT/) and $num > 1) {
        return "#";
    }
    if ($regid == 0) {
        return "AC";
    } elsif ($regid == 1) {
        return "AC1";
    } elsif ($regid == 5) {
        return "FP";
    } elsif ($regid == 7) {
        return "PC";
    } elsif ($regid == 6) {
        return "GP";
    } else {
        return $regid;
    }
}
my @out;
foreach $line (<STDIN>) {
    chop $line;
    $ifcomm = ($ARGV[0] !~ /.*c.*/);
    $NO_SUB = ($ARGV[0] =~ /.*s.*/);
    
    if ($line =~ /\s*(\d+):\s+([A-Z]+)\s+(-?\d+),\s*(-?\d+)\s*\((-?\d+)\)(.*)/) { 
        # matches "03:  LD  0, 3(7)  comment"
        $lineno = $1;
        $op = $2;
        $reg1 = &regname($3, $op, 1);
        $reg2 = $4;
        $reg3 = &regname($5, $op, 3);
        $comment = $ifcomm ? $6 : "";
        push @out, "$lineno:   $op   $reg1, $reg2 ( $reg3 )   $comment \n";
    } elsif ($line =~ /\s*(\d+):\s+([A-Z]+)\s+(-?\d+),\s*(-?\d+),\s*(-?\d+)(.*)/) { 
        # matches "03: HALT 0, 0, 0   comment"
        $lineno = $1;
        $op = $2;
        $reg1 = &regname($3, $op, 1);
        $reg2 = &regname($4, $op, 2);
        $reg3 = &regname($5, $op, 3);
        $comment = $ifcomm ? $6 : "";
        push @out, "$lineno:   $op   $reg1, $reg2, $reg3   $comment\n";
    } elsif ($line =~ /\*.*/) { # comment
        if ($ifcomm) {
            push @out, "$line \n";
        }
    } elsif ($line !~ /.*:.*/) {
        
    } else {
        push @out, "UNKNOWN ( $line )";
    }
    
}

print sort { ($a =~ /\s*(\d+):/)[0] <=> ($b =~ /\s*(\d+):/)[0] || uc($a)  cmp  uc($b) } @out;
#print 
#    map  { $_->[1] }                #get the original value back
#    sort { $a->[0] <=> $b->[0] }    #sort arrayrefs numerically on the sort value
#    map  { /\s*([0-9]+):/; [$1, $_] } #build arrayref of the sort value and orig
#    @out;

