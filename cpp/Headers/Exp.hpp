#ifndef __EXP_HPP__
#define __EXP_HPP__ 1

#include "Absyn.hpp"
#include "Var.hpp"
#include <string>


class ExpList;
class VarDecList;


class Exp         : public Absyn {
public:
    Exp(int pos) : Absyn(pos) {};


    void accept(ASTVisitor* visitor);
};


class NilExp      : public Exp {
public:

    void accept(ASTVisitor* visitor);

    NilExp(int pos) : Exp(pos) {}
};


class VarExp      : public Exp {
public:
    VarExp(int pos,   Var* variable) : Exp(pos), variable_(variable) {}


    void accept(ASTVisitor* visitor);

    Var* getVariable() {
        return variable_;
    }
private:
    Var* variable_;
};


class IntExp      : public Exp {
public:
    IntExp(int pos, int value) : Exp(pos), value_(value) {}


    void accept(ASTVisitor* visitor);

    int getValue() {
        return value_;
    }
private:
    int value_;
};


class CallExp     : public Exp {
public:
    CallExp(int pos, std::string* func, ExpList* args) : Exp(pos), args_(args) {
        func_ = func;
    }


    void accept(ASTVisitor* visitor);

    std::string* getFunctionName() {
        return func_;
    }

    ExpList* getArgumentList() {
        return args_;
    }
private:
    std::string* func_;
    ExpList* args_;
};


class OpExp       : public Exp {
public:
    enum OP {
        kPlus=0,
        kMinus=1,
        kTimes=2,
        kOver=3,
        kEq=4,
        kLt=5,
        kGt=6,
        kParen=7,
        kLe=8,
        kGe=9,
        kNe=10
    };

    OpExp(int pos,   Exp* left, OP op,   Exp* right)
         : Exp(pos), left_(left), op_(op), right_(right) {}

    void accept(ASTVisitor* visitor);

    int getOp() {
        return op_;
    }

    Exp* getLHS() {
        return left_;
    }

    Exp* getRHS() {
        return right_;
    }
private:
    Exp* left_;
    OP   op_;
    Exp* right_;
};


class AssignExp   : public Exp {
public:
    AssignExp(int pos,   Var* lhs,   Exp* rhs)
            : Exp(pos), lhs_(lhs), rhs_(rhs) {}


    void accept(ASTVisitor* visitor)  ;

    Var* getLval() {
        return lhs_;
    }

    Exp* getRval() {
        return rhs_;
    }
private:
    Var* lhs_;
    Exp* rhs_;
};


class IfExp       : public Exp {
public:
    IfExp(int pos,   Exp* test,   Exp* thenpart, Exp* elsepart)
        : Exp(pos), test_(test), thenpart_(thenpart), elsepart_(elsepart) {}

    Exp* getCondition() {
        return test_;
    }


    void accept(ASTVisitor* visitor);

    Exp* getThenBranch() {
        return thenpart_;
    }

    Exp* getElseBranch() {
        return elsepart_;
    }
private:
    Exp* test_;
    Exp* thenpart_;
    Exp* elsepart_;
};


class WhileExp    : public Exp {
public:
    WhileExp(int pos,   Exp* test,   Exp* body)
           : Exp(pos), test_(test), body_(body) {}

    Exp* getCondition() {
        return test_;
    }

    void accept(ASTVisitor* visitor)  ;

    Exp* getBody() {
        return body_;
    }
private:
    Exp* test_;
    Exp* body_;
};


class ReturnExp   : public Exp {
public:
    ReturnExp(int pos,   Exp* expr) : Exp(pos), exp_(expr) {}

    Exp* getExpression() {
        return exp_;
    }


    void accept(ASTVisitor* visitor);
private:
    Exp* exp_;
};


class CompoundExp : public Exp {
public:
    CompoundExp(int pos,   VarDecList* decs, ExpList* exps)
              : Exp(pos), decs_(decs), exps_(exps) {}

    VarDecList* getVariableDeclarations() {
        return decs_;
    }


    void accept(ASTVisitor* visitor);

    ExpList* getExpressions() {
        return exps_;
    }
private:
    VarDecList* decs_;
    ExpList* exps_;
};


class WriteExp    : public Exp {
public:
    WriteExp(int pos,   Exp* output) : Exp(pos), output_(output) {}


    void accept(ASTVisitor* visitor)  ;
    Exp* getOutput() {
        return output_;
    }
private:
    Exp* output_;
};


class ReadExp     : public Exp {
public:
    ReadExp(int pos,   VarExp* input) : Exp(pos), input_(input) {}

    void accept(ASTVisitor* visitor)  ;
    VarExp* getVariable() {
        return input_;
    }
private:
    VarExp* input_;
};

#endif
