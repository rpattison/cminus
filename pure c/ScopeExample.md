## Example of Scope Class

~~~~{.cpp}
    
    #include <string>
    #include <iostream>
    
    #include "Scope.hpp"

    int main(int argc, char* argv[] ) {
      typedef Scope<string,string> SymbolTable;
      
      SymbolTable sym;
      
      try { // must 'begin' a scope before use.
        sym.insert("Key", "Value");
      } catch (SymbolTable::NoScope& e) {
        cerr << "No scope" << endl;
      }
      
      sym.beginScope();
      sym.insert("Name", "Duck");
      sym.insert("Sound", "Quack");
      sym.insert("Swims", "Yes");
      
      cout << sym.lookup("Name") << endl;
      
      try { // undefined key
        cout << sym.lookup("Flies") << endl;
      } catch(SymbolTable::UnknownKey& e) {
        cerr << "Unknown Key" << endl;
      }
      
      try { // redeclaring name in the same scope
        sym.insert("Name", "Daffy");
      } catch (SymbolTable::Redeclaration& e) {
        cerr << "Redeclaration" << endl; //< this one called/
      } catch(SymbolTable::NoScope& e) {
        cerr << "NoScope" << endl;
      }
      
      sym.beginScope();
      // new defintion hides previous in upper scope.
      sym.insert("Name", "Rubber Duck");
      
      // previous definition of name is hidden.
      cout << sym.lookup("Name") << endl;
      
      // found in parent scope.
      cout << sym.lookup("Swims") << endl;
      sym.endScope();
      
      //returns to old defintion
      cout << sym.lookup("Name") << endl;
      sym.endScope();
      
      try {
        sym.endScope(); //< too many end scopes.
      } catch (SymbolTable::NoScope& e ) {
        cerr << "No Scope" << endl;
      }
      return 0;
    }
    
~~~~
