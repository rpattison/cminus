/* output the first n terms in the fibonacci sequence; ( second form ) */

int fib( int n ) {
	int x;
	
	if ( n <= 1 ) {
		output( n );
		return 1;
	}
	
	x = fib( n - 1 ) + fib( n - 2 );
	output( x );
	return x;
}

void main( void ) {
	fib( input() );
}
