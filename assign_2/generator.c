/**
* @file generator.c
* @brief This if the file used to generate assembly code
* @note This file is part of the C- compiler for the CIS-Compilers Course.
* @date 2013
* @authors Ryan Pattison, Abhishek Vanarase
*/

#include "globals.h"
#include "generator.h"
#include "y.tab.h"
#include "util.h"
#include <ctype.h>
#include "Hash.h"
#include <assert.h>

#define PC 7
#define GP 6
#define FP 5
#define AC 0
#define AC1 1


extern int TraceCode;
extern FILE* code;

static int emitLoc = 0;
static int highEmitLoc = 0;
static int entryPoint;
static int globalOffset;

static char stringBuff[256];

void genCompound(TreeNode* tree, int offset, int isAddr);
void genStmt(TreeNode* tree, int offset, int isAddr);
void genExp(TreeNode* tree, int offset, int isAddr);



void emitRestore(void)
{
    emitLoc = highEmitLoc;
}

int emitSkip(int distance)
{
    int i = emitLoc;
    emitLoc += distance;
    if (highEmitLoc < emitLoc)
        highEmitLoc = emitLoc;
    return i;
}

void emitComment(const char* comment)
{
    fprintf(code, "* %s\n", comment);
}

void emitBackup(int loc)
{
    if (loc > highEmitLoc) {
        emitComment("Bug in Emit Backup");
    }
    emitLoc = loc;
}


void emitInc()
{
    emitLoc++;
    if (highEmitLoc < emitLoc)
        highEmitLoc = emitLoc;
}


void emitRO(const char* op, int r, int a, int b, const char* comment)
{
    fprintf(code, "%3d: %5s %d, %d, %d",
            emitLoc, op, r, a, b);
    emitInc();
    if (TraceCode)
        fprintf(code, "\t%s", comment);
    fprintf(code, "\n");
}



void emitRM_Abs(const char* op, int r, int a,  const char* comment)
{
    fprintf(code, "%3d: %5s %d, %d(%d)",
            emitLoc, op, r, a - (emitLoc + 1), PC);
    emitInc();
    if (TraceCode)
        fprintf(code, "\t%s", comment);
    fprintf(code, "\n");
}


void emitRM(const char* op, int r, int a, int b, const char* comment)
{
    fprintf(code, "%3d: %5s %d, %d(%d)",
            emitLoc, op, r, a, b);
    emitInc();
    if (TraceCode)
        fprintf(code, "\t%s", comment);
    fprintf(code, "\n");
}


void genIf(TreeNode* tree, int offset, int isAddr) 
{
    int jump_else_start; // location of the start of the else block
    int jump_else_end; // location of the end of the else block
    int jump_false;//
    int end_else;//

    /* Gen condition statement code (result in AC)*/
    genExp(tree->child[0], offset, isAddr);
    /* store code address */
    jump_false = emitSkip(1);
    /* generate then code */
    genStmt(tree->child[1], offset, isAddr);
    /* backpatch jump for start of else, JNE AC */
    if (tree->child[2]) { /* create the jump over else if else exists */
        jump_else_end = emitSkip(1); 
    }
    jump_else_start = emitSkip(0);
    emitBackup(jump_false);
    emitRM_Abs("JEQ", AC, jump_else_start, "jump to else for false");
    emitRestore();
    if (tree->child[2]) { // if else statements...
        /* Gen else branch */
        genStmt(tree->child[2], offset, isAddr);
        /* backpatch to jump over else */
        end_else = emitSkip(0);
        emitBackup(jump_else_end);
        emitRM_Abs("LDC", PC, end_else, "Jump over else after then");
        emitRestore();
    }
}


void genWhile(TreeNode* tree, int offset, int isAddr)
{
    int jump_end; // location 
    int jump_repeat; // location
    int while_end; 

    /* save location for repeat */
    jump_repeat = emitSkip(0);
    /* Gen condition statement code */
    genExp(tree->child[0], offset, isAddr);
    /* Save location for false */
    jump_end = emitSkip(1);
    /* Generate body */
    genCompound(tree->child[1], offset, isAddr);
    /* jump for repeat */
    emitRM("LDC", PC, jump_repeat, 0, "Jump and repeat");
    /* backpatch false jump */
    while_end = emitSkip(0);
    emitBackup(jump_end);
    emitRM_Abs("JEQ", AC, while_end, "Skip while body on false");
    emitRestore();
}


void genReturn(TreeNode* tree, int offset, int isAddr)
{
    /* evaluate expression if exists (to AC) */
    if (tree->child[0]) {
        genExp(tree->child[0], offset, isAddr);
    } else {
        /* AC = 0, not necessary but nice ` */
        //emitRM("LDC", AC, 0, 0, "Clear return register"); 
    }
    /* Load PC with return from FP */
    emitRM("LD", PC, -1, FP, "load return address");
}


void genCompound(TreeNode* tree, int offset, int isAddr)
{
    TreeNode* last_declar = NULL;
    int base = 0;

    if (tree == NULL) {
        fprintf(stderr, "[genCompound] received Null tree node\n");
        return;
    }
   
    //printf("* genCompound offset %d\n",  offset);
    base = offset;
    last_declar = tree->child[0];
    /* reserve local variable (add to offset) in tree */
    while (last_declar) {
        last_declar->offset += base + 1;
        offset = last_declar->offset;
        last_declar = last_declar->sibling;
    }
    
    /* loop and generate each statement */
    TreeNode* stmt = tree->child[1];
    while (stmt) {
        genStmt(stmt, offset, isAddr);
        stmt = stmt->sibling;
    }
}


void genStmt(TreeNode* tree, int offset, int isAddr)
{
    if (tree == NULL) {
        fprintf(stderr, "[genStmt] received Null tree node\n");
        return;
    }

    if (tree->nodekind == ExpK) {
        genExp(tree, offset, isAddr);
        return;
    }

    switch (tree->kind.stmt) {
        case IfK:
            genIf(tree, offset, isAddr);
            break;
        case WhileK:
            genWhile(tree, offset, isAddr);
            break;
        case ReturnK:
            genReturn(tree, offset, isAddr);
            break;
        case CmpdK:
            genCompound(tree, offset, isAddr);
            break;
        default:
            fprintf(stderr, "[genStmt] Unknown Case\n");
            break;
    }
}


void genConst(TreeNode* tree, int offset, int isAddr)
{
    /* if isaddr load const LDC -> AC1 */
    if (isAddr) {
        emitRM("LDC", AC1, tree->attr.val, 0, "Load address constant"); 
    } else {
        /* LDC to AC */
        emitRM("LDC", AC, tree->attr.val, 0, "Load value constant"); 
    }
}


void genCompOp(TreeNode* tree, int offset, int isAddr, const char* cmpName)
{
    /* Load LHS AC, Load RHS AC1 */
    genExp(tree->child[0], offset, 0);
    offset += 1;
    emitRM("ST", AC, -offset, FP, "push right hand side");
    genExp(tree->child[1], offset, 0);
    emitRM("LD", AC1, -offset, FP, "pull right hand side");
    offset -= 1;
    /* Subtract AC = AC1 - AC */
    emitRO("SUB", AC, AC1, AC, "subtract AC AC1");
    /* J{cmpName} ahead PC = PC + 2 */
    emitRM(cmpName, AC, 2, PC, "skip false assignment");
    /* Set AC = 0 (false) */
    emitRM("LDC", AC, 0, 0, "Set AC to False");
    /* Jump 1 around true */
    emitRM("LDA", PC, 1, PC, "skip true assignment");
    /* set AC = 1 (true) */
    emitRM("LDC", AC, 1, 0, "Set AC to True");
}


void genArithOp(TreeNode* tree, int offset, int isAddr, const char* opName)
{
    /* Load LHS to AC, RHS to AC1 */
    genExp(tree->child[0], offset, 0);
    offset += 1;
    emitRM("ST", AC, -offset, FP, "push right hand side");
    genExp(tree->child[1], offset, 0);
    emitRM("LD", AC1, -offset, FP, "load rhs");
    offset -= 1;
    /* if not address */
    /* {opName} AC = AC1 op AC */
    emitRO(opName, AC, AC1, AC, "evaluate op");
    /* else AC1 = AC1 op AC */
}


void genAssign(TreeNode* tree, int offset, int isAddr)
{
    //fprintf(stderr, "[genAssign]: Not implemented yet\n");
    genExp(tree->child[0], offset, 1);
    offset += 1;
    emitRM("ST", AC1, -offset, FP, "Push value to stack");
    genExp(tree->child[1], offset, 0);
    emitRM("LD", AC1, -offset, FP, "pop the value from stack");
    offset -= 1;
    emitRM("ST", AC, 0, AC1, "Assign the value");
}


void genOp(TreeNode* tree, int offset, int isAddr)
{
    switch (tree->attr.op) {
        case ADD:
            genArithOp(tree, offset, isAddr, "ADD");
            break;
        case SUBTRACT:
            genArithOp(tree, offset, isAddr, "SUB");
            break;
        case STAR:
            genArithOp(tree, offset, isAddr, "MUL");
            break;
        case DIVISION:
            genArithOp(tree, offset, isAddr, "DIV");
            break;
        case EQUAL:
            genCompOp(tree, offset, isAddr, "JEQ");
            break;
        case NOT_EQUAL:
             genCompOp(tree, offset, isAddr, "JNE");
             break;
        case LESS_THAN:
            genCompOp(tree, offset, isAddr, "JLT");
            break;
        case GREATER_THAN:
            genCompOp(tree, offset, isAddr, "JGT");
            break;
        case GREATER_THAN_EQ:
            genCompOp(tree, offset, isAddr, "JGE");
            break;
        case LESS_THAN_EQ:
            genCompOp(tree, offset, isAddr, "JLE");
            break;
        case ASSIGN:
            genAssign(tree, offset, isAddr);
            break;
        case PAREN:
            genExp(tree, offset, isAddr);
            break;
        default:
            fprintf(stderr, "[Generator]: Unknown Operator:%d\n", tree->attr.op);
    }
}


void genId(TreeNode* tree, int offset, int isAddr)
{
    int stack = 0;
    TreeNode* decl = NULL;
    //fprintf(stderr, "Gen ID  not yet implemented\n");
    assert(tree);

    sprintf(stringBuff, "Look up for variable: %s", tree->attr.name);
    emitComment(stringBuff);
    
    decl = tree->ref;
    assert(decl != NULL);
    assert(decl->nodekind == DecK);
    assert(decl->type == tree->type);

   if (decl->nestLevel == 1) { /* global variable */
        if (isAddr) {
            /* load offset + stack to AC */
            emitRM("LDA", AC1, -decl->offset, GP, "Load the address");
        } else {
            /* stack[offset].VALUE -> AC */
            emitRM("LD", AC, -decl->offset, GP, "Load the value");
        }
    } else {
        if (isAddr) {
            /* load offset + stack to AC */
            if (decl->kind.dec == ParamK && decl->type == Array) {
                emitRM("LD", AC1, -decl->offset, FP, "Load the address");
            } else {
                emitRM("LDA", AC1, -decl->offset, FP, "Load the address");
            }
        } else {
            if (decl->kind.dec == ParamK && decl->type == Array) {
                emitRM("LD", AC, -decl->offset, FP, "Load the address");
            } else {
                /* stack[offset].VALUE -> AC */
                emitRM("LD", AC, -decl->offset, FP, "Load the value");
            }
        }
    }
}


void genCall(TreeNode* tree, int offset, int isAddr)
{
    TreeNode* arg = NULL;
    TreeNode* dec = NULL;
    
    offset += 2; /* space for return address and old frame */
    /* evaluate arguments and push on stack (with 2 spots saved for return and ofp) */
    arg = tree->child[0];
    dec = tree->ref->child[0];
    int sizeOf = 0;

    while (arg && dec) {
        if (dec->type == Array) {
            genExp(arg, (offset + sizeOf), 1);
            sizeOf++; //dec->offset;
            emitRM("ST", AC1, -(offset + sizeOf), FP, "Store arg");
         } else {
            genExp(arg, (offset + sizeOf), 0);
            sizeOf++; //dec->offset;
            emitRM("ST", AC, -(offset + sizeOf), FP, "Store arg");
        }
       arg = arg->sibling;
        dec = dec->sibling;
    }

    /* push ofp (old frame ptr) onto stack */
    emitRM("ST", FP, -(offset - 1), FP, "Push old frame pointer (ofp)");
    emitRM("LDA", FP, -(offset - 1), FP, "Push frame");

    offset += sizeOf;
    /* load AC with the return address (loaded later inbetween old frame and  first arg)*/
    
    emitRM("LDA", AC, 1, PC, "load ac with return ptr");
    /* lookup id of function, jump to function start */
    emitRM("LDC", PC, tree->ref->address, 0, "jump to func loc");
    /* pop frame */
    emitRM("LD", FP, 0, FP, "Pop old frame pointer");
}


void genSub(TreeNode* tree, int offset, int isAddr)
{
    /* TODO: check subscript < 0*/
    //fprintf(stderr, "Gen sub not yet implemented\n");
    
    /* evaluate subscript */
    genExp(tree->child[1], offset, 0);

    offset++;
    emitRM("ST", AC, -offset, FP, "store the index");

    /* save it to stack */
    emitRM("JLT", AC, 1, PC, "Halt of Subscript < 0");
    emitRM("LDA", PC, 1, PC, "Absolute Jump if not");
    emitRO("HALT", 0, 0, 0, "Halt if subscript < 0");

    /* fetch the array base */
    genId(tree->child[0], offset, 1);

    emitRM("LD", AC, -offset, FP, "store the index");
    offset--;
    /* calculate the address of the element */ 

    emitRO("SUB", AC1, AC1, AC, "Calculate the address of the element");
    /* if isAddr (assignment to array) */
    if ( isAddr ) {
        //emitRO("SUB", AC1, AC1, AC, "Calculate the address of the element");
    } else {
        /* Load AC1[AC] into AC */
        emitRM("LD", AC, 0, AC1, "Load the value of the element");
    }
}


void genExp(TreeNode* tree, int offset, int isAddr)
{
    if (tree == NULL) {
        fprintf(stderr, "[genExp] Null Tree Node\n");
        return;
    }
    switch (tree->kind.exp) {
     case ErrK:
        break;
      case OpK:
          genOp(tree, offset, isAddr);
          break;
      case ConstK:
          genConst(tree, offset, isAddr);
          break;
      case IdK:
          
               genId(tree, offset, isAddr);
          break;
      case CallK:
          genCall(tree, offset, isAddr);
          break;
      case SubsK:
          genSub(tree, offset, isAddr);
          break;
      default:
          fprintf(stderr, "[genExp] Unknown Case\n");
          break;
    }
}


void genParam(TreeNode* tree, int offset, int isAddr)
{
    /* nothing to do*/
    fprintf(stderr, "Gen Param not yet implemented\n");
}


void genVar(TreeNode* tree, int offset, int isAddr)
{
    fprintf(stderr, "Gen Var not yet implemented\n");
}


void genFunc(TreeNode* tree, int offset, int isAddr)
{
    int savedLoc = 0;
    int savedLoc2 = 0;
    int base = 0;
    TreeNode* last_param = NULL;

    savedLoc = emitSkip(1);
    tree->address = savedLoc + 1;
    if (strcmp(tree->attr.name, "main") == 0) {
        entryPoint = tree->address;
    }
    sprintf(stringBuff, "processing function: %s", tree->attr.name);
    emitComment(stringBuff);
    emitRM("ST", AC, -1, FP, "store return");
    
    last_param = tree->child[0];
    offset += 2;
    base = offset; // add 2 for return and ofp positions
    while (last_param) {
        last_param->offset += base;
        offset = last_param->offset; 
        last_param = last_param->sibling;
    }

    genCompound(tree->child[1], offset, isAddr);
    emitRM("LD", PC, -1, FP,  "return to caller");
    savedLoc2 = emitSkip(0);
    emitBackup(savedLoc);
    emitRM_Abs("LDA", PC, savedLoc2, "Backpatch");
    emitRestore();
}


void genDec(TreeNode* tree, int offset, int isAddr)
{
    switch (tree->kind.exp) {
        case VarK:
            genVar(tree, offset, isAddr);
            break;
        case FunK:
            genFunc(tree, offset, isAddr);
            break;
        case ParamK:
            genParam(tree, offset, isAddr);
            break;
        default:
            fprintf(stderr, "Unknown declaration node\n");
            break;
    }
}


void generateASM(TreeNode* tree)
{
    int offset = 0;
    int isAddr = 0;

    emitComment("Standard Prelude");
    emitRM("LD", GP, 0, 0, "load gp with maxaddr");
    emitRM("LDA", FP, 0, GP, "copy gp to fp");
    emitRM("ST", AC, 0, 0, "clear content at loc 0");
    emitRM("LDA", PC, 7, PC, "jump around io definitions");

    emitComment("generate input function");
    emitRM("ST", AC, -1, FP, "store return");
    emitRO("IN", AC, 0, 0, "input to reg 0");
    emitRM("LD", PC, -1, FP, "return to caller");

    emitComment("generate output function");
    emitRM("ST", AC, -1, FP, "store return");
    emitRM("LD", AC, -2, FP, "Load output value");
    emitRO("OUT", AC, 0, 0, "write to console");
    emitRM("LD", PC, -1, FP, "return to caller");
    
    while (tree != NULL) {
        if (tree->nodekind == DecK) { 
            if (tree->kind.dec == FunK) {
                genDec(tree, 0, 0);
            } else if (tree->kind.dec == VarK) {
                sprintf(stringBuff, "allocaing global variable:: %s", tree->attr.name);
                globalOffset = tree->offset + cmSize(tree);
                emitComment(stringBuff); 
            } else {
                fprintf(stderr, "Error: parameter in global scope\n");
            }
        }  else {
            fprintf(stderr, "Error: Non-declaration in global scope\n");
        }
        tree = tree->sibling;
    }
    
    emitComment("Generate Standard Finale");
    emitRM("ST", FP, -offset, FP, "push ofp");
    emitRM("LDA",FP, -globalOffset, FP, "push frame");
    emitRM("LDA", AC, 1, PC, "load ac with return ptr");
    emitRM("LDC", PC, entryPoint, 0, "jump to main loc");
    emitRM("LD",FP, 0, FP, "pop frame");
    emitRO("HALT", 0, 0, 0, "Main Done");
}
