/**
* @brief 	The *Scanner* definition for the C- language.
*			more information can be found on [bitbucket](https://bitbucket.org/rpattison/cminus)
* @authors  Ryan Pattison <ryan.m.pattison@gmail.com>
*           Abhishek Vanarase <abhishekvanarase@gmail.com>
* @version 	1.0
* @date 	February 6, 2013
*/

%{

#include <stdio.h>
#include "globals.h"
#include "util.h"
#include "scan.h"
#include "y.tab.h"



/**
* Token string buffer.
*/
char token_string[MAX_TOKEN_LEN + 1];

%}

%state COMMENT
/** Definitions **/
whitespace		(" "|\r|\n|\t)
letter          [[:alpha:]]
digit           [0-9]
number          {digit}+
id              {letter}+

%%

if                  { return ( IF ); } 				/** Controls **/
else                { return ( ELSE ); }
while               { return ( WHILE ); }
return              { return ( RETURN ); }

void                { return ( VOID ); } 			/** Data Types **/
int                 { return ( INT ); }

"("                 { return ( L_PAREN ); } 		/** Token Symbols **/
")"                 { return ( R_PAREN ); }
"["                 { return ( L_BRACK ); }
"]"                 { return ( R_BRACK ); }
"{"                 { return ( L_BRACE ); }
"}"                 { return ( R_BRACE ); }
"+"                 { return ( ADD ); }
";"					{ return ( SEMICOLON ); }
","					{ return ( COMMA ); }
"-"                 { return ( SUBTRACT ); }
"="                 { return ( ASSIGN ); }
"=="                { return ( EQUAL); }
"!="                { return ( NOT_EQUAL); }
"/"                 { return ( DIVISION ); }
"*"                 { return ( STAR ); } 
"<"                 { return ( LESS_THAN ); }
">"                 { return ( GREATER_THAN ); }
"<="                { return ( LESS_THAN_EQ ); }
">="                { return ( GREATER_THAN_EQ ); }
"/*"                { /** eat up comments */
                        char c;
                        do {
                            do {
                                c = input();
                                if ( c == EOF || c == 0 ) {
                                    return ( ERROR );
                                } 
                                
                                if ( c == '\n' ) {
                                    lineno++;
                                }
                            } while ( c != '*' );
                            
                            c = input();
                            
                            if ( c != '/' ) {
                                unput( c );
                            }

                        } while( c != '/' );
                    }
"\n"                { lineno++; }
{id}                { return ( ID ); } 				/** Identifiers **/
{number}            { return ( NUMBER ); }

{whitespace}        { /* no semantic value */ }		/** Formatting **/
.					{ return ERROR; }

%%

/**
* @return the next token in the input stream.
*/
int getToken( void ) {
	static int firstTime = 1;
	int currentToken;
	
    if ( firstTime ) {
		firstTime = 0;
		lineno++;
		yyin = source;
		yyout = listing;
	}
    
	currentToken = yylex();
	strncpy( token_string, yytext, MAX_TOKEN_LEN );
	return currentToken;
}
