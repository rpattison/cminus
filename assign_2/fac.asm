* C-Minus Compilation to TM Code
* File: fac.tm
* Standard prelude:
0:	LD	GP = AC[0] 	load gp with maxaddress
1:	LDA	FP = &GP[0] 	copy to gp to fp
2:	ST	AC[0] = AC 	clear location 0
* Jump around i/o routines here
* code for input routine
4:	ST	FP[-1] = AC 	store return
5:	IN	AC = IN() 	input
6:	LD	PC = FP[-1] 	return to caller
* code for output routine
7:	ST	FP[-1] = AC 	store return
8:	LD	AC = FP[-2] 	load output value
9:	OUT	OUT(AC) 	output
10:	LD	PC = FP[-1] 	return to caller
3:	LDA	PC = &PC[7] 	jump around i/o code
* End of standard prelude.
* processing function: main
* jump around function body here
12:	ST	FP[-1] = AC 	store return
* -> compound statement
* processing local var: x
* processing local var: fac
* -> op
* -> id
* looking up id: x
13:	LDA	AC = &FP[-2] 	load id address
* <- id
14:	ST	FP[-4] = AC 	op: push left
* -> call of function: input
15:	ST	FP[-5] = FP 	push ofp
16:	LDA	FP = &FP[-5] 	push frame
17:	LDA	AC = &PC[1] 	load ac with ret ptr
18:	LDA	PC = &PC[-15] 	jump to fun loc
19:	LD	FP = FP[0] 	pop frame
* <- call
20:	LD	AC1 = FP[-4] 	op: load left
21:	ST	AC1[0] = AC 	assign: store value
* <- op
* -> op
* -> id
* looking up id: fac
22:	LDA	AC = &FP[-3] 	load id address
* <- id
23:	ST	FP[-4] = AC 	op: push left
* -> constant
24:	LDC	AC = 1	 	load const
* <- constant
25:	LD	AC1 = FP[-4] 	op: load left
26:	ST	AC1[0] = AC 	assign: store value
* <- op
* -> while
* while: jump after body comes back here
* -> op
* -> id
* looking up id: x
27:	LD	AC = FP[-2] 	load id value
* <- id
28:	ST	FP[-4] = AC 	op: push left
* -> constant
29:	LDC	AC = 1	 	load const
* <- constant
30:	LD	AC1 = FP[-4] 	op: load left
31:	SUB	AC = AC1 - AC 	op >
32:	JGT	if AC > 0: PC = PC[2] 	br if true
33:	LDC	AC = 0	 	false case
34:	LDA	PC = &PC[1] 	unconditional jmp
35:	LDC	AC = 1	 	true case
* <- op
* while: jump to end belongs here
* -> compound statement
* -> op
* -> id
* looking up id: fac
37:	LDA	AC = &FP[-3] 	load id address
* <- id
38:	ST	FP[-4] = AC 	op: push left
* -> op
* -> id
* looking up id: fac
39:	LD	AC = FP[-3] 	load id value
* <- id
40:	ST	FP[-5] = AC 	op: push left
* -> id
* looking up id: x
41:	LD	AC = FP[-2] 	load id value
* <- id
42:	LD	AC1 = FP[-5] 	op: load left
43:	MUL	AC = AC1 * AC 	op *
* <- op
44:	LD	AC1 = FP[-4] 	op: load left
45:	ST	AC1[0] = AC 	assign: store value
* <- op
* -> op
* -> id
* looking up id: x
46:	LDA	AC = &FP[-2] 	load id address
* <- id
47:	ST	FP[-4] = AC 	op: push left
* -> op
* -> id
* looking up id: x
48:	LD	AC = FP[-2] 	load id value
* <- id
49:	ST	FP[-5] = AC 	op: push left
* -> constant
50:	LDC	AC = 1	 	load const
* <- constant
51:	LD	AC1 = FP[-5] 	op: load left
52:	SUB	AC = AC1 - AC 	op -
* <- op
53:	LD	AC1 = FP[-4] 	op: load left
54:	ST	AC1[0] = AC 	assign: store value
* <- op
* <- compound statement
55:	LDA	PC = &PC[-29] 	while: absolute jmp to test
36:	JEQ	if AC == 0: PC = PC[19] 	while: jmp to end
* <- while
* -> call of function: output
* -> id
* looking up id: fac
56:	LD	AC = FP[-3] 	load id value
* <- id
57:	ST	FP[-6] = AC 	store arg val
58:	ST	FP[-4] = FP 	push ofp
59:	LDA	FP = &FP[-4] 	push frame
60:	LDA	AC = &PC[1] 	load ac with ret ptr
61:	LDA	PC = &PC[-55] 	jump to fun loc
62:	LD	FP = FP[0] 	pop frame
* <- call
* <- compound statement
63:	LD	PC = FP[-1] 	return to caller
11:	LDA	PC = &PC[52] 	jump around fn body
* <- fundecl
64:	ST	FP[0] = FP 	push ofp
65:	LDA	FP = &FP[0] 	push frame
66:	LDA	AC = &PC[1] 	load ac with ret ptr
67:	LDA	PC = &PC[-56] 	jump to main loc
68:	LD	FP = FP[0] 	pop frame
* End of execution.
69:	HALT	EXIT 	
