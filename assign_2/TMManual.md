# The TM (Tiny Machine) Simulator

## Registers And Memory

`int reg[NO_REGS]` (*Register*)
:   

PC = 7 (Program Counter)
GP = 6 (Global Pointer)
FP = 5 (Frame Pointer)
AC = 0 (Accumulator) (Returns)


`PC_REG` (Program Counter)
:   the iMem address of the next instruction to be executed.

`int dMem[DADDR_SIZE]` (Data Memory)
: Holds the stack data for activation records and temporary variables.


`INSTRUCTION iMem[IADDR_SIZE]` *Instruction Memory*
:   Holds  the instructions (code).

The current instruction is `iMem[reg[PC_REG]]`

### IO Functions

| *Opcode*                          | *Format*    | *Effect*             | 
|:-----------------------------------|:-----------:|-----------------------|
|**IN** (*Get Input From User*)    | IN R, 0, 0  |`scanf("%d\n", &reg[R]);` |
|**OUT** (*Write Value to Console*)| OUT R, 0, 0 |`printf("%d\n", reg[R]);` |

### Mathematical Functions

| *Opcode*                    |  *Effect*             |        
|:----------------------------|:-----------------------|
| **ADD** (*Addition*)   | `reg[R] = reg[X] + reg[Y];`   |     
| **SUB** (*Subtraction*) | `reg[R] = reg[X] - reg[Y];`   |   
| **MUL** (*Multiplication*)   | `reg[R] = reg[X] * reg[Y];`   |     
| **DIV** (*Division*) | `reg[R] = reg[X] / reg[Y];`   |   

### Loading and Storing Values

Format: opcode r, d(s)

`d` is the offset added to the value in register `s`

    a = reg[s] + d;

| *Opcode*                    |  *Effect*             |        
|:----------------------------|:-----------------------|
| **LD**  (*Load*)            | `reg[r] = dMem[a]`     |      
| **LDA** (*Load Address*)   |  `reg[r] = a`          |
| **LDC** (*Load Constant*)  |  `reg[r] = d`          |
| **ST** (*Store*)            |  `dMem[a] = reg[r]`    |

Note that `LDC` ignores register `s`.

### Control Flow

| *Opcode*                                    |  *Effect*      |        
|:---------------------------------------------|:-----------------------|
|**JLT** (*Jump on Less Than*)               |`reg[r] = dMem[a]`|      
|**JLE** (*Jump on Less Than or Equal to*)  |`reg[r] = a`          |
|**JGT** (*Jump on Greater Than*)  |  `reg[r] = d`          |
|**JGE** (*Jump on Greater Than or Equal to.*)            |  `dMem[a] = reg[r]`    |
|**JEQ** (*Jump on Equal*)  |  `reg[r] = d`          |
|**JNE** (*Jump on not Equal*)            |  `dMem[a] = reg[r]`    |
|**HALT** (*Stops Execution*) | `exit(0)` |

### Instruction Format

\* this is a full line comment (it does *not* have a line number)

LINENO :        OPCODE R, X, Y

OR

LINENO :       OPCODE R, D(S)

Note: LINENO's do not have to be presented in order.


