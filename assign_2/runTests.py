#! /usr/bin/env python

import os
import subprocess

TESTS = './test'


if __name__ == "__main__":
    # run build

    # clean directory tm files

    # compile to assembly
    for dirpath, dirname, files in os.walk(TESTS):
        for test in sorted(files):
            name, ext = os.path.splitext(test)
            if ext == ".cm":
                subprocess.check_output("./cm -c ./test/%s | ./tm_reader.pl -cs > ./test/%s.tm" %
                        (test, name), shell=True)

    # run assembly and verify output
    for dirpath, dirname, files in os.walk(TESTS):
            for test in sorted(files):
                name, ext = os.path.splitext(test)
                if ext == ".tm":
                    print test
                    #process = subprocess.Popen("./TMSimulator/tm ./test/%s" % test,
                    #    stdin=subprocess.PIPE, stdout=subprocess.PIPE, shell=True)
                    #process.stdin.write("g\n")
                    #output = process.stdout.read()
                    #process.terminate()
                    #print output
