/**
 * @file main.c
 * @brief Executes the main fuction which opens the source file, parses it and displays the syntax tree.
 */

#include "globals.h"
#include "util.h"
#include "parse.h"
#include <getopt.h>


int lineno = 0; ///< the line number in the current document
FILE* source; ///< the file stream for the source file used in tokenizing
FILE* listing; ///< the output stream for displaying the parse tree

/**
    The main function which interacts with the command line and processes the user's request
*/
int main( int argc, char** argv ) {
	TreeNode* syntaxTree;
    int opt;
    
	if ( argc != 3 ) {
		fprintf( stderr, "usage: %s -[asc] <filename>\n", argv[0] );
		exit( 1 );
	}
    
    
	char pgm[120]; /* source code file name */
	
	strcpy( pgm, argv[2] );
	
	if ( strchr( pgm, '.' ) == NULL ) {
		strcat( pgm, ".cm" );
    }
	
	source = fopen( pgm, "r" );
	
	if ( source == NULL ) {
		fprintf( stderr, "File %s not found\n", pgm );
		exit( 1 );
	}
	
	listing = stdout; /* send listing to screen */
    
    opt = getopt(argc, argv, "ascp");
    switch(opt) {
        case 'a':
    	    fprintf( listing, "\nC Minus COMPILATION: %s\n", pgm);
	
    	    //while( (ttype = getToken()) != 0 ) {
    	    //	printToken( ttype, token_string );
            //}
    	    syntaxTree = parse();
            printTree( syntaxTree );
            break;
        case 's':
            fprintf( stderr, "\nThis feature is under construction\n");
            break;
        case 'c':
            fprintf( stderr, "\nThis feature is under construction\n");
            break;
            case 'p':
                syntaxTree = parse();
                printTreeColour( syntaxTree );
            break;
        default:
            fprintf( stderr, "usage: %s -[asc] <filename>\n", argv[0] );
            break;
    }
	
    
    fclose( source );
	
    return ( 0 );
}
