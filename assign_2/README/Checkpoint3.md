

### Title: CIS*4650 Compilers ###
### Author: Ryan Pattison and Abhishek Vanarase ###
### Date: 2013-04-04 ###

	 	
# Checkpoint 3 # 
## What was Accomplished ##
We have portions of the c-minus language being supported, however due to the lack of time to 
get the assembly generator all the way to functioning we have some parts of the language not 
yet implemented. For instance we are aple to store local and global variables, output their 
values, and call functions from other functions. We do not at the moment have conditional 
while working to our satisfaction. We did divote the first half of the aloted time to fixing 
problems in previous itterations of the code, in doing so we have a stable parser and lexer
and the assembly being generated this far has been tested and has minor bugs but is known 
to work.


## How we worked ##
We initially started by trying to reproduce the assembly code provided as a sample, but 
soon realized that this was more difficult than producing logically functional assembly 
code using our own procedures. 


## Development Process ##
### Checkpoint 1 ###
Checkpoint one was the implementation of the lexical and syntactical analysis portion of the 
C-Minus language. This was done in the C programming language using the flex and yacc tools 
which are available on most unix machines. The initial goal of the project was to implement 
the compiler in `C++`. However, due to the unfamiliarity of the project, we decided 
to implement C-Minus in C and build off the sample code that was provided by our instructor. 
This restructuring of the *tiny* code gave us a clear design and structure to work within,. 
In addition, the use of the *tiny* code allowed us to use  the course textbook and slides as 
a reference.


### Checkpoint 2 ###
Checkpoint two was the implementation of the syntactic checking portion of the C-Minus 
language. This was done in the C programming language using the flex and yacc tools which 
are available on most unix machines. The initial goal of the checkpoint was to implement 
the compiler in `C++`. We managed to implement a large portion of the project in `C++`
however the amount of work required to get the project to the Checkpoint one stage 
required more than the alloted time. The final Checkpoint 2 was implemented in C which 
allowd us to use the course textbook and slides as a reference.


### Checkpoint 3 ###
Checkpoint three was the development of assembly code using the tree generated from 
checkpoint 1 and using the scopes from checkpoint 2. The most challanging part of this 
itteration was understanding and decyphering the assembly code and generating the proper 
logic to generate this code at will.


###Work Distribution ###
Unfortunately, a breakdown of roles and responsibilities in the group is not possible with the development 
style used, *scrum*. Since the work was done in the same room, at the same time, and work was distributed 
on a micro level, all we can say is that the work was evenly distributed. This work method has been very 
effective and maximizes our learning of the task of implementing every part of the compiler.



