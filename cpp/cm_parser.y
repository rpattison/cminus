%{
#define YYPARSER /* distinguishes Yacc output from other code files */

#include "globals.h"
#include "util.h"
#include "scan.h"
#include "parse.h"

// Abstract syntax
#include "Headers/Absyn.hpp"

// Expressions
#include "Headers/Exp.hpp"

// Variables
#include "Headers/Var.hpp"

// Declarations
#include "Headers/Dec.hpp"

// Variables declarations
#include "Headers/VarDec.hpp"

// Miscellaneous
#include "Headers/Misc.hpp"

//debugging
//#include "Headers/Util.hpp"
//static TreePrint printer;

//#include "Headers.hpp"
#include <string>

union TreeTypes {
    Absyn* absyn;
    Exp* expr;
    Dec* dec;
    Var* var;
    VarDec* vardec;
    SimpleVar* simplevar;
    IndexVar* indexvar;
    NilExp* nilexp;
    VarExp* varexp;
    IntExp* intexp;
    CallExp* callexp;
    OpExp* opexp;
    AssignExp* assignexp;
    IfExp* ifexp;
    WhileExp* whileexp;
    ReturnExp* returnexp;
    CompoundExp* compoundexp;
    WriteExp* writeexp;
    ReadExp* readexp;
    FunctionDec* functiondec;
    SimpleDec* simpledec;
    ArrayDec* arraydec;
    NameTy* namety;
    ExpList* explist;
    DecList* declist;
    VarDecList* vardeclist;
    std::string* str;
};

#define YYSTYPE TreeTypes
Absyn* savedTree = NULL; /**< stores syntax tree for later return */

static NameTy* savedType;

extern int yychar;

int yyerror(const char* message) {
    fprintf(listing, "Syntax error at line %d: %s\n", lineno, message);
    fprintf(listing, "Current token: ");
    printToken(yychar, token_string);
    return 0;
}

/**
 yylex calls getToken to make Yacc/Bison output
 compatible with the CMinus scanner
*/
static int yylex(void) {
    return getToken();
}

Absyn* parse(void) {
    yyparse();
    return savedTree;
}

%}


%token ENDFILE
%token IF ELSE WHILE RETURN
%token VOID INT ID NUMBER
%token SEMICOLON COMMA
%token L_PAREN  R_PAREN L_BRACK  R_BRACK L_BRACE R_BRACE
%token ADD SUBTRACT ASSIGN DIVISION STAR PAREN
%token EQUAL NOT_EQUAL LESS_THAN GREATER_THAN LESS_THAN_EQ GREATER_THAN_EQ
%token ERROR

%nonassoc EQUAL NOT_EQUAL LESS_THAN GREATER_THAN LESS_THAN_EQ GREATER_THAN_EQ
%left ADD SUBTRACT
%left DIVISION STAR

%% /* Grammar for C Minus */

program     : dec_list
                {
                    savedTree = $1.absyn;
                }
            ;

dec_list    : dec_list declar
                {
                    //YYSTYPE t = $1;
                    //if ( t != NULL ) {
                    //    while ( t->sibling != NULL ) {
                    //        t = t->sibling;
                    //    }
                    //    t->sibling = $2;
                    //    $$ = $1;
                    //} else {
                    //    $$ = $2;
                    //}
                    printf("Declist\n");
                    if ($1.declist != NULL) {
                        $1.declist->append($2.dec);
                        $$ = $1;
                    } else {
                        $$ = $1;
                        //.declist = new DecList($2.dec);
                    }
                }
            | declar
                {
                    $$.declist = new DecList($1.dec);
                }
            ;

declar      : var_declar
                {
                    $$ = $1;
                }
            | func_declar
                {
                    $$ = $1;
                }
            ;

var_declar  :   type_spec id SEMICOLON
                {
                    //$$ = newDeclNode( VarK );
                    //$$->attr.name = (char*) $2;
                    //$$->type = savedType;
                    //-------------------------
                    $$.vardec = new VarDec(lineno, savedType, $2.str);
                }
            |   type_spec id L_BRACK number R_BRACK SEMICOLON
                {
                    // TODO Array Class used here ...
                    //$$ = newDeclNode( VarK );
                    //$$->attr.name = (char*)$2;
                    //$$->type = savedType;
                    //YYSTYPE t = newExpNode( ConstK );
                    //t->attr.val = atoi((char*)$4);
                    //t->type = Integer;
                    //$$->child[0] = t;

                    $$.arraydec = new ArrayDec(lineno,
                                    $1.namety, $2.str, $4.intexp);
                }
              | type_spec lex_err SEMICOLON
                {
                    $$ = $2;
                }
            | type_spec error SEMICOLON
                {
                    // TODO
                    //$$ = newErrorNode();
                }
            ;

lex_err :   ERROR
        {
            //$$ = newErrorNode(); TODO
            yyerror("Not a valid identifier");
        }
        | lex_err ERROR
        {
            //$$ = newErrorNode(); TODO
            yyerror("Not a valid identifier");
        }
        ;

type_spec   : INT
                {
                    savedType = new NameTy(lineno, NameTy::kInt);//Integer;
                    $$.namety = savedType;
                }
            | VOID
                {
                    savedType = new NameTy(lineno, NameTy::kVoid); //Void;
                    $$.namety = savedType;
                }
            ;

func_declar :   type_spec id L_PAREN params R_PAREN compound_stmt
                    {
                        //$$ = newDeclNode( FunK );
                        //$$->attr.name =  (char*) $2;
                        //$$->type = savedType;
                        //$$->child[0] = $4;
                        //$$->child[1] = $6;
                        //------------------------
                        $$.functiondec = new FunctionDec(lineno, $1.namety,
                                $2.str, $4.vardeclist, $6.compoundexp);
                    }
            ;

params      :   param_list
                {
                    $$ = $1;
                }
            | VOID
                {
                    $$.vardeclist = NULL;
                }
            ;

param_list  :   param_list COMMA param
                {
                   // YYSTYPE t = $1;
                   // if ( t != NULL ) {
                   //     while ( t->sibling != NULL ) {
                   //         t = t->sibling;
                   //     }
                   //     t->sibling = $3;
                   //     $$ = $1;
                   // } else {
                   //     $$ = $3;
                   //}
                   //?? $1.declist->getTa il(), $3 //TODO
                   if ($1.vardeclist == NULL) {
                        //$3.vardec->accept(&printer);
                        $$.vardeclist = new VarDecList($3.vardec);
                   } else {
                        //$3.vardec->accept(&printer);
                        $1.vardeclist->append($3.vardec);
                        $$ = $1;
                   }
                }
            | param
                {
                    $$ = $1;
                    //$$.vardeclist = new VarDecList($1.vardec);
                }
            ;

param       :  type_spec id
                {
                    //$$ = newDeclNode( ParamK );
                    //$$->type = savedType;
                    //$$->attr.name = (char*) $2;
                    $$.simpledec = new SimpleDec(lineno, $1.namety, $2.str);
                }
            | type_spec id L_BRACK R_BRACK
                {
                    //$$ = newDeclNode( ParamK );
                    //$$->type = savedType;
                    //$$->attr.name = (char*) $2;
                    $$.arraydec = new ArrayDec(lineno, $1.namety, $2.str);
                    /* TODO DOC null */
                }
            ;

compound_stmt   :   L_BRACE local_declar stmt_list R_BRACE
                    {
                        //$$ = newStmtNode( CmpdK );
                        //$$->child[0] = $2;
                        //$$->child[1] = $3;
                        $$.compoundexp = new CompoundExp(lineno,
                                            $2.vardeclist, $3.explist);
                    }
                ;

local_declar    :   /* empty */
                    {
                        $$.vardeclist = new VarDecList(NULL); //TODO BUG??
                    }
                | local_declar var_declar
                    {
                        //YYSTYPE t = $1;
                        //if ( t != NULL ) {
                        //    while ( t->sibling != NULL ) {
                        //        t = t->sibling;
                        //    }
                        //    t->sibling = $2;
                        //    $$ = $1;
                        //} else {
                        //    $$ = $2;
                        //}
                        if ($1.vardeclist == NULL) {
                           $$.vardeclist = new VarDecList($2.vardec);
                        } else {
                           $1.vardeclist->append($2.vardec);
                           $$ = $1;
                        }
                    }
                ;


stmt_list       :   /* empty */
                    {
                        $$.explist = new ExpList(NULL);
                    }
                | stmt_list statement
                    {
                        //YYSTYPE t = $1;
                        //if ( t != NULL ) {
                        //    while ( t->sibling != NULL ) {
                        //        t = t->sibling;
                        //    }
                        //    t->sibling = $2;
                        //    $$ = $1;
                        //} else {
                        //    $$ = $2;
                        //}
                        if ($1.explist == NULL) {
                            $$.explist = new ExpList($2.expr);
                        } else {
                            $1.explist->append($2.expr);
                            $$ = $1;
                        }
                    }
                ;

statement       : expression_stmt
                    {
                        $$ = $1;
                    }
                | compound_stmt
                    {
                        $$ = $1;
                    }
                | selection_stmt
                    {
                        $$ = $1;
                    }
                | iteration_stmt
                    {
                        $$ = $1;
                    }
                | return_stmt
                    {
                        $$ = $1;
                    }
                ;

expression_stmt : expression SEMICOLON
                    {
                        $$ = $1;
                    }
                | SEMICOLON
                    {
                        $$.expr = new NilExp(lineno);
                    }
                ;

selection_stmt  : IF L_PAREN expression R_PAREN statement
                    {
                         //$$ = newStmtNode( IfK );
                         //$$->child[0] = $3;
                         //$$->child[1] = $5;
                         $$.ifexp = new IfExp(lineno, $3.expr,
                                            $5.expr, new NilExp(lineno));
                    }
                | IF L_PAREN expression R_PAREN statement ELSE statement
                    {
                        //$$ = newStmtNode( IfK );
                        //$$->child[0] = $3;
                        //$$->child[1] = $5;
                        //$$->child[2] = $7;
                        $$.ifexp = new IfExp(lineno, $3.expr, $5.expr, $7.expr);
                    }
                ;

iteration_stmt  : WHILE L_PAREN expression R_PAREN statement
                    {
                        //$$ = newStmtNode( WhileK );
                        //$$->child[0] = $3;
                        //$$->child[1] = $5;
                        //-----------------
                        $$.whileexp = new WhileExp(lineno, $3.expr, $5.expr);
                    }
                ;

return_stmt     :   RETURN SEMICOLON
                    {
                        //$$ = newStmtNode( ReturnK );
                        $$.returnexp = new ReturnExp(lineno,
                                            new NilExp(lineno));
                    }
                |   RETURN expression SEMICOLON
                    {
                        //$$ = newStmtNode( ReturnK );
                        //$$->child[0] = $2;
                        $$.returnexp = new ReturnExp(lineno, $2.expr);
                    }
                ;

expression      :   var ASSIGN expression
                    {
                       // $$ = newExpNode( OpK );
                       // $$->child[0] = $1;
                       // $$->child[1] = $3;
                       // $$->attr.op = ASSIGN;
                        //------------------
                        $$.assignexp = new AssignExp(lineno, $1.var, $3.expr);
                    }
                | simple_express
                    {
                        $$ = $1;
                    }
                | error
                    {
                        //$$ = newErrorNode(); TODO
                    }
                | ERROR  /* Scanner error */
                    {
                        //$$ = newErrorNode(); TODO
                    }
                ;

simple_express  :   add_expr
                    {
                        $$ = $1;
                    }
                |   add_expr EQUAL add_expr
                    {
                        //$$ = newExpNode( OpK );
                        //$$->child[0] = $1;
                        //$$->child[1] = $3;
                        //$$->attr.op = EQUAL;
                        //--------------------
                        $$.opexp = new OpExp(lineno, $1.expr,
                                            OpExp::kEq, $3.expr);
                    }
                |   add_expr NOT_EQUAL add_expr
                        {
                            //$$ = newExpNode( OpK );
                            //$$->child[0] = $1;
                            //$$->child[1] = $3;
                            //$$->attr.op = NOT_EQUAL;
                            //-------------------
                            $$.opexp = new OpExp(lineno, $1.expr,
                                                OpExp::kNe, $3.expr);
                        }
                |   add_expr LESS_THAN add_expr
                        {
                            //$$ = newExpNode( OpK );
                            //$$->child[0] = $1;
                            //$$->child[1] = $3;
                            //$$->attr.op = LESS_THAN;
                            $$.opexp = new OpExp(lineno, $1.expr,
                                                OpExp::kLt, $3.expr);
                        }
                |   add_expr GREATER_THAN add_expr
                        {
                            //$$ = newExpNode( OpK );
                            //$$->child[0] = $1;
                            //$$->child[1] = $3;
                            //$$->attr.op = GREATER_THAN;
                            $$.opexp = new OpExp(lineno, $1.expr,
                                                OpExp::kGt, $3.expr);
                        }
                |   add_expr LESS_THAN_EQ add_expr
                    {
                        //$$ = newExpNode( OpK );
                        //$$->child[0] = $1;
                        //$$->child[1] = $3;
                        //$$->attr.op = LESS_THAN_EQ;
                        $$.opexp = new OpExp(lineno, $1.expr,
                                            OpExp::kLe, $3.expr);
                    }
                |   add_expr GREATER_THAN_EQ add_expr
                    {
                        //$$ = newExpNode( OpK );
                        //$$->child[0] = $1;
                        //$$->child[1] = $3;
                        //$$->attr.op = GREATER_THAN_EQ;
                        $$.opexp = new OpExp(lineno, $1.expr,
                                            OpExp::kGe, $3.expr);
                    }
                ;


var             :   id
                    {
                        //TODO ???
                        //$$ = newExpNode( IdK );
                        //$$->attr.name = (char*) $1;
                        $$.simplevar = new SimpleVar(lineno, $1.str);
                    }
                |   id L_BRACK expression R_BRACK
                    {
                        //$$ = newExpNode( SubsK );
                        //$$->child[0] = newExpNode( IdK );
                        //$$->child[0]->attr.name = (char*) $1;
                        //$$->child[1] = $3;
                        //-----------------------------
                        $$.indexvar = new IndexVar(lineno, $1.str, $3.expr);
                    }
                ;

id              : ID
                    {
                        $$.str = new std::string(token_string);
                        //$$ = (void*)copyString( token_string );
                    }
                ;

number          : NUMBER
                    {
                        $$.intexp = new IntExp(lineno,
                                atoi(token_string));
                        //$$ = (void*)copyString( token_string );
                    }
                ;

add_expr        :   add_expr ADD term
                    {
                        //$$ = newExpNode( OpK );
                        //$$->child[0] = $1;
                        //$$->child[1] = $3;
                        //$$->attr.op = ADD;
                        //---------------
                        $$.opexp = new OpExp(lineno, $1.expr,
                                        OpExp::kPlus, $3.expr);
                    }
                |   add_expr SUBTRACT term
                    {
                        //$$ = newExpNode( OpK );
                        //$$->child[0] = $1;
                        //$$->child[1] = $3;
                        //$$->attr.op = SUBTRACT;
                        //----------------
                        $$.opexp = new OpExp(lineno, $1.expr,
                                        OpExp::kMinus, $3.expr);
                    }
                |   term
                    {
                        $$ = $1;
                    }
                ;

term            :   term STAR factor
                    {
                        //$$ = newExpNode( OpK );
                        //$$->child[0] = $1;
                        //$$->child[1] = $3;
                        //$$->attr.op = STAR;
                        $$.opexp = new  OpExp(lineno, $1.expr,
                                            OpExp::kTimes, $3.expr);
                    }
                |   term DIVISION factor
                    {
                        //$$ = newExpNode( OpK );
                        //$$->child[0] = $1;
                        //$$->child[1] = $3;
                        //$$->attr.op = DIVISION;
                        $$.opexp = new  OpExp(lineno, $1.expr,
                                            OpExp::kOver, $3.expr);
                    }
                | factor
                    {
                        $$ = $1;
                    }
                ;


factor          :   L_PAREN expression R_PAREN
                    {
                        //???
                        //$$ = newExpNode( OpK );
                        //$$->child[0] = $2;
                        //$$->attr.op = PAREN;
                        $$.opexp = new  OpExp(lineno, $2.expr,
                                            OpExp::kParen, new NilExp(lineno));
                    }
                | var
                    {
                        $$ = $1;
                    }
                | call
                    {
                        $$ = $1;
                    }
                | number
                    {
                        $$ = $1;
                        //$$ = newExpNode(ConstK);
                        //$$->attr.val = atoi( token_string );
                        //$$.intexp = new IntExp(lineno, atoi(token_string));
                    }
                ;

call            : id L_PAREN args R_PAREN
                    {
                        //$$ = newExpNode( CallK );
                        //$$->attr.name = (char*) $1;
                        //$$->child[0] = $3;
                        $$.callexp = new CallExp(lineno, $1.str, $3.explist);
                    }
                ;

args            : /* empty */
                    {
                        $$.explist = new ExpList(new NilExp(lineno));
                    }
                | arg_list
                    {
                        $$ = $1;
                    }
                ;


arg_list        : arg_list COMMA expression
                    {
                        //YYSTYPE t = $1;
                        //if ( t != NULL ) {
                        //    while ( t->sibling != NULL ) {
                        //        t = t->sibling;
                        //    }
                        //    t->sibling = $3;
                        //    $$ = $1;
                        //} else {
                        //    $$ = $3;
                        //}
                        if ($1.explist == NULL) {
                            $$.explist = new ExpList($3.expr);
                        } else {
                            $1.explist->append($3.expr);
                            $$ = $1;
                        }
                    }
                | expression
                    {
                        $$ = $1;
                    }

