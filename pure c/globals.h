/**
 * @file globals.h
 * @brief Defines the globally accesible variables and types.
 */

#ifndef _GLOBALS_H_
#define _GLOBALS_H_
   
 #ifdef __cplusplus
 extern "C" {
 #endif

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

extern FILE* source;                    ///< source code text file
extern FILE* listing;                   ///< listing output text file
extern int lineno;                      ///< source line number for listing

                                        /** Needed to decide what attributes in the unions will be active */
typedef enum {
    StmtK,                              ///< identifies this node as a Statement Node @see newStmtNode
    ExpK,                               ///< identifies this node as an Expression Node @see newExpNode
    DecK                                ///< identifies this node as a Declaration Node @see newDecNode
} NodeKind;

                                        /** Needed to decide what attributes in the unions will be active */
typedef enum {
    IfK,                                ///< identifies this node as an if statement
    ReturnK,                            ///<  identifies this node as a return statement
    WhileK,                             ///<  identifies this node as a while loop
    CmpdK                               ///<  identifies this node as a compound statement
} StmtKind;


                                        /** Needed to decide what attributes in the unions will be active */
typedef enum {
    OpK,                                ///< identifies this node as an operator e.g. ` +, -, * `
    ConstK,                             ///< identifies this node as a constant expression ( Number )
    IdK,                                ///< identifies this node as an id reference, the use of a variable
    CallK,                              ///< identifies this node as a function call, e.g. `myfunc()`
    SubsK,                              ///< identifies this node as a subscript operation on an array e.g. `x[i]`
    ErrK                                ///< if the nodeKind is an error the type can be put here
} ExpKind;


                                            /**
                                                Needed to decide what attributes in the unions will be active
                                                When the @see NodeKind is a Declaration @see DecK
                                            */
typedef enum {
    VarK,                                   ///< a variable declaration node e.g. `int x;`
    FunK,                                   ///< a function declaration node e.g. `int main( void ) { ... }`
    ParamK                                  ///< a parameter declaration node e.g. `( ... int x ... ) {`
} DecKind;

                                    /** ExpType is used for type checking */
typedef enum {
    Void,                                   ///< the type is `void`
    Integer                                 ///< same as `int` type 
} ExpType;

                                    /** largest is the Function definition { return_type, Name, Params, Body} */
#define MAXCHILDREN 4

                                         /** Tree nodes for the Sytnax Tree */
typedef struct treeNode {
	struct treeNode* child[MAXCHILDREN]; ///< The children nodes of the current node
	struct treeNode* sibling;            ///< the siblings of the node used in argument and parameter lists
    int lineno;                          ///< the line number the current node occurs on
	NodeKind nodekind;                   ///< the specific type of node (StmtK, ExpK or DecK)
	
                                        /** The union to store the enum value of the kind of node this is */
	union {
		StmtKind stmt;                  ///< if the nodeKind is statement the type can be put here
		ExpKind exp;                    ///< if the nodeKind is expression the type can be put here
		DecKind dec;                    ///< if the nodeKind is declaration the type can be put here
	} kind;
	
                            /** The union to store the value of the node as either an token, int or a string */
	union {
		int op;                         ///< tokentype op holds the operator token as defined in at kind
		int val;                        ///< the value of the node when representing an integer value
        char* name;                    ///< the name of the token as represented in the source always an ID token
	} attr;
    
	ExpType type;                       ///< for type checking of exps either an Integer of Void
} TreeNode;

#ifdef __cplusplus
}
#endif

#endif
