#ifndef __VAR_H__
#define __VAR_H__

#include "Absyn.hpp"
#include <string>

class Exp;


class Var : public Absyn {
public:
    Var(int pos, std::string* name) : Absyn(pos) {
        name_ = name;
    }

     
    void accept(ASTVisitor* visitor);

    std::string* getName() {
        return name_;
    }
private:
    std::string* name_;
};


class SimpleVar : public Var {
public:
    SimpleVar(int pos, std::string* name) : Var(pos, name) {}

     
    void accept(ASTVisitor* visitor);
};


class IndexVar : public Var {
public:
    IndexVar(int pos, std::string* name, Exp* index) : Var(pos, name),

    index_(index) {}

     
    void accept(ASTVisitor* visitor);

    Exp* getExpression()   {
        return index_;
    }
private:
    Exp* index_;
};

#endif /* __VAR_H__ */

