#include <fstream>
#include <iostream>
#include "working.hpp"

using namespace std;

class TreePrint : public ASTVisitor {
public:
    TreePrint( int indent = 0) {
        indent_ = indent;
    }
    
    ~TreePrint(){}
    void visit(Absyn*         node) { cout << "Absyn"       << endl; }
    void visit(Exp*           node) { cout << "Exp"         << endl; }
    void visit(Dec*           node) { cout << "Dec"         << endl; }
    void visit(Var*           node) { cout << "Var"         << endl; }
    void visit(VarDec*        node) { cout << "VarDec"      << endl; }
    void visit(SimpleVar*     node) { cout << "SimpleVar"   << endl; }
    void visit(IndexVar*      node) { cout << "IndexVar"    << endl; }
    void visit(NilExp*        node) { cout << "NilExp"      << endl; }
    void visit(VarExp*        node) { cout << "VarExp"      << endl; }
    void visit(IntExp*        node) { cout << "IntExp"      << endl; }
    void visit(CallExp*       node) { cout << "CallExp"     << endl; }
    void visit(OpExp*         node) { cout << "OpExp"       << endl; }
    void visit(AssignExp*     node) { cout << "AssignExp"   << endl; }
    void visit(IfExp*         node) {
        cout << "IfExp:" << endl;
        node->getCondition().accept(*(TreePrint*)this);
        node->getThenBranch().accept(*(TreePrint*)this);
        node->getElseBranch().accept(*(TreePrint*)this);
    }
    void visit(WhileExp*      node) { cout << "WhileExp"    << endl; }
    void visit(ReturnExp*     node) { cout << "ReturnExp"   << endl; }
    void visit(CompoundExp*   node) { cout << "CompoundExp" << endl;
    }
    void visit(WriteExp*      node) { cout << "WriteExp"    << endl; }
    void visit(ReadExp*       node) { cout << "ReadExp"     << endl; }
    void visit(FunctionDec*   node) { cout << "FunctionDec" << endl; }
    void visit(SimpleDec*     node) { cout << "SimpleDec"   << endl; }
    void visit(ArrayDec*      node) { cout << "ArrayDec"    << endl; }
    void visit(NameTy*        node) { cout << "NameTy"      << endl; }
    void visit(ExpList*       node) { cout << "ExpList"     << endl; }
    void visit(DecList*       node) { cout << "DecList"     << endl; }
    void visit(VarDecList*    node) { cout << "VarDecList"  << endl; }
private:
    int indent_;
};

int main (int argc, char  * argv[])
{
  /*
  if ( 0 == 2 ) {
    return 1;
  } 
*/  
    NilExp nil(0);
    ExpList exps;
    VarDecList vars;
    CompoundExp cmp(0, vars, exps);
    IfExp iff(0, nil, cmp, nil);
    TreePrint tp;
    iff.accept(tp);
    return 0;
}