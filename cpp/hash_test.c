/** example usage **/

#include <stdio.h>

#include "Hash.h"

int main(void) {
    Table* T = tbl_new(101);
    TableIter* iter;

    tbl_beginScope(T);

    tbl_insert(T, "x", 3);
    tbl_insert(T, "y", 7);

    tbl_beginScope(T);
    tbl_insert(T, "x", 5);
    tbl_insert(T, "z", 1);

    int val;
    if (tbl_lookup(T, "x", &val) >= 0) {
        printf("%s = %d\n", "x", val);
    } else {
        printf("x not in table\n");
    }
    printf("---------------\n");

    /* iterate through all pairs with key `x` from any (-1) scope */
    iter = itr_new(T, "x", -1);
    if (iter == NULL) {
        fprintf(stderr, "No Iter\n");
    }

    while (!itr_done(iter)) {
        
        itr_step(iter);
    }
    itr_delete(iter);
    printf("---------------\n");



    TableIter* it;
    

    for (it = itr_new(T, "x", -1); !itr_done(it); itr_step(it))
        printf("%s_%d = %d\n", itr_key(it), itr_scope(it), itr_value(it));
    itr_delete(it);
    
    printf("------------------------------\n");    

    /* iterate through all pairs from the current scope */
    iter = itr_new(T, NULL, tbl_scope(T));
    while (!itr_done(iter)) {
        printf("%s_%d = %d\n", itr_key(iter), itr_scope(iter), itr_value(iter));
        itr_step(iter);
    }

    itr_delete(iter);
    printf("---------------\n");

    /* similar to tbl_lookup x but will not resolve scope */
    iter = itr_new(T, "x", tbl_scope(T));
    while (!itr_done(iter)) {
        printf("%s_%d = %d\n", itr_key(iter), itr_scope(iter), itr_value(iter));
        itr_step(iter);
    }
    itr_delete(iter);
    printf("---------------\n");

    /** pretty print **/
    printf("Table (\n");
    int i = 0;
    for (i = 0; i <= tbl_scope(T); ++i) {
        iter = itr_new(T, NULL, i);
        printf("\t[%d] { ", i);
        while (!itr_done(iter)) {
            printf("%s:%d  ", itr_key(iter), itr_value(iter));
            itr_step(iter);

        }
        printf("}\n");
        itr_delete(iter);
    }
    printf(");\n");
    
    tbl_insert(T, "w", 9);
    
    /* Code for printing scope variables before ending scope */
    printf("[%d]{ ", tbl_scope(T));
    it = itr_new(T, NULL, tbl_scope(T));
    for ( ; itr_done(it) == 0; itr_step(it))
        printf("%s:%d ", itr_key(it), itr_value(it));
    printf("}\n");
    itr_delete(it); /* releases the table (allowing it to be modified) */
    tbl_endScope(T);
    
    
    printf("---------------\n");
    for (it = itr_new(T, NULL, tbl_scope(T)); !itr_done(it); itr_step(it))
        printf("%s_%d = %d\n", itr_key(it), itr_scope(it), itr_value(it));
    itr_delete(it);
    tbl_endScope(T);
    
    tbl_delete(T);
    printf("---------------\n");

    return 0;
}

