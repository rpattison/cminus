#ifndef _ASTVISITOR_HPP_
#define _ASTVISITOR_HPP_ 1

#include "Absyn.hpp"
#include "Exp.hpp"
#include "Misc.hpp"
#include "Var.hpp"
#include "VarDec.hpp"

class ASTVisitor;

class ASTVisitor {
public:
  virtual void visit(Absyn* node) = 0;
  virtual void visit(Exp* node) = 0;
  virtual void visit(Dec* node) = 0;
  virtual void visit(Var* node) = 0;
  virtual void visit(VarDec*      node) = 0;
  virtual void visit(SimpleVar*   node) = 0;
  virtual void visit(IndexVar*    node) = 0;
  virtual void visit(NilExp*      node) = 0;
  virtual void visit(VarExp*      node) = 0;
  virtual void visit(IntExp*      node) = 0;
  virtual void visit(CallExp*     node) = 0;
  virtual void visit(OpExp*       node) = 0;
  virtual void visit(AssignExp*   node) = 0;
  virtual void visit(IfExp*       node) = 0;
  virtual void visit(WhileExp*    node) = 0;
  virtual void visit(ReturnExp*   node) = 0;
  virtual void visit(CompoundExp* node) = 0;
  virtual void visit(WriteExp*    node) = 0;
  virtual void visit(ReadExp*     node) = 0;
  virtual void visit(FunctionDec* node) = 0;
  virtual void visit(SimpleDec*   node) = 0;
  virtual void visit(ArrayDec*    node) = 0;
  virtual void visit(NameTy*      node) = 0;
  virtual void visit(ExpList*     node) = 0;
  virtual void visit(DecList*     node) = 0;
  virtual void visit(VarDecList*  node) = 0;
};


void Absyn::accept(ASTVisitor* visitor)   {
  visitor->visit(this);
}
void Exp::accept(ASTVisitor* visitor)   {
  visitor->visit(this);
}
void Dec::accept(ASTVisitor* visitor)  {
  visitor->visit(this);
}
void Var::accept(ASTVisitor* visitor)  {
  visitor->visit(this);
}
void VarDec::accept(ASTVisitor* visitor)  {
  visitor->visit(this);
}
void SimpleVar::accept(ASTVisitor* visitor)  {
  visitor->visit(this);
}
void IndexVar::accept(ASTVisitor* visitor)  {
  visitor->visit(this);
}
void NilExp::accept(ASTVisitor* visitor)  {
  visitor->visit(this);
}
void VarExp::accept(ASTVisitor* visitor)  {
  visitor->visit(this);
}
void IntExp::accept(ASTVisitor* visitor)  {
  visitor->visit(this);
}
void CallExp::accept(ASTVisitor* visitor)  {
  visitor->visit(this);
}
void OpExp::accept(ASTVisitor* visitor)  {
  visitor->visit(this);
}
void AssignExp::accept(ASTVisitor* visitor)  {
  visitor->visit(this);
}
void IfExp::accept(ASTVisitor* visitor)  {
  visitor->visit(this);
}
void WhileExp::accept(ASTVisitor* visitor)  {
  visitor->visit(this);
}
void ReturnExp::accept(ASTVisitor* visitor)  {
  visitor->visit(this);
}
void CompoundExp::accept(ASTVisitor* visitor)  {
  visitor->visit(this);
}
void WriteExp::accept(ASTVisitor* visitor)  {
  visitor->visit(this);
}
void ReadExp::accept(ASTVisitor* visitor)  {
  visitor->visit(this);
}
void FunctionDec::accept(ASTVisitor* visitor)  {
  visitor->visit(this);
}
void SimpleDec::accept(ASTVisitor* visitor)  {
  visitor->visit(this);
}
void ArrayDec::accept(ASTVisitor* visitor)  {
  visitor->visit(this);
}
void NameTy::accept(ASTVisitor* visitor)   {
  visitor->visit(this);
}
void ExpList::accept(ASTVisitor* visitor)   {
  visitor->visit(this);
}
void DecList::accept(ASTVisitor* visitor)   {
  visitor->visit(this);
}
void VarDecList::accept(ASTVisitor* visitor)  {
  visitor->visit(this);
}

#endif
