/****************************************************/
/* File: tiny.l                                     */
/* FLex specification for the TINY language         */
/* Fei Song                                         */
/****************************************************/

%{
#include "globals.h"
#include "util.h"
#include "scan.h"

TokenStruct yylval;
%}

digit       [0-9]
number      {digit}+
letter      [a-zA-Z]
identifier  {letter}+
newline     \n
whitespace  [ \t]+

%%

"if"            { yylval.row = rowno; return IF; }
"then"          { yylval.row = rowno; return THEN;}
"else"          { yylval.row = rowno; return ELSE;}
"end"           { yylval.row = rowno; return END;}
"repeat"        { yylval.row = rowno; return REPEAT;}
"until"         { yylval.row = rowno; return UNTIL;}
"read"          { yylval.row = rowno; return READ;}
"write"         { yylval.row = rowno; return WRITE;}
":="            { yylval.row = rowno; return ASSIGN;}
"="             { yylval.row = rowno; return EQ;}
"<"             { yylval.row = rowno; return LT;}
">"		{ yylval.row = rowno; return GT;}
"+"             { yylval.row = rowno; return PLUS;}
"-"             { yylval.row = rowno; return MINUS;}
"*"             { yylval.row = rowno; return TIMES;}
"/"             { yylval.row = rowno; return OVER;}
"("             { yylval.row = rowno; return LPAREN;}
")"             { yylval.row = rowno; return RPAREN;}
";"             { yylval.row = rowno; return SEMI;}
{number}        { yylval.row = rowno; 
                  yylval.value = yytext; 
                  return NUM;}
{identifier}    { yylval.row = rowno; 
                  yylval.value = yytext; 
                  return ID;}
{newline}       { rowno++; }
{whitespace}    { /* skip whitespaces */ }
"{"[^}]*"}"     { /* skip comments */ }
.               { yylval.row = rowno; 
                  yylval.value = yytext; 
                  return ERROR; }

%%

TokenType getToken(void) { 
  return (TokenType)yylex(); 
}

