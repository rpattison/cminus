/**
    @file Absyn.hpp
    @brief
*/

#ifndef __ABSYN_HPP__
#define __ABSYN_HPP__ 1

class ASTVisitor;

class Absyn {
public:
    Absyn(int pos) : pos_(pos) {};

    virtual void accept(ASTVisitor* visitor) = 0;

    int getLine() {
        return pos_;
    }
private:
    int pos_;
};

#endif
