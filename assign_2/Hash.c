#include <stdlib.h>
#include <string.h>

#include "Hash.h"
#include "Hash_types.h"

#include <stdio.h>

/**< shift value for hash **/
#define SHIFT 4

/** From professor's notes and slides. **/
static int hash(int size, const char* key) {
    int temp = 0;
    int i = 0;

    if (key == NULL || size == 0) {
        fprintf(stderr, "Invalid hash inputs.\n");
        return 0;
    }

    while (key[i] != '\0') {
        temp = ((temp << SHIFT) + key[i]) % size;
        i++;
    }

    return temp;
}


static Hash* newCell(const char* key, TreeNode* value, int scope) {
    Hash* H = (Hash*)malloc(sizeof(Hash));
    if (H == NULL) {
        fprintf(stderr, "Hash cell allocation failed.\n");
        return NULL;
    }

    H->key = strdup(key);

    if (H->key == NULL) {
        free(H);
        fprintf(stderr, "Hash key allocation failed.\n");
        return NULL;
    }

    H->value = value;
    H->scope = scope;
    H->next = NULL;
    return H;
}


static void deleteCell(Hash* cell) {
    free((void*)cell->key);
    free(cell);
}


int itr_step(TableIter* iter)
{
    if (iter == NULL || (iter->current == NULL &&
            iter->cell_index >= iter->T->size))
    {
        fprintf(stderr, "Iterator already at end\n");
        return 0;
    }

    do
    {
        if (iter->current) 
        {
            iter->current = iter->current->next;
        }

        while (iter->current == NULL)
        {
            iter->cell_index++;
            /* if new cell, and searching key, key DNE anywhere */
            if (iter->key || iter->cell_index >= iter->T->size)
            { /* optimize for key search */
                iter->current = NULL;
                iter->cell_index = iter->T->size;
                return 0;
            }
            iter->current = iter->T->cell[iter->cell_index];
        }
    }  while (iter->current == NULL || /* end of cell list */
                /* not a scope we're looking for '*/
              (iter->scope >= 0 && iter->scope != iter->current->scope) ||
              /* not a key we're looking for'*/
              (iter->key && strcmp(iter->key, iter->current->key) != 0));

    return 1;
}


TableIter* itr_new(Table* T, const char* key, const int scope) {
    TableIter* iter;
    if (T == NULL)
    {
        fprintf(stderr, "invalid table parameter.\n");
        return NULL;
    }

    iter = (TableIter*)malloc(sizeof(TableIter));

    if (iter == NULL)
    {
        fprintf(stderr, "Failed to allocate memory for iterator.\n");
        return NULL;
    }

    iter->cell_index = key ? hash(T->size, key) : 0;
    iter->current = T->cell[iter->cell_index];
    iter->scope = scope;
    iter->key = key ? strdup(key) : NULL;
    iter->T = T;
    T->TableIters++;

    if (iter->current == NULL)  {
        itr_step(iter);
        return iter;
    }

    if (key && strcmp(key, iter->current->key) != 0) {
        itr_step(iter);
        return iter;
    }

    if (scope >= 0 && iter->current->scope != scope ) {
        itr_step(iter);
        return iter;
    }

    return iter;
}


int itr_done(const TableIter* iter) {
    return iter == NULL || iter->current == NULL;
}


void itr_delete(TableIter* iter) 
{
    if (iter == NULL)
        return;
    if (iter->T)
        iter->T->TableIters--;
    free((void *)iter->key);
    free(iter);
}


TreeNode* itr_value(const TableIter* iter)
{
    TreeNode* fake;
    if (iter == NULL || iter->current == NULL)
        return fake;
    return iter->current->value;
}


int itr_scope(const TableIter* iter)
{
    if (iter == NULL || iter->current == NULL)
        return -1;
    return iter->current->scope;
}


const char* itr_key(const TableIter* iter)
{
    if (iter == NULL || iter->current == NULL)
        return NULL;
    return iter->current->key;
}


static int hasKey(const Table* T, const char* key, int cell)
{
    Hash* H = NULL;

    H = T->cell[cell];
    while (H != NULL && T->scope == H->scope )
    {
        if (strcmp(key, H->key) == 0)
        {
            //fprintf(stderr, "value:%d\n", H->value);
            return 1;
        }
        H = H->next;
    }
    return 0;
}


int tbl_insert(Table* T, const char* key, TreeNode* value) {
    int cell = -1;
    Hash* new_cell = NULL;

    if (T == NULL || key == NULL || T->TableIters > 0)
    {
        fprintf(stderr, "null key or null table.\n");
        return -1;
    }

    cell = hash(T->size, key);
    if (hasKey(T, key, cell) == 1)
    {
        //fprintf(stderr, "Key \"%s\" already defined in scope '%d'.\n",
         //   key, T->scope);
        return -1;
    }

    new_cell = newCell(key, value, T->scope);

    if (new_cell == NULL)
    {
        fprintf(stderr, "Allocation of cell failed.\n");
        return -2;
    }

    new_cell->next = T->cell[cell];
    T->cell[cell] = new_cell;
    return T->scope;
}

static int getCell(const Table* T, const char* key, Hash** storage)
{
    int cell = -1;
    Hash* H = NULL;

    if (T == NULL || key == NULL) {
        fprintf(stderr, "Invalid parameters.\n");
        return -2;
    }

    cell = hash(T->size, key);
    H = T->cell[cell];

    while (H != NULL) {
        if (strcmp(key, H->key) == 0) {
            *storage = H;
            return 0;
        }
        H = H->next;
    }
    return -1;
}

int tbl_update(Table* T, const char* key, TreeNode* new_value) {
    Hash* cell;
    
    if (getCell(T, key, &cell) == 0)
    {
        cell->value = new_value;    
        return 0;
    }
    else
    {
        //fprintf(stderr, "Key \"%s\" not found.\n", key);
        return -1;
    }
}

int tbl_lookup(const Table* T, const char* key, TreeNode** value) {
    Hash* cell;
    if (value == NULL) {
        return -2;
    }
    if (getCell(T, key, &cell) == 0)
    {
        *value = cell->value;
        return 0;
    } 
    else
    {
        //fprintf(stderr, "Key \"%s\" not found.\n", key);
        return -1;
    }
}


int tbl_scope(const Table* T) {
    if (T == NULL) {
        fprintf(stderr, "No scope for table.\n");
        return -1;
    }
    return T->scope;
}


int tbl_beginScope(Table* T) {
    if (T == NULL || T->TableIters > 0) {
        fprintf(stderr, "Cant't begin scope.\n");
        return -1;
    }
    T->scope++;
    return 0;
}


int tbl_endScope(Table* T) {
    Hash* cell = NULL;
    Hash* prev = NULL;
    int i = 0;

    if (T == NULL || T->scope <= 0 || T->TableIters > 0) {
        fprintf(stderr, "Can't end scope.\n");
        return -2;
    }

    T->scope--;

    for (i = 0; i < T->size; ++i) {
        cell = T->cell[i];
        while (cell && cell->scope > T->scope) {
            T->cell[i] = cell->next;
            deleteCell(cell);
            cell = T->cell[i];
        }

        if (cell == NULL)
            continue;
        prev = cell;
        cell = cell->next;
        while (cell) {
            /* re arrange pointers here TODO*/
            if (cell->scope > T->scope) {
                prev->next = cell->next;
                deleteCell(cell);
                cell = prev->next;
            } else {
                prev = cell;
                cell = cell->next;
            }
        }
    }
    return 0;
}


Table* tbl_new(int size) {
    Table* T = NULL;
    if (size <= 0) return NULL;
    T = (Table*)malloc(sizeof(Table));
    if (T == NULL) return NULL;
    T->cell = (Hash**)calloc(size, sizeof(Hash*));
    T->TableIters = 0;
    if (T->cell == NULL)  {
        free(T);
        return NULL;
    }

    T->size = size;
    T->scope = 0;
    return T;
}


void tbl_delete(Table* T) {
    Hash* cell = NULL;
    Hash* prev = NULL;
    int i = 0;

    if (T == NULL) return;
    for (i = 0; i < T->size; ++i) {
        cell = T->cell[i];
        prev = cell;
        while (cell) {
            prev = cell;
            cell = cell->next;
            deleteCell(prev);
        }
    }

    free(T->cell);
    free(T);
}
