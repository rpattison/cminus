/* output the first n terms in the fibonacci sequence; ( second form ) */

void fib( int n ) {
	int x;
	
	if ( n <= 1 ) {
		output( n );
		return;
	}
	
	x = fib( n - 1 ) + fib( n - 2 );
	output( x );
}

void main( void ) {
	fib( input() );
}
