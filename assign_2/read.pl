#! /usr/bin/env perl

##
# This script makes the TM Assembly code (Compiled from CM) more readaable.
#
# Flags
# o = operations
# c = side comments
# l = line comments
# 
# Example:   cat fib.tm | ./read.pl cl > legible.txt
# the example removes comments and prints the readable version to legible.txt
# use the flags to remove them from output on the command line
# reorganizes the register instructions into a high level (C-like) syntax
##


sub regname { # interprets the context and gives the register name if applicable.
    my ($regid, $op, $num);
    $regid = shift @_;
    $op = shift @_;
    $num = shift @_;

    if ($op =~ /HALT/) {
        return "#";
    }

    if ($op =~ /LDC/) {
        if ($num == 3) {
            return "#";
        } elsif ($num == 2) {
            return $regid;   
        }
    }
    
    if (($op =~ /IN/ or $op =~ /OUT/) and $num > 1) {
        return "#";
    }

    if ($regid == 0) {
        return "AC";
    } elsif ($regid == 1) {
        return "AC1";
    } elsif ($regid == 5) {
        return "FP";
    } elsif ($regid == 7) {
        return "PC";
    } elsif ($regid == 6) {
        return "GP";
    } else {
        return $regid;
    }
}

foreach $line (<STDIN>) {
    chop $line;
    $ifcomm = ($ARGV[0] !~ /.*c.*/);
    $ifline = ($ARGV[0] !~ /.*l.*/);
    $ifop = ($ARGV[0] !~ /.*o.*/);
    if ($line =~ /\s*(\d+):\s+([A-Z]+)\s+(-?\d+),\s*(-?\d+)\s*\((-?\d+)\)(.*)/) { # matches "03:  LD  0, 3(7)  comment"
        $lineno = $1;
        $op = $ifop ? $2 : "";
        $opp = $2;
        $reg1 = &regname($3, $2, 1);
        $reg2 = $4;
        $reg3 = &regname($5, $2, 3);
        $comment = $ifcomm ? $6 : "";
        %action = ("JLT", " < 0: ", "JEQ", " == 0: ", "JNE", " != 0: ", "JGT"," > 0: ", "JGE", " >= 0: ", "JLE", " <= 0: ");
        if ($opp =~ /ST/) {
            print($lineno, ":\t", $op, "\t",  $reg3, "[", $reg2, "] = ", $reg1,  $comment, "\n");
        } elsif ($opp =~ /LDC/) {
            print($lineno, ":\t", $op, "\t", $reg1, " = ", $reg2, "\t", $comment, "\n");
        } elsif ($opp =~ /J../) {
            print($lineno, ":\t", $op, "\t", "if ", $reg1, $action{$opp}, "PC = ", $reg3, "[", $reg2, "]", $comment, "\n");
        } elsif ($opp =~ /LDA/) {
            print($lineno, ":\t", $op, "\t", $reg1, " = &", $reg3, "[", $reg2, "]", $comment, "\n");
        } else {
            print($lineno, ":\t", $op, "\t", $reg1, " = ", $reg3, "[", $reg2, "]", $comment, "\n");
        }

    } elsif ($line =~ /\s*(\d+):\s+([A-Z]+)\s+(-?\d+),\s*(-?\d+),\s*(-?\d+)(.*)/) { # matches "03: HALT 0, 0, 0   comment"
        $lineno = $1;
        $op = $ifop ? $2 : "";
        $opp = $2;
        $reg1 = &regname($3, $2, 1);
        $reg2 = &regname($4, $2, 2);
        $reg3 = &regname($5, $2, 3);
        $comment = $ifcomm ? $6 : "";
        %sign = ("ADD", "+", "MUL", "*", "DIV", "/", "SUB", "-");

        if ($opp =~ /(ADD)|(MUL)|(DIV)|(SUB)/) {
            print($lineno, ":\t", $op, "\t", $reg1, " = ", $reg2, " ", $sign{$opp}, " ", $reg3, $comment, "\n");
        } elsif ($opp =~ /IN/) {
            print($lineno, ":\t", $op, "\t", $reg1, " = IN()", $comment, "\n");
        } elsif ($opp =~ /OUT/) {
            print($lineno, ":\t", $op, "\tOUT(", $reg1, ")", $comment, "\n");
        } elsif ($opp =~ /HALT/) {
            print($lineno, ":\t", $op, "\tEXIT", $comment, "\n");
        } else {
            print($lineno, ":\t", $op, "\t", $reg1, ", ", $reg2, ", ", $reg3, $comment, "\n");
        }
    } elsif ($line =~ /\*.*/) { # Full Line comment
        if ($ifline) {
            print($line , "\n");
        }
    } else {
        print("UNKNOWN (", $line , ")");
    }
}
