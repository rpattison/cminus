%{
#define YYPARSER /* distinguishes Yacc output from other code files */

#include "globals.h"
#include "util.h"
#include "scan.h"
#include "parse.h"

extern "C" {
  int yyparse(void);

  int yywrap()
  {
          return 1;
  }
  extern int yychar;

  int yyerror( char* message ) { 
      fprintf( listing, "Syntax error at line %d: %s\n", lineno, message );
      fprintf( listing, "Current token: " );
      printToken( yychar, token_string );
      return ( 0 );
  }
 /**
  yylex calls getToken to make Yacc/Bison output
  compatible with the CMinus scanner
 */
 static int yylex(void) { 
     return getToken();
 }
}

#define YYSTYPE TreeNode*
TreeNode* savedTree = NULL; /* stores syntax tree for later return */
static ExpType savedType;

TreeNode* parse(void) { 
    yyparse();
    return savedTree;
}

%}


%token ENDFILE
%token IF ELSE WHILE RETURN
%token VOID INT ID NUMBER
%token SEMICOLON COMMA
%token L_PAREN  R_PAREN L_BRACK  R_BRACK L_BRACE R_BRACE
%token ADD SUBTRACT ASSIGN DIVISION STAR PAREN
%token EQUAL NOT_EQUAL LESS_THAN GREATER_THAN LESS_THAN_EQ GREATER_THAN_EQ 
%token ERROR

%nonassoc EQUAL NOT_EQUAL LESS_THAN GREATER_THAN LESS_THAN_EQ GREATER_THAN_EQ
%left ADD SUBTRACT
%left DIVISION STAR

%% /* Grammar for C Minus */

program     : dec_list  
                {
                    savedTree = $1;
                }
            ;

dec_list    : dec_list declar
                {
                    YYSTYPE t = $1;
                    if ( t != NULL ) {
                        while ( t->sibling != NULL ) {
                            t = t->sibling;
                        }
                        t->sibling = $2;
                        $$ = $1;
                    } else {
                        $$ = $2;
                    }
                }
            | declar 
                {
                    $$ = $1;
                }
            ;

declar      : var_declar
                {
                    $$ = $1;
                }
            | func_declar {
                $$ = $1;
                }
            ;

var_declar  :   type_spec id SEMICOLON
                {
                    $$ = newDeclNode( VarK );
                    $$->attr.name = (char*) $2;
                    $$->type = savedType;
                }
            |   type_spec id L_BRACK number R_BRACK SEMICOLON
                {
                    $$ = newDeclNode( VarK );
                    $$->attr.name = (char*)$2;
                    $$->type = savedType;
                    YYSTYPE t = newExpNode( ConstK );
                    t->attr.val = atoi((char*)$4);
                    t->type = Integer;
                    $$->child[0] = t;
                }
              | type_spec lex_err SEMICOLON
                {
                    $$ = $2;
                }
            | type_spec error SEMICOLON
                {
                       $$ = newErrorNode();
                }
            ;

lex_err :   ERROR 
        {
            $$ = newErrorNode();
            yyerror("Not a valid identifier");
        }
        | lex_err ERROR 
        {
            $$ = newErrorNode();
            yyerror("Not a valid identifier");
        }
        ;

type_spec   : INT
                {
                    savedType = Integer;
                }
            | VOID
                {
                    savedType = Void;
                }
            ;

func_declar :   type_spec id L_PAREN params R_PAREN compound_stmt
                    {
                        $$ = newDeclNode( FunK );
                        $$->attr.name =  (char*) $2;
                        $$->type = savedType;
                        $$->child[0] = $4;
                        $$->child[1] = $6;
                    }
            ;

params      :   param_list 
                {
                    $$ = $1;
                }
            | VOID 
                {
                    $$ = NULL;
                }
            ;

param_list  :   param_list COMMA param
                {
                    YYSTYPE t = $1;
                    if ( t != NULL ) {
                        while ( t->sibling != NULL ) {
                            t = t->sibling;
                        }
                        t->sibling = $3;
                        $$ = $1;
                    } else {
                        $$ = $3;
                    }
                }
            | param
                {
                    $$ = $1;
                }
            ;

param       :  type_spec id
                {
                    $$ = newDeclNode( ParamK );
                    $$->type = savedType;
                    $$->attr.name = (char*) $2;
                }
            | type_spec id L_BRACK R_BRACK
                {
                    $$ = newDeclNode( ParamK );
                    $$->type = savedType;
                    $$->attr.name = (char*) $2;
                }
            ;

compound_stmt   :   L_BRACE local_declar stmt_list R_BRACE
                    {
                        $$ = newStmtNode( CmpdK );
                        $$->child[0] = $2;
                        $$->child[1] = $3;
                    }
                ;

local_declar    :   /* empty */ 
                    {
                        $$ = NULL;
                    }
                | local_declar var_declar
                    {
                        YYSTYPE t = $1;
                        if ( t != NULL ) {
                            while ( t->sibling != NULL ) {
                                t = t->sibling;
                            }
                            t->sibling = $2;
                            $$ = $1;
                        } else {
                            $$ = $2;
                        }
                    }
                ;


stmt_list       :   /* empty */ 
                    {
                        $$ = NULL;
                    }
                | stmt_list statement
                    {
                        YYSTYPE t = $1;
                        if ( t != NULL ) {
                            while ( t->sibling != NULL ) {
                                t = t->sibling;
                            }
                            t->sibling = $2;
                            $$ = $1;
                        } else {
                            $$ = $2;
                        }
                    }
                ;

statement       : expression_stmt
                    {
                        $$ = $1;
                    }
                | compound_stmt 
                    {
                        $$ = $1;
                    }
                | selection_stmt 
                    {
                        $$ = $1;
                    }
                | iteration_stmt
                    {
                        $$ = $1;
                    }
                | return_stmt
                    {
                        $$ = $1;
                    }
                ;
                
expression_stmt : expression SEMICOLON
                    {
                        $$ = $1;
                    }
                | SEMICOLON
                    {
                        $$ = NULL;
                    }
                ;

selection_stmt  : IF L_PAREN expression R_PAREN statement
                    {
                         $$ = newStmtNode( IfK );
                         $$->child[0] = $3;
                         $$->child[1] = $5;
                    }
                | IF L_PAREN expression R_PAREN statement ELSE statement
                    {
                        $$ = newStmtNode( IfK );
                        $$->child[0] = $3;
                        $$->child[1] = $5;
                        $$->child[2] = $7;
                    }
                ;

iteration_stmt  : WHILE L_PAREN expression R_PAREN statement
                    {
                        $$ = newStmtNode( WhileK );
                        $$->child[0] = $3;
                        $$->child[1] = $5;
                    }
                ;
                
return_stmt     :   RETURN SEMICOLON
                    {
                        $$ = newStmtNode( ReturnK );
                    }
                |   RETURN expression SEMICOLON
                    {
                        $$ = newStmtNode( ReturnK );
                        $$->child[0] = $2;
                    }
                ;
                
expression      :   var ASSIGN expression
                    {
                        $$ = newExpNode( OpK );
                        $$->child[0] = $1;
                        $$->child[1] = $3;
                        $$->attr.op = ASSIGN;
                    }
                | simple_express 
                    {
                        $$ = $1;
                    }
                | error
                    {
                        $$ = newErrorNode();
                    }
                | ERROR  /* Scanner error */
                    {
                        $$ = newErrorNode();
                    }
                ;

simple_express  :   add_expr 
                    {
                        $$ = $1;
                    }
                |   add_expr EQUAL add_expr 
                    {
                        $$ = newExpNode( OpK );
                        $$->child[0] = $1;
                        $$->child[1] = $3;
                        $$->attr.op = EQUAL;
                    }
                |   add_expr NOT_EQUAL add_expr 
                        {
                            $$ = newExpNode( OpK );
                            $$->child[0] = $1;
                            $$->child[1] = $3;
                            $$->attr.op = NOT_EQUAL;
                        }
                |   add_expr LESS_THAN add_expr 
                        {
                            $$ = newExpNode( OpK );
                            $$->child[0] = $1;
                            $$->child[1] = $3;
                            $$->attr.op = LESS_THAN;
                        }
                |   add_expr GREATER_THAN add_expr 
                        {
                            $$ = newExpNode( OpK );
                            $$->child[0] = $1;
                            $$->child[1] = $3;
                            $$->attr.op = GREATER_THAN;
                        }
                |   add_expr LESS_THAN_EQ add_expr 
                    {
                        $$ = newExpNode( OpK );
                        $$->child[0] = $1;
                        $$->child[1] = $3;
                        $$->attr.op = LESS_THAN_EQ;
                    }
                |   add_expr GREATER_THAN_EQ add_expr 
                    {
                        $$ = newExpNode( OpK );
                        $$->child[0] = $1;
                        $$->child[1] = $3;
                        $$->attr.op = GREATER_THAN_EQ;
                    }
                ;
                

var             :   id
                    {
                        $$ = newExpNode( IdK );
                        $$->attr.name = (char*) $1;
                    }
                |   id L_BRACK expression R_BRACK
                    {
                        $$ = newExpNode( SubsK );
                        $$->child[0] = newExpNode( IdK );
                        $$->child[0]->attr.name = (char*) $1;
                        $$->child[1] = $3;
                    }
                ;

id              : ID 
                    {
                        $$ = (TreeNode*)copyString( token_string ); 
                    }
                ;

number          : NUMBER
                    {
                         $$ = (TreeNode*)copyString( token_string );
                    }
                ;
                    
add_expr        :   add_expr ADD term
                    {
                        $$ = newExpNode( OpK );
                        $$->child[0] = $1;
                        $$->child[1] = $3;
                        $$->attr.op = ADD;
                    }
                |   add_expr SUBTRACT term
                    {
                        $$ = newExpNode( OpK );
                        $$->child[0] = $1;
                        $$->child[1] = $3;
                        $$->attr.op = SUBTRACT;
                    }
                |   term 
                    {
                        $$ = $1;
                    }
                ;

term            :   term STAR factor
                    {
                        $$ = newExpNode( OpK );
                        $$->child[0] = $1;
                        $$->child[1] = $3;
                        $$->attr.op = STAR;
                    }
                |   term DIVISION factor
                    {
                        $$ = newExpNode( OpK );
                        $$->child[0] = $1;
                        $$->child[1] = $3;
                        $$->attr.op = DIVISION;
                    }
                | factor 
                    { 
                        $$ = $1;
                    }
                ;


factor          :   L_PAREN expression R_PAREN 
                    {
                        $$ = newExpNode( OpK );
                        $$->child[0] = $2;
                        $$->attr.op = PAREN;
                    }
                | var 
                    {
                        $$ = $1;
                    }
                | call 
                    {
                        $$ = $1;
                    }
                | NUMBER 
                    {
                        $$ = newExpNode(ConstK);
                        $$->attr.val = atoi( token_string );
                    }
                ;

call            : id L_PAREN args R_PAREN 
                    {
                        $$ = newExpNode( CallK );
                        $$->attr.name = (char*) $1;
                        $$->child[0] = $3;
                    }
                ;

args            : /* empty */ 
                    {
                        $$ = NULL;
                    }
                | arg_list
                    {
                        $$ = $1;
                    }
                ;


arg_list        : arg_list COMMA expression
                    {
                        YYSTYPE t = $1;
                        if ( t != NULL ) {
                            while ( t->sibling != NULL ) {
                                t = t->sibling;
                            }
                            t->sibling = $3;
                            $$ = $1;
                        } else {
                            $$ = $3;
                        }
                    }
                | expression
                    {
                        $$ = $1;
                    }
                ;
