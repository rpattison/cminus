/**
* @file generator.h
* @brief This if the file used to call generator.c
* @note This file is part of the C- compiler for the CIS-Compilers Course.
* @date 2013
* @authors Ryan Pattison, Abhishek Vanarase
*/

#ifndef _GENERATOR_H_
#define _GENERATOR_H_ 1

#include "globals.h"

extern void generateASM(TreeNode* tree);

#endif
