/****************************************************/
/* File: globals.h                                  */
/* Global types and vars for the TINY scanner       */
/* Fei Song                                         */
/****************************************************/

#ifndef _GLOBALS_H_
#define _GLOBALS_H_

using namespace std;

#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cstring>
#include <string>

typedef enum {
    /* book-keeping tokens */
    ENDFILE, ERROR,
    /* reserved words */
    IF, THEN, ELSE, END, REPEAT, UNTIL, READ, WRITE,
    /* multicharacter tokens */
    ID, NUM,
    /* special symbols */
    ASSIGN, EQ, LT, GT, PLUS, MINUS, TIMES, OVER, LPAREN, RPAREN, SEMI
} TokenType;

struct TokenStruct {
  string value;
  int row;
};

extern int rowno;     /* input file row number */

#endif
