#include <string>
using namespace std;

/**
 @todo
 - Needs to be tested
 - Needs documentation
 */

class VarDecList;
class NameTy;
class ExpList;
class ASTVisitor;

class Absyn {
public:
    Absyn(int pos) : pos_(pos) {};
    
    virtual void accept(ASTVisitor& visitor) = 0;

    int getLine() {
        return pos_;
    }

private:
      int pos_;
};


class Exp : public Absyn {
public:
    Exp(int pos) : Absyn(pos) {};
    
    /** @override */ 
    void accept(ASTVisitor& visitor);
};

class Dec : public Absyn {
public:
    Dec(int pos) : Absyn(pos) {};
    
    /** @override */
    void accept(ASTVisitor& visitor);
};

class Var : public Absyn {
public:
    Var(int pos, string name) : Absyn(pos) {
        name_ = name;
    }
    /** @override */
    void accept(ASTVisitor& visitor);
    
    string getName() {
        return name_;
    }

private:
    string name_;
};

class VarDec : public Absyn {
public:
    VarDec(int pos,   NameTy& type, string name) : Absyn(pos), typ_(type) {
      name_ = name;
    }
    
    /** @override */ 
    void accept(ASTVisitor& visitor)  ;
    
    string getName()   {
        return name_;
    }
    
    NameTy& getType()   {
        return typ_;
    }
    
private:
    string name_;
    NameTy& typ_;
};


class SimpleVar : public Var {
public:
    SimpleVar(int pos, string name) : Var(pos, name) {}
    
    /** @override */ 
    void accept(ASTVisitor& visitor)  ;
};

class IndexVar : public Var {
public:

    IndexVar(int pos, string name,   Exp& index) : Var(pos, name),
    index_(index) {
    }
    /**
    @override
    */ 
      void accept(ASTVisitor& visitor)  ;
    
      Exp& getExpression()   {
        return index_;
    }
private:
      Exp& index_;
};


class NilExp : public Exp {
public:

  /**
  @override
  */ 
    void accept(ASTVisitor& visitor)  ;
    NilExp(int pos) : Exp(pos) {
    }
};

class VarExp : public Exp {
public:

    VarExp(int pos,   Var& variable) : Exp(pos), variable_(variable) {
    }

    /**
    @override
    */ 
      void accept(ASTVisitor& visitor)  ;
      Var& getVariable()   {
        return variable_;
    }
private:
      Var& variable_;
};

class IntExp : public Exp {
public:

    IntExp(int pos, int value) : Exp(pos), value_(value) {
    }

    /**
    @override
    */ 
      void accept(ASTVisitor& visitor)  ;
    int getValue()   {
        return value_;
    }
private:
      int value_;
};

class CallExp : public Exp {
public:

    CallExp(int pos, string func, ExpList& args) : Exp(pos),
    args_(args) {
        func_ = func;
    }
    /**
    @override
    */ 
      void accept(ASTVisitor& visitor)  ;

    string getFunctionName()   {
        return func_;
    }

      ExpList& getArgumentList()   {
        return args_;
    }
private:
    string func_;
      ExpList& args_;
};

class OpExp : public Exp {
public:
  enum OP {
      kPlus=0,
      kMinus=1,
      kTimes=2,
      kOver=3,
      kEq=4,
      kLt=5,
      kGt=6
  };

    OpExp(int pos,   Exp& left, OP op,   Exp& right) : Exp(pos),
    left_(left), right_(right), op_(op) {
    }
    /**
    @override
    */ 
      void accept(ASTVisitor& visitor)  ;
  
    int getOp()   {
        return op_;
    }

      Exp& getLHS()   {
        return left_;
    }

      Exp& getRHS()   {
        return right_;
    }
private:
      Exp& left_;
      OP op_;
      Exp& right_;
};

class AssignExp : public Exp {
public:

    AssignExp(int pos,   Var& lhs,   Exp& rhs) : Exp(pos),
    lhs_(lhs), rhs_(rhs) {
    }

    /**
    @override
    */ 
      void accept(ASTVisitor& visitor)  ;
    
      Var& getLval()   {
        return lhs_;
    }

      Exp& getRval()   {
        return rhs_;
    }
private:
      Var& lhs_;
      Exp& rhs_;
};

class IfExp : public Exp {
public:

    IfExp(int pos,   Exp& test,   Exp& thenpart,
              Exp& elsepart) : Exp(pos), test_(test),
            thenpart_(thenpart), elsepart_(elsepart) {
    }

      Exp& getCondition()   {
        return test_;
    }

    /**
    @override
    */ 
      void accept(ASTVisitor& visitor)  ;
    
      Exp& getThenBranch()   {
        return thenpart_;
    }

      Exp& getElseBranch()   {
        return elsepart_;
    }
private:
      Exp& test_;
      Exp& thenpart_;
      Exp& elsepart_;
};

class WhileExp : public Exp {
public:

    WhileExp(int pos,   Exp& test,   Exp& body) : Exp(pos),
    test_(test), body_(body) {
    }

      Exp& getCondition()   {
        return test_;
    }
    /**
    @override
    */ 
      void accept(ASTVisitor& visitor)  ;
    
      Exp& getBody()   {
        return body_;
    }
private:
      Exp& test_;
      Exp& body_;
};

class ReturnExp : public Exp {
public:

    ReturnExp(int pos,   Exp& expr) : Exp(pos), exp_(expr) {
    }

      Exp& getExpression()   {
        return exp_;
    }
    /**
    @override
    */ 
      void accept(ASTVisitor& visitor)  ;
private:
      Exp& exp_;
};

class CompoundExp : public Exp {
public:

    CompoundExp(int pos,   VarDecList& decs,
              ExpList& exps) : Exp(pos), decs_(decs), exps_(exps) {
    }

      VarDecList& getVariableDeclarations()   {
        return decs_;
    }
    
    /**
    @override
    */ 
      void accept(ASTVisitor& visitor)  ;

      ExpList& getExpressions()   {
        return exps_;
    }
private:
      VarDecList& decs_;
      ExpList& exps_;
};

class WriteExp : public Exp {
public:

    WriteExp(int pos,   Exp& output) : Exp(pos), output_(output) {
    }

    /**
    @override
    */ 
      void accept(ASTVisitor& visitor)  ;
      Exp& getOutput()   {
        return output_;
    }
private:
      Exp& output_;
};


class ReadExp : public Exp {
public:

    ReadExp(int pos,   VarExp& input) : Exp(pos), input_(input) {
    }
    /**
    @override
    */ 
      void accept(ASTVisitor& visitor)  ;
      VarExp& getVariable()   {
        return input_;
    }
private:
      VarExp& input_;
};

class FunctionDec : public Dec {
public:

    FunctionDec(int pos,   NameTy& result, string func,
              VarDecList& params,
              CompoundExp& body) : Dec(pos), result_(result),
    params_(params), body_(body) {
        func_ = func;
    }
    /**
    @override
    */ 
      void accept(ASTVisitor& visitor)  ;
    string getFunctionName()   {
        return func_;
    }

      NameTy& getReturnType()   {
        return result_;
    }

      CompoundExp& getStatements()   {
        return body_;
    }

private:
    string func_;
      NameTy& result_;
      VarDecList& params_;
      CompoundExp& body_;
};

class SimpleDec : public VarDec {
public:

    SimpleDec(int pos,   NameTy& typ, string name) : VarDec(pos, typ, name) {}
    /**
    @override
    */ 
      void accept(ASTVisitor& visitor)  ;
};

class ArrayDec : public VarDec {
public:

    ArrayDec(int pos,   NameTy& typ, string name,
              IntExp& size) : VarDec(pos, typ, name), size_(size) {
    }
    /**
    @override
    */ 
      void accept(ASTVisitor& visitor)  ;
      IntExp& getSizeExpression()   {
        return size_;
    }
private:
      IntExp& size_;
};

class NameTy : public Absyn {
public:

    enum Type {
        kInt=0,
        kVoid=1
    };

    NameTy(int pos, Type typ) : typ_(typ), Absyn(pos) {
    }
    /**
    @override
    */ 
      void accept(ASTVisitor& visitor)  ;
    Type getType()   {
        return typ_;
    }

private:
      Type typ_;
};


class ExpList {
public:

    ExpList(Exp* head=NULL, ExpList* tail = NULL) : head_(head) {
        tail_ = tail;
    }
    /**
    @override
    */ 
      void accept(ASTVisitor& visitor)  ;
      Exp* getHead()   {
        return head_;
    }

      ExpList* getNext()   {
      return tail_;
    }

private:
      Exp* head_;
    ExpList* tail_;
};

class DecList {
public:

    DecList(  Dec* head=NULL, DecList* tail = NULL) : head_(head) {
        tail_ = tail;
    }
    /**
    @override
    */ 
    void accept(ASTVisitor& visitor)  ;
    
      Dec* getHead()   {
        return head_;
    }

      DecList* getNext()   {
        return tail_;
    }

private:
      Dec* head_;
    DecList* tail_;
};

class VarDecList {
public:

    VarDecList(  VarDec* head=NULL, VarDecList* tail = NULL) : head_(head) {
        tail_ = tail;
    }
    /**
    @override
    */ 
      void accept(ASTVisitor& visitor)  ;

      VarDec* getHead()   {
        return head_;
    }

      VarDecList* getNext()   {
        return tail_;
    }

private:
      VarDec* head_;
    VarDecList* tail_;
};

class ASTVisitor {
public:
  virtual void visit(  Absyn* node)=0;
  virtual void visit(  Exp* node)=0;
  virtual void visit(  Dec* node)=0;
  virtual void visit(  Var* node)=0;
  virtual void visit(  VarDec* node)=0;
  virtual void visit(  SimpleVar* node)=0;
  virtual void visit(  IndexVar* node)=0;
  virtual void visit(  NilExp* node)=0;
  virtual void visit(  VarExp* node)=0;
  virtual void visit(  IntExp* node)=0;
  virtual void visit(  CallExp* node)=0;
  virtual void visit(  OpExp* node)=0;
  virtual void visit(  AssignExp* node)=0;
  virtual void visit(  IfExp* node)=0;
  virtual void visit(  WhileExp* node)=0;
  virtual void visit(  ReturnExp* node)=0;
  virtual void visit(  CompoundExp* node)=0;
  virtual void visit(  WriteExp* node)=0;
  virtual void visit(  ReadExp* node)=0;
  virtual void visit(  FunctionDec* node)=0;
  virtual void visit(  SimpleDec* node)=0;
  virtual void visit(  ArrayDec* node)=0;
  virtual void visit(  NameTy* node)=0;
  virtual void visit(  ExpList* node)=0;
  virtual void visit(  DecList* node)=0;
  virtual void visit(  VarDecList* node)=0;
};


void Absyn::accept(ASTVisitor& visitor)   {
  visitor.visit(this);
}
  
void Exp::accept(ASTVisitor& visitor)   {
  visitor.visit(this);
}
 
void Dec::accept(ASTVisitor& visitor)  {
  visitor.visit(this);
}
          
void Var::accept(ASTVisitor& visitor)  {
  visitor.visit(this);
}
          
void VarDec::accept(ASTVisitor& visitor)  {
  visitor.visit(this);
}
       
void SimpleVar::accept(ASTVisitor& visitor)  {
  visitor.visit(this);
}
    
void IndexVar::accept(ASTVisitor& visitor)  {
  visitor.visit(this);
}
     
void NilExp::accept(ASTVisitor& visitor)  {
  visitor.visit(this);
}
       
void VarExp::accept(ASTVisitor& visitor)  {
  visitor.visit(this);
}
       
void IntExp::accept(ASTVisitor& visitor)  {
  visitor.visit(this);
}
       
void CallExp::accept(ASTVisitor& visitor)  {
  visitor.visit(this);
}
      
void OpExp::accept(ASTVisitor& visitor)  {
  visitor.visit(this);
}
        
void AssignExp::accept(ASTVisitor& visitor)  {
  visitor.visit(this);
}
    
void IfExp::accept(ASTVisitor& visitor)  {
  visitor.visit(this);
}
        
void WhileExp::accept(ASTVisitor& visitor)  {
  visitor.visit(this);
}
     
void ReturnExp::accept(ASTVisitor& visitor)  {
  visitor.visit(this);
}
    
void CompoundExp::accept(ASTVisitor& visitor)  {
  visitor.visit(this);
}
  
void WriteExp::accept(ASTVisitor& visitor)  {
  visitor.visit(this);
}
     
void ReadExp::accept(ASTVisitor& visitor)  {
  visitor.visit(this);
}
      
void FunctionDec::accept(ASTVisitor& visitor)  {
  visitor.visit(this);
}
  
void SimpleDec::accept(ASTVisitor& visitor)  {
  visitor.visit(this);
}
    
void ArrayDec::accept(ASTVisitor& visitor)  {
  visitor.visit(this);
}
     
void NameTy::accept(ASTVisitor& visitor)   {
  visitor.visit(this);
}
       
void ExpList::accept(ASTVisitor& visitor)   {
  visitor.visit(this);
}
      
void DecList::accept(ASTVisitor& visitor)   {
  visitor.visit(this);
}
      
void VarDecList::accept(ASTVisitor& visitor)  {
  visitor.visit(this);
}