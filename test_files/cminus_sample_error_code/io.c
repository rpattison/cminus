#include <stdio.h>
#include "io.h"

/* see io.h for more information */

int input( void ) {
	int v;
	scanf("%d\n", &v);
	return ( v );
} 

void output( int v ) {
	printf("%d\n", v);
}