/*
	This file is intended for compiling C-Minus Programs as regular C executables.
	If you add #include "io.h" to the top of a cminus file, it will compile.
*/

#ifndef IO_H
#define IO_H

extern int input( void );

extern void output( int );

#endif